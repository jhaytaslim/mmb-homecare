/** @format */

import { AppRegistry } from "react-native";
import App from "./src/App";
import { name as appName } from "./app.json";
import RNPaystack from "react-native-paystack";

RNPaystack.init({
  publicKey: "pk_live_f1c2b09d9da126072aa099059a136d3d6d2d721d"
});

AppRegistry.registerComponent(appName, () => App);
