export const SET_JWT = "@jwt";
export const SET_USER = "@user";
export const SET_ACCOUNT_TYPE = "@userType";
export const SET_CARER = "@carer";
export const SET_PAYSTACK = "@paystack";
export const SET_PAYMENT_LIST = "@paymentList";
export const SET_CONTRACT_LIST = "@contractList";
export const SET_NOTIFICATION_LOGS = "@notificationLogs";
export const SET_CONTRACT_LOGS = "@contractLogs";
export const SET_CONVERSATIONS = "@conversations";
export const SET_WITHDRAWAL_HISTORY = "@withdrawalHistory";
export const SET_SUBSCRIPTION = "@subscription";
export const SET_CURRENT_PAGE = "@currentPage";
export const SET_PUSH_NOTIFICATION_DATA = "@pushNotificationData";
export const UPDATE_NEW_MESSAGE_NOTIFICATION_STATE =
  "@updatNewMessageNotificationState";
export const SET_UNREAD_MESSAGES = "@setUnreadMessages";
export const SET_MESSAGE_COUNT_OBJECT = "@setMessageCountObject";
export const SET_DEVICE_TOKEN = "@setDeviceToken";
export const SET_MESSAGE_FOR_CURRENT_VIEWED_MESSAGE_PAGE =
  "@setMessageForCurrentViewedMessagePage";
export const SET_UNCHECKED_NEW_CONTRACTS =
  "@setUncheckedNewContracts";
