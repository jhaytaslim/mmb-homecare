import * as types from "./types";

export const setJwt = jwt => ({
  type: types.SET_JWT,
  jwt
});

export const setAccountType = accountType => ({
  type: types.SET_ACCOUNT_TYPE,
  accountType
});

export const setUser = user => ({
  type: types.SET_USER,
  user
});

export const setCarer = carer => ({
  type: types.SET_CARER,
  carer
});

export const setPaystack = paystack => ({
  type: types.SET_PAYSTACK,
  paystack
});

export const setPaymentList = paymentList => ({
  type: types.SET_PAYMENT_LIST,
  paymentList
});

export const setContractList = contractList => ({
  type: types.SET_CONTRACT_LIST,
  contractList
});

export const setContractLogs = contractLogs => ({
  type: types.SET_CONTRACT_LOGS,
  contractLogs
});

export const setNotificationLogs = notificationLogs => ({
  type: types.SET_NOTIFICATION_LOGS,
  notificationLogs
});

export const setConversations = conversations => ({
  type: types.SET_CONVERSATIONS,
  conversations
});

export const setWithdrawalHistory = withdrawalHistory => ({
  type: types.SET_WITHDRAWAL_HISTORY,
  withdrawalHistory
});

export const setSubscription = subscription => ({
  type: types.SET_SUBSCRIPTION,
  subscription
});

export const setCurrentPageData = (currentPageName, currentId) => ({
  type: types.SET_CURRENT_PAGE,
  currentPageName,
  currentId
});

export const setPushNotificationData = pushNotificationData => ({
  type: types.SET_PUSH_NOTIFICATION_DATA,
  pushNotificationData
});

export const updateNewMessageNotificationState = newMessages => ({
  type: types.UPDATE_NEW_MESSAGE_NOTIFICATION_STATE,
  newMessages
});

export const setUnreadMessages = unreadMessages => ({
  type: types.SET_UNREAD_MESSAGES,
  unreadMessages
});

export const setMessageCountObject = messageCountObj => ({
  type: types.SET_MESSAGE_COUNT_OBJECT,
  messageCountObj
});

export const setDeviceToken = deviceToken => ({
  type: types.SET_DEVICE_TOKEN,
  deviceToken
});

export const setMessageForCurrentViewedMessagePage = message => ({
  type: types.SET_MESSAGE_FOR_CURRENT_VIEWED_MESSAGE_PAGE,
  message
});

export const setUncheckedNewContracts = newContracts => ({
  type: types.SET_UNCHECKED_NEW_CONTRACTS,
  newContracts
});
