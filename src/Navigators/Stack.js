import React from "react";
import { Dimensions } from "react-native";
import { StackNavigator } from "react-navigation";

import Tabs from "./Tabs";
import SettingsScreen from "../screens/SettingsScreen";
import HelpScreen from "../screens/HelpScreen";
import PaymentList from "../screens/PaymentScreen/PaymentList";
import PaymentDetails from "../screens/PaymentScreen/PaymentDetails";
import Subscribe from "../screens/SubscriptionScreen/Subscribe";
import CarerVisit from "../screens/VisitScreen/CarerVisit";
import CarerCreateVisit from "../screens/VisitScreen/CarerCreateVisit";
import CarerCreateVisitPlan from "../screens/VisitScreen/CarerCreateVisitPlan";
import ContractInfo from "../screens/ContractScreen/ContractInfo";
import CarerContractDetails from "../screens/ContractScreen/CarerContractDetails";
import CreateCareContract from "../screens/ContractScreen/CreateCareContract";
import Chats from "../screens/MessageScreen/Chats";
import WithdrawalHistory from "../screens/WithdrawalScreen/WithdrawalHistory";
import Carerlist from "../screens/CarerScreen/Carerlist";
import CarerDetail from "../screens/CarerScreen/CarerDetail";
import CarerMessage from "../screens/CarerScreen/CarerMessage";
import CarerVerify from "../screens/CarerScreen/CarerVerify";
import ProfileDetailsTwo from "../screens/ProfileScreen/ProfileDetailsTwo";
import CarerProfessionalDetails from "../screens/ProfileScreen/CarerProfessionalDetails";
import CarerVerificationDocument from "../screens/ProfileScreen/CarerVerificationDocuments";
import CarerPaymentInformation from "../screens/ProfileScreen/CarerPaymentInformation";
import CarerIntroduction from "../screens/ProfileScreen/CarerIntroduction";
import PaystackScreen from "../screens/PaymentScreen/Paystack";
import ViewPlansList from "../screens/VisitScreen/ViewPlansList";
import PatientVisit from "../screens/VisitScreen/PatientVisit";
import CarerSetup from "../screens/CarerScreen/CarerSetup";
import ProfileScreen from "../screens/ProfileScreen/ProfileDetailsOne";
import AboutScreen from "../screens/AboutScreen";
import PasswordSettings from "../screens/SettingsScreen/PasswordSettings";
import PhoneNumberSettings from "../screens/SettingsScreen/PhoneNumberSettings";
import PhoneNumberVerification from "../screens/SettingsScreen/PhoneNumberVerification";

const customTransition = (index, position) => {
  const inputRange = [index - 1, index, index + 1];
  const outputRange = [
    Dimensions.get("window").width,
    0,
    -Dimensions.get("window").width
  ];

  const translateX = position.interpolate({
    inputRange,
    outputRange
  });

  return {
    transform: [{ translateX }]
  };
};

let TransitionConfiguration = () => {
  return {
    screenInterpolator: sceneProps => {
      const { position, scene } = sceneProps;
      const { index } = scene;

      return customTransition(index, position);
    }
  };
};

const Stack = StackNavigator(
  {
    tabRoutes: { screen: Tabs },
    paystack: {
      screen: PaystackScreen
    },
    chats: {
      screen: Chats
    },
    carerSetup: {
      screen: CarerSetup
    },
    profileDetailsTwo: {
      screen: ProfileDetailsTwo
    },
    withdrawalHistory: {
      screen: WithdrawalHistory
    },
    professionalDetails: {
      screen: CarerProfessionalDetails
    },
    verificationDocs: {
      screen: CarerVerificationDocument
    },
    carerPaymentInfo: {
      screen: CarerPaymentInformation
    },
    carerIntro: {
      screen: CarerIntroduction
    },
    carerList: {
      screen: Carerlist
    },
    verifyVisit: {
      screen: CarerVerify
    },
    patientVisit: {
      screen: PatientVisit
    },
    viewplansList: {
      screen: ViewPlansList
    },
    carerMessage: {
      screen: CarerMessage
    },
    engageCarer: {
      screen: CarerDetail
    },
    createContract: {
      screen: CreateCareContract
    },
    carerContractDetails: {
      screen: CarerContractDetails
    },
    contractInfo: {
      screen: ContractInfo
    },
    carerCreateVisitPlan: {
      screen: CarerCreateVisitPlan
    },
    carerCreateVisit: {
      screen: CarerCreateVisit
    },
    carerVisit: {
      screen: CarerVisit
    },
    subscribe: {
      screen: Subscribe
    },
    paymentList: {
      screen: PaymentList
    },
    paymentDetails: {
      screen: PaymentDetails
    },
    help: {
      screen: HelpScreen
    },
    profile: {
      screen: ProfileScreen
    },
    settings: {
      screen: SettingsScreen
    },
    about: {
      screen: AboutScreen
    },
    passwordSettings: {
      screen: PasswordSettings
    },
    phoneNumberSettings: {
      screen: PhoneNumberSettings
    },
    phoneNumberVer: {
      screen: PhoneNumberVerification
    }
  },
  {
    navigationOptions: { header: null },
    transitionConfig: TransitionConfiguration
  }
);

export default Stack;
