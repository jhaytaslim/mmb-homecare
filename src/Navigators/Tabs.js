import React from "react";
import { TabView, SceneMap } from "react-native-tab-view";
import { Dimensions, View, StyleSheet } from "react-native";
import { Icon } from "native-base";
import PropTypes from "prop-types";

import AppHeader from "../components/AppHeader";
import DashboardScreen from "../screens/DashboardScreen";
import InboxList from "../screens/MessageScreen/InboxList";
import DecisionScreen from "../screens/DecisionScreen";
import ContractList from "../screens/ContractScreen/ContractList";
import NotificationScreen from "../screens/NotificationScreen";
import Fonts from "../assets/Fonts";
import Colors from "../assets/Colors";
import { AccountTypeContext } from "../context/AccountTypeContext";
import { ReduxContext } from "../context/ReduxContext";
import NavigationProvider from "../containers/NavigationProvider";
import TabsNavigationContext from "../context/TabsNavigationContext";

class Tabs extends React.PureComponent {
  state = {
    routes: [
      { key: "dashboard", title: "Dashboard" },
      { key: "messaging", title: "Messaging" },
      { key: "subWithDrawal", title: "Subwithdrawal" },
      { key: "contracts", title: "Contracts" },
      { key: "notifications", title: "Notifications" }
    ],
    isContractListModalVisible: false
  };

  static propTypes = {
    account: PropTypes.string,
    screenProps: PropTypes.object,
    onChangeTab: PropTypes.func,
    currentTabIndex: PropTypes.number
  };

  setPageData = () => {
    let { screenProps, currentTabIndex } = this.props;

    if (currentTabIndex == 3) {
      screenProps.setCurrentPageData("new_contract", null);
      screenProps.setUncheckedNewContracts(null);
    }
  };

  getCurrentTabTitle = () => {
    let { account, currentTabIndex } = this.props;

    let title = "";
    switch (currentTabIndex) {
      case 1:
        title = "Inbox";
        break;
      case 2:
        title = account == "carer" ? "Withdraw Funds" : "Best Price Guaranteed";
        break;
      case 3:
        title = "My Contracts";
        break;
      case 4:
        title = "Notifications";
        break;
    }
    return title;
  };

  renderTabBar = props => {
    let { account, currentTabIndex, onChangeTab } = this.props,
      {
        tabs: { iconActiveColor, iconUnactiveColor }
      } = Colors;

    return (
      <View style={styles.tabBarContainer}>
        <Icon
          name="graph-bar"
          type="Foundation"
          style={[
            styles.icon,
            {
              color: currentTabIndex == 0 ? iconActiveColor : iconUnactiveColor,
              fontSize: 23,
              marginTop: 2
            }
          ]}
          onPress={() => onChangeTab(0)}
        />
        <Icon
          name="envelope-o"
          type="FontAwesome"
          onPress={() => onChangeTab(1)}
          style={[
            styles.icon,
            {
              color: currentTabIndex == 1 ? iconActiveColor : iconUnactiveColor
            }
          ]}
        />
        <Icon
          name={account == "carer" ? "wallet" : "thumbs-o-up"}
          type={account == "carer" ? "SimpleLineIcons" : "FontAwesome"}
          style={[
            styles.icon,
            {
              color: currentTabIndex == 2 ? iconActiveColor : iconUnactiveColor
            }
          ]}
          onPress={() => onChangeTab(2)}
        />
        <Icon
          name="heart-o"
          type="FontAwesome"
          style={[
            styles.icon,
            {
              color: currentTabIndex == 3 ? iconActiveColor : iconUnactiveColor
            }
          ]}
          onPress={() => {
            onChangeTab(3);
            this.setPageData();
          }}
        />
        <Icon
          name="bell-o"
          type="FontAwesome"
          style={[
            styles.icon,
            {
              color: currentTabIndex == 4 ? iconActiveColor : iconUnactiveColor
            }
          ]}
          onPress={() => onChangeTab(4)}
        />
      </View>
    );
  };

  render() {
    let { currentTabIndex, onChangeTab } = this.props;

    return (
      <React.Fragment>
        <NavigationProvider navigation={this.props.navigation}>
          <AppHeader
            title={this.getCurrentTabTitle()}
            onPressBackButton={() => onChangeTab(0)}
            navigation={this.props.navigation}
            isTitleHeaderHidden={currentTabIndex == 0 ? true : false}
            titleHeaderRightComponent={
              currentTabIndex == 3 ? (
                <Icon
                  name="filter"
                  type="FontAwesome"
                  style={styles.filterIcon}
                  onPress={() =>
                    this.setState({ isContractListModalVisible: true })
                  }
                />
              ) : null
            }
          />
        </NavigationProvider>
        <TabView
          navigationState={{
            routes: this.state.routes,
            index: this.props.currentTabIndex
          }}
          renderScene={SceneMap({
            dashboard: props => <DashboardScreen {...props} {...this.props} />,
            messaging: props => <InboxList {...props} {...this.props} />,
            subWithDrawal: props => (
              <DecisionScreen {...props} {...this.props} />
            ),
            contracts: props => (
              <ContractList
                {...props}
                {...this.props}
                isFilterModalVisible={this.state.isContractListModalVisible}
                hideFilterModal={() =>
                  this.setState({ isContractListModalVisible: false })
                }
              />
            ),
            notifications: props => (
              <NotificationScreen {...props} {...this.props} />
            )
          })}
          onIndexChange={index => {
            onChangeTab(index);
            setTimeout(() => {
              this.setPageData();
            }, 1);
          }}
          initialLayout={{
            width: Dimensions.get("window").width,
            height: Dimensions.get("window").height
          }}
          tabBarPosition="bottom"
          renderTabBar={this.renderTabBar}
          useNativeDriver
        />
      </React.Fragment>
    );
  }
}

const _Tabs = props => (
  <TabsNavigationContext.Consumer>
    {({ currentTabIndex, onChangeTab }) => (
      <AccountTypeContext.Consumer>
        {({ account }) => (
          <ReduxContext.Consumer>
            {({ screenProps }) => (
              <Tabs
                {...props}
                account={account}
                screenProps={screenProps}
                currentTabIndex={currentTabIndex}
                onChangeTab={onChangeTab}
              />
            )}
          </ReduxContext.Consumer>
        )}
      </AccountTypeContext.Consumer>
    )}
  </TabsNavigationContext.Consumer>
);

export default _Tabs;

const styles = StyleSheet.create({
  tabBarContainer: {
    flexDirection: "row",
    backgroundColor: "#fff",
    height: 60,
    borderTopWidth: 0,
    paddingVertical: 2,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    justifyContent: "space-between",
    alignItems: "center"
  },
  icon: {
    fontSize: Fonts.tabsFont
  },
  filterIcon: {
    color: "#fff",
    fontSize: Fonts.appHeader.icons
  }
});
