import React from "react";
import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  TouchableHighlight,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import { Container, Toast } from "native-base";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";
import ImagePicker from "react-native-image-picker";
import { NavigationContext } from "../context/NavigtionContext";

import Fonts from "../assets/Fonts";
import Colors from "../assets/Colors";
import Icon from "../components/Icon";
import { BoldText } from "../components/AppText";
import NativeTouchableFeedback from "../components/NativeTouchableFeedback";
import ToastStyle from "../assets/ToastStyle";
import { Util } from "../utils";
import TabsNavigationContext from "../context/TabsNavigationContext";

/**
 * This is a stateless functional render function which renders the header
 * of the drawer
 * @param {Object} props
 */
const Header = props => {
  let {
      currentUser: {
        imageUrl,
        first_name,
        last_name,
        account_type,
        patient_id
      },
      currentCarer: { carer_id },
      loading,
      imageUri
    } = props,
    tag =
      account_type != "carer"
        ? patient_id
        : carer_id
          ? `carer - ${carer_id}`
          : "";

  return (
    <View style={styles.headerContainer}>
      <View style={styles.avatarContainer}>
        {imageUrl && !imageUri ? (
          <TouchableHighlight
            style={{ borderRadius: 100 }}
            onPress={props.upload}
          >
            {loading ? (
              <ActivityIndicator
                style={{ alignSelf: "center" }}
                animating={true}
                size={Platform.OS == "android" ? 25 : 0}
                color="#1da1f2"
              />
            ) : (
              <Image
                source={{
                  uri: `https://www.mmbhomecare.com/uploads/70-${imageUrl}`
                }}
                style={styles.avatar}
              />
            )}
          </TouchableHighlight>
        ) : (
          <TouchableHighlight
            style={{ borderRadius: 100 }}
            onPress={props.upload}
          >
            {loading ? (
              <ActivityIndicator
                style={{ alignSelf: "center" }}
                animating={true}
                size={Platform.OS == "ios" ? 0 : 25}
                color="#1da1f2"
              />
            ) : imageUri ? (
              <Image
                source={{
                  uri: imageUri
                }}
                style={styles.avatar}
              />
            ) : (
              <Icon
                name="md-person"
                type="ionic-icon"
                style={styles.iconAvatar}
              />
            )}
          </TouchableHighlight>
        )}
      </View>
      <BoldText style={[styles.headerText, { marginTop: 5 }]}>
        {`${last_name.trim() || ""} ${first_name.trim() || ""}`}
      </BoldText>
      <BoldText style={[styles.headerSubText]}>{tag}</BoldText>
    </View>
  );
};

Header.propTypes = {
  currentUser: PropTypes.any,
  currentCarer: PropTypes.any,
  upload: PropTypes.func,
  imageUri: PropTypes.string,
  loading: PropTypes.bool
};

/**
 * This is a stateless functional render function which renders the body
 * of the drawer
 * @param {Object} props
 */
const Body = props => (
  <NavigationContext.Consumer>
    {({ navigation: { navigate } }) => (
      <View>
        <NativeTouchableFeedback
          onPress={() =>
            navigate(
              props.accountType == "carer" ? "paymentList" : "subWithDrawal"
            )
          }
        >
          <View style={styles.segment}>
            <Icon
              style={styles.defaultText}
              name={props.accountType == "carer" ? "ios-cart" : "thumbs-o-up"}
              type={
                props.accountType == "carer"
                  ? "ionic-icon"
                  : "font-awesome-icon"
              }
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {props.accountType == "carer" ? " My Income" : " My Subscription"}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback
          onPress={() => {
            props.onChangeTab(3);
            props.navigate("DrawerClose");
          }}
        >
          <View style={[styles.segment, styles.coloredSegment]}>
            <Icon
              style={styles.defaultText}
              name="heart-o"
              type="font-awesome-icon"
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {" "}
              My Contracts{" "}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback
          onPress={() => {
            props.onChangeTab(1);
            navigate("DrawerClose");
          }}
        >
          <View style={styles.segment}>
            <Icon
              style={styles.defaultText}
              name="envelope-o"
              type="font-awesome-icon"
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {" "}
              Messages{" "}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback onPress={() => navigate("profile")}>
          <View style={[styles.segment, styles.coloredSegment]}>
            <Icon
              style={styles.defaultText}
              name="user-o"
              type="font-awesome-icon"
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {" "}
              Profile{" "}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback onPress={() => navigate("help")}>
          <View style={styles.segment}>
            <Icon
              style={styles.defaultText}
              name="info-circle"
              type="font-awesome-icon"
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {" "}
              Help{" "}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback onPress={props.logout}>
          <View style={[styles.segment, styles.coloredSegment]}>
            <Icon
              style={styles.defaultText}
              name="power-off"
              type="font-awesome-icon"
            />
            <BoldText style={[styles.defaultText, styles.leftMargin]}>
              {" "}
              Log out{" "}
            </BoldText>
          </View>
        </NativeTouchableFeedback>
      </View>
    )}
  </NavigationContext.Consumer>
);

Body.propTypes = {
  accountType: PropTypes.string,
  logout: PropTypes.func
};

/**
 * This is a stateless functional render function which renders the footer
 * of the drawer
 * @param {Object} props
 */
const Footer = props => (
  <NavigationContext>
    {({ navigation: { navigate } }) => (
      <View>
        <NativeTouchableFeedback onPress={() => navigate("settings")}>
          <View style={[styles.segment, { paddingLeft: 40 }]}>
            <BoldText style={styles.defaultText}>Settings & Privacy</BoldText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback onPress={() => navigate("about")}>
          <View style={[styles.segment, { paddingLeft: 40 }]}>
            <BoldText style={styles.defaultText}>About MMB Homecare</BoldText>
          </View>
        </NativeTouchableFeedback>
      </View>
    )}
  </NavigationContext>
);

/**
 * @Component which renders the drawer Layout of the application
 */
class DrawerLayout extends React.Component {
  state = {
    data: {
      image: undefined
    }
  };

  static propTypes = {
    loading: PropTypes.bool,
    updateProfileImage: PropTypes.func
  };

  upload = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        //@ just kill
      } else if (response.error) {
        //@ send to appcenter
      } else if (response.customButton) {
        //@ Just kill
      } else {
        let source = { uri: `data:${response.type};base64,${response.data}` };
        this.setState(
          {
            data: {
              ...this.state.data,
              image: source.uri
            }
          },
          () => this.uploadImageToServe()
        );
      }
    });
  };

  uploadImageToServe = () => {
    const data = this.state.data;

    this.props
      .updateProfileImage({ data })
      .then(result => {
        //@Todo use a toolTip for both success and error
      })
      .catch(err => {
        this.setState({
          data: { ...this.state.data, image: undefined }
        });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          type: "danger",
          buttonText: "OKAY",
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      });
  };

  notifyUserOfLogout = navigation => {
    Alert.alert(
      "Are You Sure?",
      "Do you realize you are Logging Out?",
      [
        { text: "Cancel", onPress: () => null },
        { text: "Yes, Log me Out!", onPress: () => Util.logout(navigation) }
      ],
      { cancelable: false }
    );
  };

  render() {
    return (
      <NavigationContext.Consumer>
        {({ navigation }) => (
          <Container>
            <ScrollView>
              <Header
                currentUser={this.props.currentUser || {}}
                currentCarer={this.props.currentCarer}
                upload={this.upload}
                imageUri={this.state.data.image}
                loading={this.props.loading}
              />
              <Body
                accountType={this.props.accountType}
                onChangeTab={this.props.onChangeTab}
                logout={() => this.notifyUserOfLogout(navigation)}
              />
              <Footer />
            </ScrollView>
          </Container>
        )}
      </NavigationContext.Consumer>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    currentCarer: state.currentCarer,
    accountType: state.accountType
  };
}

const _DrawerLayout = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <Mutation operation="updateProfileImage">
        {(mutationState, mutate) => (
          <DrawerLayout
            updateProfileImage={mutate}
            loading={mutationState.loading}
            onChangeTab={onChangeTab}
            {...props}
          />
        )}
      </Mutation>
    )}
  </TabsNavigationContext.Consumer>
);

export default connect(mapStateToProps)(_DrawerLayout);

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: Colors.drawerLayout.headerContainer,
    paddingVertical: 15,
    paddingHorizontal: 20
  },
  avatarContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: Colors.drawerLayout.avatarContainer,
    alignItems: "center",
    justifyContent: "center"
  },
  iconAvatar: {
    fontSize: 70,
    color: "#fff"
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  defaultText: {
    color: Colors.drawerLayout.defaultText,
    fontSize: Fonts.drawerLayout.defaultText
  },
  headerText: {
    fontSize: Fonts.drawerLayout.headerText,
    color: Colors.drawerLayout.headerText
  },
  headerSubText: {
    fontSize: Fonts.drawerLayout.headerSubText,
    color: Colors.drawerLayout.headerText
  },
  segment: {
    alignItems: "center",
    paddingLeft: 30,
    height: 50,
    flexDirection: "row"
  },
  coloredSegment: {
    backgroundColor: Colors.drawerLayout.coloredSegment
  },
  leftMargin: {
    marginLeft: 30
  }
});
