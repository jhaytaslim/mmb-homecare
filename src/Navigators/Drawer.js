/**
 * Top Level navigation router of the application
 */
import React from "react";
import { DrawerNavigator } from "react-navigation";

import Stack from "./Stack";
import DrawerLayout from "./DrawerLayout";
import NavigationProvider from "../containers/NavigationProvider";

const CustomDrawerContextDelegate = props => {
  let { navigation } = props;
  return (
    <NavigationProvider navigation={navigation}>
      <DrawerLayout />
    </NavigationProvider>
  );
};

const Drawer = DrawerNavigator(
  {
    Stack: { screen: Stack }
  },
  {
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle",
    contentComponent: CustomDrawerContextDelegate
  }
);

export default Drawer;
