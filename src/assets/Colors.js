/**
 * This file exports the colors needed for the application
 * I think its best practice we stay away from generalizing colors.
 * For instance, we should stay way from using color names such as black100 or black80 to depict the variation of darkness
 * This could cause confusion, since it would be hard to tell how many components in the application are currently making use of the palette
 *
 * Its better we make use of more descriptive and distinctive names such as dashboardLabelColor, statusBarColor
 * tabNavigationIconColor, AppHeaderColor etc. This are more descriptive
 *
 */
export default {
  brandColor: "#00b0cf",
  errorIconColor: "#8594a3",
  roundedInputPlaceholderColor: "#bdbdbd",
  roundedInputIconColor: "#bdbdbd",
  tabs: {
    iconActiveColor: "#1fc5db",
    iconUnactiveColor: "#8594a3"
  },
  drawerLayout: {
    headerContainer: "#00b3d1",
    avatarContainer: "#bbe5ed",
    defaultText: "#00b3d1",
    headerText: "#fff",
    coloredSegment: "#fff"
  },
  appHeader: {
    symbolContainerBackground: "#fff",
    titleHeaderBackground: "#00b0cf",
    title: "#fff",
    mainHeaderBackground: "#fff",
    defaultIcon: "#7a8a9a",
    titleIcon: "#fff"
  },
  profileDisplayCard: {
    largeText: "#0096c0",
    smallText: "#bdc8cf",
    background: "#e6f9fc",
    avatarContainer: "#00a1c4",
    lineSeperator: "#bdbdbd"
  },
  unorderedStepIndicator: {
    indicator: "#00b0cf",
    text: "#000"
  },
  signUp: {
    defaultText: "#fff",
    verificationTitle: "#757575",
    mobileVerificationBackground: "#fff",
    input: "#fff"
  },
  buttons: {
    icon: "#fff",
    text: "#fff"
  },
  modal: {
    defaultText: "#fff",
    background: "#00e8ff",
    input: "#fff",
    textColor: "#fff",
    cancelButton: "#00e8f1",
    subscribeButton: "red"
  }
};
