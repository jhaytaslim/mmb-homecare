/**
 * This file exports the fonts of the application
 *
 * This file should only contain the fontSize for the application only and not other styles such as font weight etc
 * The fontSize of the application seems to be the most important metric of mobile apps
 *
 * Also distictive names should be used here and not general names.
 * meaning names like tabIcon, cardTitle etc are generally good
 */
export default {
  errorIcon: 90,
  largeText: 15,
  smallText: 13,
  tabsFont: 20,
  drawerLayout: {
    defaultText: 14,
    headerText: 20,
    headerSubText: 16
  },
  appHeader: {
    healthIcon: 50,
    icons: 25,
    text: 17
  },
  profileDisplayCard: {
    largeText: 24,
    smallText: 16
  },
  signUp: {
    defaultText: 19,
    navigationIcon: 40,
    verificationTitle: 30
  }
};
