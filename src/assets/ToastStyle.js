import { StyleSheet } from "react-native";

export default StyleSheet.create({
  toast: {
    fontFamily: "Quicksand-Medium",
    fontSize: 14,
    textAlign: "center"
  },
  buttonStyle: {
    backgroundColor: "#fff"
  },
  buttonErrorTextStyle: {
    color: "#e53935"
  },
  buttonSuccessTextStyle: {
    color: "#4caf50"
  }
});
