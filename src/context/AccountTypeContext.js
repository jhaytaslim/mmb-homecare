import React from "react";

export const AccountTypeContext = React.createContext({
  setAccount: () => null,
  account: undefined
});
