import React from "react";

const TabsNavigationContext = React.createContext({
  currentTabIndex: 0,
  onChangeTab: () => null
});

export default TabsNavigationContext;
