import React from "react";
import { View, StyleSheet, ImageBackground } from "react-native";
import PropTypes from "prop-types";
import { Content } from "native-base";

const BackgroundImage = props => (
  <ImageBackground
    source={require("../assets/Images/bgImage.png")}
    style={{ flex: 1 }}
  >
    {props.children}
  </ImageBackground>
);

export default class PaddingProvider extends React.PureComponent {
  static propTypes = {
    style: PropTypes.any,
    type: PropTypes.oneOf(["image", "plain"])
  };

  render() {
    let { style, type } = this.props,
      _style = style || {};

    return !type || type != "plain" ? (
      <BackgroundImage>
        <View style={[styles.mainContainer, _style]}>
          {this.props.children}
        </View>
      </BackgroundImage>
    ) : (
      <View
        style={[styles.mainContainer, _style, { backgroundColor: "#dff5f9" }]}
      >
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 15
  }
});
