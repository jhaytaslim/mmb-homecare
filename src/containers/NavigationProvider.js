import React from "react";
import PropTypes from "prop-types";

import { NavigationContext } from "../context/NavigtionContext";

export default class NavigationProvider extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      navigation: props.navigation
    };
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired
  };

  render() {
    return (
      <NavigationContext.Provider value={this.state}>
        {this.props.children}
      </NavigationContext.Provider>
    );
  }
}
