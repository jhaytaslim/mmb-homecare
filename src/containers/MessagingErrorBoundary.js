import React from "react";
import { LightText } from "../components/AppText";

export default class MessagingErrorBoundary extends React.PureComponent {
  state = {
    hasError: false
  };

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    //@Todo , send the logs to app center
  }

  render() {
    if (this.state.hasError) {
      return <LightText> Something went wrong </LightText>;
    }
    return this.props.children;
  }
}
