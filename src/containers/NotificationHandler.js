import React from "react";
import { NavigationActions } from "react-navigation";
import { AsyncStorage, Linking } from "react-native";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { Util } from "../utils";

class NotificationHandler extends React.Component {
  state = {
    shouldComponentRender: false
  };

  static propTypes = {
    pushNotificationData: PropTypes.object
  };

  componentDidMount() {
    let { navigation, pushNotificationData, accountType } = this.props;

    Linking.getInitialURL()
      .then(url => {
        AsyncStorage.multiGet(["@isOldUser", "@jwt"], (err, results) => {
          if (results[0][1] && results[1][1]) {
            if (url || (!url && !pushNotificationData)) {
              navigation.dispatch(
                NavigationActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "inception"
                    })
                  ]
                })
              );
            } else if (!url && pushNotificationData) {
              //@Its definately a push: Therefore handle data
              Util.handleBackgroundPushNotification(
                this.props.navigation,
                pushNotificationData,
                accountType
              );
            } else {
              navigation.dispatch(
                NavigationActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "inception"
                    })
                  ]
                })
              );
            }
          } else if (results[0][1] && !results[1][1]) {
            navigation.dispatch(
              NavigationActions.reset({
                index: 0,
                key: null,
                actions: [
                  NavigationActions.navigate({
                    routeName: "login"
                  })
                ]
              })
            );
          } else {
            AsyncStorage.setItem("@isOldUser", "true").then(() => {
              this.setState({
                shouldComponentRender: true
              });
            });
          }
        });
      })
      .catch(err => null);
  }

  render() {
    return this.state.shouldComponentRender ? this.props.children : null;
  }
}

function mapStateToProps(state) {
  return {
    pushNotificationData: state.pushNotificationData,
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(NotificationHandler);
