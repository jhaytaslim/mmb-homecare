/**
 * This should contain the utility functions used by the application
 * such as complex data formatting and complex logic which is not coupled with the setState function of your components
 *
 *
 * This means set set should not be called here
 */

import moment from "moment";
import { NavigationActions } from "react-navigation";
import { AsyncStorage } from "react-native";
import axios from "axios";

export const Util = {
  /**
   * This function helps in verifying teh users information when signing up
   * The object passed to this argument must have the following keys and in the following order
   *
   * first_name, last_name, email, mobile, pass, cpass
   *
   * @param {string} data
   */
  verifyRegistrationForm: function(data) {
    let nameRegex = /^[a-zA-Z]{3,}$/,
      emailRegex = /^[^\s@]+@[^\s@.]+\.[^\s@]+$/,
      mobileRegex = /^[0-9]{11}$/,
      errorMessage = undefined,
      isValid = undefined,
      _temporaryPassword = undefined;

    Object.keys(data).forEach((key, i) => {
      (i == 0 || i == 1) &&
        !nameRegex.test(data[key].trim()) &&
        (errorMessage =
          "first/last name can only contain 3 or more characters");

      i == 2 &&
        !emailRegex.test(data[key].trim()) &&
        (errorMessage = "email address invalid");

      i == 3 &&
        !mobileRegex.test(data[key].trim()) &&
        (errorMessage = "Mobile number can only contain 11 numbers");

      i == 4 && (_temporaryPassword = data[key].trim());

      i == 5 &&
        (data[key].trim() != _temporaryPassword ||
          _temporaryPassword.length < 6) &&
        (errorMessage = "Password does not match");
    });

    return { isValid: errorMessage ? false : true, errorMessage };
  },

  formatToNaira: amount => {
    let _naira = Number(amount)
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, "$&,");
    return `₦${_naira}`;
  },

  flattenArray: function(array, currentUser, _previousChats, convo_info) {
    let _generatedObj = array.reduce((acc, value) => {
      let { email, _id, imageUrl } = currentUser;
      let _formerChat =
        (_previousChats.chats && _previousChats.chats[value.created]) ||
        undefined;
      if (!acc[value.created] && _formerChat == undefined) {
        acc[value.created] = {
          _id: value._id,
          text: value.message,
          createdAt: new Date(
            moment(value.created).format("MMMM DD, YYYY H:mm:ss")
          ),
          sent: true,
          id: value.user_id,
          user: {
            _id: value.user_id,
            name:
              currentUser._id == value.user_id
                ? currentUser.first_name
                : convo_info.full_name || value.full_name,
            avatar: value.img
              ? `https://www.mmbhomecare.com/uploads/70-${value.img}`
              : undefined
          }
        };
      }
      return acc;
    }, {});

    return _generatedObj;
  },

  buildMessageObjectFromNotification: notification => ({
    _id: Number(notification._id),
    message: notification.body.trim(),
    convo_id: notification.message_id,
    created: notification.created,
    user_id: Number(notification.user_id),
    read_status: 1,
    img: notification.img,
    full_name: notification.title,
    notification_id: notification.id
  }),

  buildContractObjectFromNotification: notification => ({
    patient: {},
    contract_id: `CTR-${notification.contract_id}`,
    _id: notification.contract_id,
    created: notification.created,
    care_category: notification.care_category,
    carer_name: notification.title
  }),

  patchStatesOfMessages: function(
    _previousObj,
    array,
    convo_id,
    currentUser,
    convo_info
  ) {
    let _previousChats = _previousObj[convo_id] || {},
      _generatedObj = Util.flattenArray(
        array,
        currentUser,
        _previousChats,
        convo_info
      );

    return {
      ..._previousObj,
      [convo_id]: { ..._previousChats, chats: _generatedObj }
    };
  },

  handleBackgroundPushNotification: function(navigation, data, accountType) {
    let routeName = undefined,
      params = undefined,
      { body, title } = data,
      type = data.type || "";

    switch (type) {
      case "message":
        routeName = "chats";
        params = {
          convo_info: {
            convo_id: data.message_id,
            full_name: title,
            message: [
              {
                message: body,
                _id: data._id,
                created: data.created,
                user_id: data.user_id
              }
            ]
          }
        };
        break;
      case "new_contract":
        routeName = "contractInfo";
        params = {
          contract_details: {
            _id: data.contract_id
          }
        };
        break;
      case "new_contract_payment":
        routeName = "carerContractDetail";
        params = {
          contract_details: {
            _id: data.contract_id
          }
        };
        break;
      case "visit_verify":
        routeName = data.visit_id == "0" ? "carerContractDetail" : "carerVisit";
        params =
          data.visit_id == "0"
            ? {
                contract_details: {
                  _id: data.contract_id
                }
              }
            : {
                visit_plan: {
                  visit_plan_id: data.visit_plan_id,
                  contract_id: data.contract_id
                }
              };
        break;
      case "new_visit_plan":
        routeName = "patientVisit";
        params = {
          visit_plan: {
            visit_plan_id: data.visit_plan_id,
            contract_id: data.contract_id
          }
        };
        break;
      case "new_visit_plan_payment":
        routeName = "carerVisit";
        params = {
          visit_plan: {
            visit_plan_id: data.visit_plan_id,
            contract_id: data.contract_id
          }
        };
    }

    if (routeName && params) {
      navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: "inception"
            })
          ]
        })
      );
      navigation.navigate(routeName, params);
    } else {
      navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: "inception"
            })
          ]
        })
      );
    }
  },

  logout: function(navigation) {
    AsyncStorage.multiRemove([
      "@jwt",
      "@userType",
      "@user",
      "@carer",
      "@chats",
      "@paymentList",
      "@contractList",
      "@notificationLogs",
      "@contractLogs",
      "@conversations",
      "@withdrawalHistory",
      "@subscription",
      "@unreadMessages"
    ])
      .then(data => {
        axios({
          method: "DELETE",
          baseURL: "https://www.mmbhomecare.com/api/account/deviceToken"
        })
          .then(data => null)
          .catch(err => null);

        AsyncStorage.clear(err => {
          navigation.dispatch(
            NavigationActions.reset({
              index: 0,
              key: null,
              actions: [
                NavigationActions.navigate({
                  routeName: "login"
                })
              ]
            })
          );
        });
      })
      .catch(err => null);
  }
};
