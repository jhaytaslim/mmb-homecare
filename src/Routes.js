/**
 * Top Level navigation router of the application
 */
import React from "react";
import { Dimensions } from "react-native";
import { StackNavigator } from "react-navigation";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { ActionCreators } from "./actions";
import AccountTypeSelection from "./screens/RegisterScreen/AccountTypeSelection";
import DetailsEntries from "./screens/RegisterScreen/DetailsEntries";
import MobileVerification from "./screens/RegisterScreen/MobileVerification";
import IntroSlider from "./components/IntroSlider";
import LoginScreen from "./screens/LoginScreen";
import Drawer from "./Navigators/Drawer.js";
import EmailVerification from "./screens/ForgotPasswordScreen/EmailVerification";
import PasswordReset from "./screens/ForgotPasswordScreen/PasswordReset";
import { ReduxContext } from "./context/ReduxContext";
import TabsNavigationContext from "./context/TabsNavigationContext";

const customTransition = (index, position) => {
  const inputRange = [index - 1, index, index + 1];
  const outputRange = [
    Dimensions.get("window").width,
    0,
    -Dimensions.get("window").width
  ];

  const translateX = position.interpolate({
    inputRange,
    outputRange
  });

  return {
    transform: [{ translateX }]
  };
};

let TransitionConfiguration = () => {
  return {
    screenInterpolator: sceneProps => {
      const { position, scene } = sceneProps;
      const { index } = scene;

      return customTransition(index, position);
    }
  };
};

const Stack = StackNavigator(
  {
    introSlider: {
      screen: IntroSlider
    },
    inception: {
      screen: Drawer
    },
    mobileVerification: {
      screen: MobileVerification
    },
    signupForm: {
      screen: DetailsEntries
    },
    passwordReset: {
      screen: PasswordReset
    },
    emailVerification: {
      screen: EmailVerification
    },
    login: {
      screen: LoginScreen
    },
    accontType: {
      screen: AccountTypeSelection
    }
  },
  {
    navigationOptions: { header: null },
    transitionConfig: TransitionConfiguration
  }
);

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

class TabsNavigationProvider extends React.PureComponent {
  state = {
    currentTabIndex: 0,
    onChangeTab: index => this.setState({ currentTabIndex: index })
  };

  render() {
    return (
      <TabsNavigationContext.Provider value={this.state}>
        {this.props.children}
      </TabsNavigationContext.Provider>
    );
  }
}

class RoutesAdvanced extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      screenProps: props.screenProps
    };
  }

  render() {
    return (
      <TabsNavigationProvider>
        <ReduxContext.Provider value={this.state}>
          <Stack />
        </ReduxContext.Provider>
      </TabsNavigationProvider>
    );
  }
}

export default connect(
  () => ({}),
  mapDispatchToProps
)(props => <RoutesAdvanced screenProps={props} />);
