import * as types from "../actions/types";

export const jwt = (state = "", action) => {
  switch (action.type) {
    case types.SET_JWT:
      return action.jwt;
      break;
    default:
      return state;
      break;
  }
};

export const accountType = (state = "", action) => {
  switch (action.type) {
    case types.SET_ACCOUNT_TYPE:
      return action.accountType;
      break;
    default:
      return state;
      break;
  }
};

export const currentUser = (state = {}, action) => {
  switch (action.type) {
    case types.SET_USER:
      return action.user;
      break;
    default:
      return state;
      break;
  }
};

export const currentCarer = (state = {}, action) => {
  switch (action.type) {
    case types.SET_CARER:
      return action.carer;
      break;
    default:
      return state;
      break;
  }
};

export const paystack = (state = {}, action) => {
  switch (action.type) {
    case types.SET_PAYSTACK:
      return action.paystack;
      break;
    default:
      return state;
      break;
  }
};

export const paymentList = (state = null, action) => {
  switch (action.type) {
    case types.SET_PAYMENT_LIST:
      return action.paymentList;
      break;
    default:
      return state;
      break;
  }
};

export const contractList = (state = null, action) => {
  switch (action.type) {
    case types.SET_CONTRACT_LIST:
      return action.contractList;
      break;
    default:
      return state;
      break;
  }
};

export const contractLogs = (state = null, action) => {
  switch (action.type) {
    case types.SET_CONTRACT_LOGS:
      return action.contractLogs;
      break;
    default:
      return state;
      break;
  }
};

export const notificationLogs = (state = null, action) => {
  switch (action.type) {
    case types.SET_NOTIFICATION_LOGS:
      return action.notificationLogs;
      break;
    default:
      return state;
      break;
  }
};

export const conversations = (state = null, action) => {
  switch (action.type) {
    case types.SET_CONVERSATIONS:
      return action.conversations;
      break;
    default:
      return state;
      break;
  }
};

export const withdrawalHistory = (state = null, action) => {
  switch (action.type) {
    case types.SET_WITHDRAWAL_HISTORY:
      return action.withdrawalHistory;
      break;
    default:
      return state;
      break;
  }
};

export const subscription = (state = null, action) => {
  switch (action.type) {
    case types.SET_SUBSCRIPTION:
      return action.subscription;
      break;
    default:
      return state;
      break;
  }
};

export const currentPageData = (
  state = { currentPageName: null, currentId: null },
  action
) => {
  switch (action.type) {
    case types.SET_CURRENT_PAGE:
      return {
        currentPageName: action.currentPageName,
        currentId: action.currentId
      };
      break;
    default:
      return state;
      break;
  }
};

export const pushNotificationData = (state = null, action) => {
  switch (action.type) {
    case types.SET_PUSH_NOTIFICATION_DATA:
      return action.pushNotificationData;
      break;
    default:
      return state;
      break;
  }
};

export const newMessagesNotification = (state = null, action) => {
  switch (action.type) {
    case types.UPDATE_NEW_MESSAGE_NOTIFICATION_STATE:
      return action.newMessages;
      break;
    default:
      return state;
      break;
  }
};

export const unreadMessages = (state = [], action) => {
  switch (action.type) {
    case types.SET_UNREAD_MESSAGES:
      return action.unreadMessages;
      break;
    default:
      return state;
      break;
  }
};

export const messageCountObject = (state = {}, action) => {
  switch (action.type) {
    case types.SET_MESSAGE_COUNT_OBJECT:
      return action.messageCountObj;
      break;
    default:
      return state;
      break;
  }
};

export const deviceToken = (state = "", action) => {
  switch (action.type) {
    case types.SET_DEVICE_TOKEN:
      return action.deviceToken;
      break;
    default:
      return state;
      break;
  }
};

export const messageForCurrentViewedMessagePage = (state = null, action) => {
  switch (action.type) {
    case types.SET_MESSAGE_FOR_CURRENT_VIEWED_MESSAGE_PAGE:
      return action.message;
      break;
    default:
      return state;
      break;
  }
};

export const uncheckedNewContracts = (state = null, action) => {
  switch (action.type) {
    case types.SET_UNCHECKED_NEW_CONTRACTS:
      return action.newContracts;
      break;
    default:
      return state;
      break;
  }
};
