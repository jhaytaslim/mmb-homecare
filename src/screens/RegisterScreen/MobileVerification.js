import React from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  Alert,
  Platform
} from "react-native";
import { Input, Content, Item, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import { MediumText, BoldText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import Icon from "../../components/Icon";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import AppStatusBar from "../../components/AppStatusBar";

class MobileVerification extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    mutate: PropTypes.func,
    otpMutateFn: PropTypes.func,
    loadingOtpResend: PropTypes.bool
  };

  state = {
    isVerified: false,
    accessCode: ""
  };

  handleApiResolve = result => {
    let {
        navigation: { navigate }
      } = this.props,
      {
        status: { code, desc }
      } = result;

    if (parseInt(code) == 100) {
      this.setState(
        {
          isVerified: true
        },
        () => setTimeout(() => navigate("login"), 2000)
      );
    } else {
      Platform.OS !== "ios"
        ? Alert.alert(
            "Error Message",
            desc,
            [{ text: "Okay", onPress: () => null }],
            { cancelable: false }
          )
        : Toast.show({
            text: desc,
            textStyle: ToastStyle.toast,
            type: "danger",
            buttonText: "okay",
            duration: 30000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle
          });
    }
  };

  verifyAccount = () => {
    let {
        navigation: {
          state: {
            params: { email, accountType }
          },
          navigate
        }
      } = this.props,
      { accessCode } = this.state;

    if (accessCode.trim().length != 0) {
      if (accountType == "register") {
        this.props
          .mutate({ data: { email, code: accessCode } })
          .then(result => {
            this.handleApiResolve(result);
          })
          .catch(err => {
            Toast.show({
              text: `${err}`,
              textStyle: ToastStyle.toast,
              type: "danger",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonErrorTextStyle
            });
          });
      } else {
        navigate("passwordReset", {
          email,
          code: this.state.accessCode
        });
      }
    } else {
      Alert.alert(
        "Error Message",
        "Access code cannot be empty",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  resendOtp = () => {
    let {
      navigation: {
        state: {
          params: { email }
        }
      }
    } = this.props;
    this.props
      .otpMutateFn({ data: { email } })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (parseInt(code) == 100) {
          Toast.show({
            text: `Please check your mail and mobile phone for code`,
            duration: 10000,
            textStyle: ToastStyle.toast,
            type: "success",
            buttonText: "okay",
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  renderSuccessModal = () => {
    let {
      navigation: {
        state: {
          params: { accountType }
        }
      }
    } = this.props;

    return (
      <View style={styles.container}>
        <Icon
          name="md-checkmark-circle-outline"
          type="ionic-icon"
          style={styles.icon}
        />
        <MediumText style={styles.title}>
          MOBILE VERIFICATION SUCCESSFUL
        </MediumText>
        {accountType == "register" && (
          <MediumText>Login to continue</MediumText>
        )}
      </View>
    );
  };

  renderVerificationInputContainer = () => (
    <React.Fragment>
      <View style={styles.container}>
        <BoldText style={styles.title}>MOBILE VERIFICATION</BoldText>
        <MediumText>
          Please enter the 4 digit verification code sent to your phone number
        </MediumText>
        <Item style={styles.inputBox}>
          <Input
            value={this.state.accessCode}
            keyboardType="numeric"
            onChangeText={value => this.setState({ accessCode: value })}
            placeholder="Input 4 digits code"
            style={{ fontFamily: "Quicksand-Medium" }}
          />
        </Item>
        <View style={styles.appButtonContainer}>
          <AppButton
            title="Verify"
            style={styles.verificationButton}
            onPress={this.verifyAccount}
          />
          <AppButton
            title="Back"
            style={styles.verificationButton}
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </View>
      <MediumText style={styles.bottomText} onPress={this.resendOtp}>
        Resend Verification Number
      </MediumText>
    </React.Fragment>
  );

  render() {
    let { loading, loadingOtpResend } = this.props;
    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={styles.mainContainer}
      >
        <AppStatusBar />
        <React.Fragment>
          {(loading || loadingOtpResend) && <LoadingView />}
          {this.state.isVerified
            ? this.renderSuccessModal()
            : this.renderVerificationInputContainer()}
        </React.Fragment>
      </ImageBackground>
    );
  }
}

export default (_MobileVerification = props => (
  <Mutation operation="createVerification">
    {(mutationState, mutate) => (
      <Mutation operation="createResendOtp">
        {(otpResendState, otpMutateFn) => (
          <MobileVerification
            mutate={mutate}
            loading={mutationState.loading}
            otpMutateFn={otpMutateFn}
            loadingOtpResend={otpResendState.loading}
            {...props}
          />
        )}
      </Mutation>
    )}
  </Mutation>
));

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    marginHorizontal: 10,
    backgroundColor: "#fff",
    borderRadius: 15,
    alignItems: "center",
    padding: 20
  },
  title: {
    fontSize: Fonts.signUp.verificationTitle,
    color: Colors.signUp.verificationTitle,
    marginVertical: 10,
    textAlign: "center"
  },
  inputBox: {
    borderBottomColor: "#006ba5"
  },
  icon: {
    fontSize: 50,
    color: "green"
  },
  bottomText: {
    color: "#fff"
  },
  appButtonContainer: {
    flexDirection: "row",
    alignSelf: "center"
  },
  verificationButton: {
    backgroundColor: "red",
    marginTop: 10,
    marginHorizontal: 10
  }
});
