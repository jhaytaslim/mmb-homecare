import React from "react";
import { View, AsyncStorage, ImageBackground } from "react-native";
import PropTypes from "prop-types";

import Icon from "../../components/Icon";
import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import AppButton from "../../components/AppButton";
import AppStatusBar from "../../components/AppStatusBar";

export default class AccountTypeSelection extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };

  setUserType = type => {
    let {
      navigation: { navigate }
    } = this.props;
    AsyncStorage.setItem("@userType", type)
      .then(result => navigate("signupForm"))
      .catch(err => null);
  };

  render() {
    let {
      navigation: { goBack }
    } = this.props;

    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={styles.container}
      >
        <AppStatusBar />
        <View style={styles.body}>
          <MediumText style={styles.text}> Select Account Type </MediumText>
          <MediumText style={styles.text}> I am a: </MediumText>
          <AppButton
            style={{ ...styles.appButton, backgroundColor: "red" }}
            iconName="md-man"
            onPress={() => this.setUserType("patient")}
            title="Patient"
          />
          <AppButton
            style={styles.appButton}
            iconName="md-git-network"
            onPress={() => this.setUserType("carer")}
            title="Carer"
          />
        </View>
        <Icon
          onPress={() => goBack()}
          contentContainerStyle={styles.iconContainer}
          name="ios-arrow-round-back"
          type="ionic-icon"
          style={styles.icon}
        />
      </ImageBackground>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: "space-between"
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  iconContainer: {
    marginBottom: 30,
    marginLeft: 30
  },
  icon: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.navigationIcon
  },
  text: {
    marginTop: 40,
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.defaultText
  },
  appButton: {
    marginTop: 10,
    alignSelf: "center",
    height: 55,
    paddingHorizontal: 50
  }
};
