import React from "react";
import {
  StyleSheet,
  View,
  AsyncStorage,
  Platform,
  ImageBackground,
  Alert
} from "react-native";
import { Content, Item, Icon, Input, Toast } from "native-base";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";

import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import AppIcon from "../../components/Icon";
import AppButton from "../../components/AppButton";
import { Util } from "../../utils";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import AppStatusBar from "../../components/AppStatusBar";

class DetailsEntries extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    mutate: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
  };

  state = {
    first_name: "",
    last_name: "",
    email: "",
    mobile: "",
    pass: "",
    cpass: ""
  };

  setFormValue = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  registerUser = () => {
    let {
      navigation: { navigate }
    } = this.props;
    let { isValid, errorMessage } = Util.verifyRegistrationForm(this.state);
    if (isValid) {
      AsyncStorage.getItem("@userType").then(type => {
        this.props
          .mutate({ data: { ...this.state, usertype: type } })
          .then(result => {
            let {
              status: { code, desc }
            } = result;
            if (parseInt(code) != 100) {
              Platform.OS !== "ios"
                ? Alert.alert(
                    "Error Message",
                    desc,
                    [{ text: "Okay", onPress: () => null }],
                    { cancelable: false }
                  )
                : Toast.show({
                    text: desc,
                    textStyle: ToastStyle.toast,
                    type: "danger",
                    buttonText: "okay",
                    duration: 30000,
                    buttonStyle: ToastStyle.buttonStyle,
                    buttonTextStyle: ToastStyle.buttonErrorTextStyle
                  });
            } else {
              navigate("mobileVerification", {
                email: this.state.email,
                accountType: "register"
              });
            }
          })
          .catch(err => {
            Toast.show({
              text: `${err}`,
              textStyle: ToastStyle.toast,
              type: "danger",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonErrorTextStyle
            });
          });
      });
    } else {
      Alert.alert(
        "Error Message",
        errorMessage,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let { first_name, last_name, email, mobile, pass, cpass } = this.state,
      {
        loading,
        navigation: { goBack }
      } = this.props;

    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={{ width: "100%", height: "100%" }}
      >
        <AppStatusBar />
        <Content>
          <View style={styles.body}>
            <MediumText style={styles.title}>INPUT DETAILS</MediumText>
            <Item style={styles.inputContainer}>
              <Icon active name="md-person" style={styles.inputIcon} />
              <Input
                placeholder="First Name*"
                value={first_name}
                onChangeText={value => this.setFormValue("first_name", value)}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="md-person" style={styles.inputIcon} />
              <Input
                placeholder="Last Name*"
                value={last_name}
                onChangeText={value => this.setFormValue("last_name", value)}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="ios-mail-outline" style={styles.inputIcon} />
              <Input
                placeholder="Email*"
                value={email}
                onChangeText={value => this.setFormValue("email", value)}
                placeholderTextColor="#fff"
                keyboardType="email-address"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="ios-call-outline" style={styles.inputIcon} />
              <Input
                placeholder="Phone Number*"
                value={mobile}
                onChangeText={value => this.setFormValue("mobile", value)}
                placeholderTextColor="#fff"
                keyboardType="numeric"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="md-lock" style={styles.inputIcon} />
              <Input
                placeholder="Password*"
                value={pass}
                onChangeText={value => this.setFormValue("pass", value)}
                secureTextEntry={true}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="md-lock" style={styles.inputIcon} />
              <Input
                placeholder="Retype Password*"
                value={cpass}
                onChangeText={value => this.setFormValue("cpass", value)}
                secureTextEntry={true}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <AppButton
              title="Register"
              style={styles.registerButton}
              onPress={this.registerUser}
            />
          </View>
        </Content>
        {loading && <LoadingView />}
        <AppIcon
          onPress={() => goBack()}
          contentContainerStyle={styles.iconContainer}
          name="ios-arrow-round-back"
          type="ionic-icon"
          style={styles.icon}
        />
      </ImageBackground>
    );
  }
}

export default (_DetailsEntries = props => (
  <Mutation operation="createAccount">
    {(mutationState, mutate) => (
      <DetailsEntries
        mutate={mutate}
        loading={mutationState.loading}
        {...props}
      />
    )}
  </Mutation>
));

const styles = StyleSheet.create({
  inputIcon: {
    color: Colors.signUp.input
  },
  textInput: {
    color: Colors.signUp.input,
    fontFamily: "Quicksand-Medium"
  },
  inputContainer: {
    marginTop: 20
  },
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "#006ba5"
  },
  body: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 30
  },
  title: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.defaultText,
    marginTop: 70
  },
  icon: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.navigationIcon
  },
  bottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 30,
    paddingHorizontal: 30
  },
  registerButton: {
    alignSelf: "center",
    marginTop: 10
  },
  iconContainer: {
    marginBottom: 10,
    marginLeft: 30
  },
  icon: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.navigationIcon
  }
});
