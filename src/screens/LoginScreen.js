import React from "react";
import {
  View,
  AsyncStorage,
  Image,
  Platform,
  ImageBackground,
  Alert
} from "react-native";
import { Content, Item, Icon, Input, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";

import Colors from "../assets/Colors";
import AppButton from "../components/AppButton";
import Fonts from "../assets/Fonts";
import { MediumText } from "../components/AppText";
import LoadingView from "../components/LoadingView";
import { ReduxContext } from "../context/ReduxContext";
import { AccountTypeContext } from "../context/AccountTypeContext";
import ToastStyle from "../assets/ToastStyle";
import AppStatusBar from "../components/AppStatusBar";

class LoginScreen extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    mutate: PropTypes.func,
    screenProps: PropTypes.object,
    setAccount: PropTypes.func
  };

  state = {
    email: "",
    password: "",
    loading: false
  };

  componentDidMount() {
    let { screenProps } = this.props;
    screenProps.setJwt("");
    screenProps.setAccountType("");
    screenProps.setUser({});
    screenProps.setCarer({});
    screenProps.setPaymentList(null);
    screenProps.setContractList(null);
    screenProps.setNotificationLogs(null);
    screenProps.setContractLogs(null);
    screenProps.setConversations(null);
    screenProps.setSubscription(null);
  }

  updateDeviceToken = () => {
    this.props
      .updateDeviceToken({ data: { token: this.props.deviceToken } })
      .then(data => null)
      .catch(err => null);
  };

  login = () => {
    let error = undefined,
      { email } = this.state;

    if (!/^[^\s@]+@[^\s@.]+\.[^\s@]+$/.test(email.trim()))
      error = "Email address is not valid";

    Platform.OS == "ios" && !error && this.setState({ loading: true });

    if (!error) {
      this.props
        .mutate({ data: { ...this.state } })
        .then(data => {
          Platform.OS == "ios" && this.setState({ loading: false });
          let {
            status: { code, desc },
            entity
          } = data;
          let _token = entity && entity.token;
          if (parseInt(code) == 100) {
            this.props.setAccount(entity.detail.account_type);
            this.props.screenProps.setUser(entity.detail);
            this.props.screenProps.setAccountType(entity.detail.account_type);
            this.updateDeviceToken();

            AsyncStorage.multiSet([
              ["@jwt", _token],
              ["@userType", entity.detail.account_type],
              ["@user", JSON.stringify(entity.detail)]
            ]).then(data => {
              this.props.screenProps.setJwt(_token);
              this.props.navigation.dispatch(
                NavigationActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "inception"
                    })
                  ]
                })
              );
            });
          } else if (parseInt(code) == 107) {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  "Account is not verified, check your mail/phone number and enter verification code ",
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text:
                    "Account is not verified, check your mail/phone number and enter verification code ",
                  textStyle: ToastStyle.toast,
                  type: "danger",
                  buttonText: "okay",
                  duration: 20000,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle
                });

            this.props.navigation.navigate("mobileVerification", {
              email: this.state.email,
              accountType: "register"
            });
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  textStyle: ToastStyle.toast,
                  type: "danger",
                  buttonText: "okay",
                  duration: 30000,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle
                });
          }
        })
        .catch(err => {
          Platform.OS == "ios" && this.setState({ loading: false });
          Toast.show({
            text: "No network connection",
            textStyle: ToastStyle.toast,
            type: "danger",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle
          });
        });
    } else {
      Toast.show({
        text: error,
        textStyle: ToastStyle.toast,
        type: "danger",
        duration: 10000,
        buttonText: "okay",
        buttonStyle: ToastStyle.buttonStyle,
        buttonTextStyle: ToastStyle.buttonErrorTextStyle
      });
    }
  };

  setFormValue = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  render() {
    let { email, password } = this.state,
      {
        navigation: { navigate },
        loading
      } = this.props;

    return (
      <ImageBackground
        source={require("../assets/Images/authImage.png")}
        style={{ flex: 1 }}
      >
        {Platform.OS == "android"
          ? loading && <LoadingView />
          : this.state.loading && <LoadingView />}
        <AppStatusBar barStyle="light-content" backgroundColor="#00b0cf" />
        <Content>
          <View style={styles.body}>
            <Image
              source={require("../assets/Images/logo2.png")}
              style={styles.logo}
            />
            <Item style={styles.inputContainer}>
              <Icon active name="ios-mail-outline" style={styles.inputIcon} />
              <Input
                placeholder="Email"
                value={email}
                onChangeText={value => this.setFormValue("email", value)}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="md-lock" style={styles.inputIcon} />
              <Input
                placeholder="Password"
                value={password}
                onChangeText={value => this.setFormValue("password", value)}
                secureTextEntry={true}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <AppButton
              title="Log In"
              style={{ ...styles.appButton, marginTop: 40 }}
              onPress={this.login}
            />
            <View style={styles.lineSeperator} />
            <AppButton
              title="Create an account"
              style={{ ...styles.appButton, backgroundColor: "red" }}
              onPress={() => navigate("accontType")}
            />
            <MediumText style={styles.text}>Or</MediumText>
            <MediumText
              style={{ ...styles.text, textDecorationLine: "underline" }}
              onPress={() => navigate("emailVerification")}
            >
              Forgot Password ?
            </MediumText>
          </View>
        </Content>
      </ImageBackground>
    );
  }
}

const _LoginScreen = props => (
  <ReduxContext.Consumer>
    {({ screenProps }) => (
      <AccountTypeContext.Consumer>
        {({ setAccount }) => (
          <Mutation operation="createLogin">
            {(mutationState, mutate) => (
              <Mutation operation="updateDeviceToken">
                {(mutationState, updateDeviceToken) => {
                  return (
                    <LoginScreen
                      mutate={mutate}
                      loading={mutationState.loading}
                      {...props}
                      screenProps={screenProps}
                      setAccount={setAccount}
                      updateDeviceToken={updateDeviceToken}
                    />
                  );
                }}
              </Mutation>
            )}
          </Mutation>
        )}
      </AccountTypeContext.Consumer>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state) {
  return {
    deviceToken: state.deviceToken
  };
}

export default connect(mapStateToProps)(_LoginScreen);

const styles = {
  body: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 30
  },
  logo: {
    width: 200,
    height: 30,
    marginTop: 70,
    marginBottom: 30
  },
  inputContainer: {
    marginTop: 20
  },
  inputIcon: {
    color: Colors.signUp.input
  },
  textInput: {
    color: Colors.signUp.input,
    fontFamily: "Quicksand-Medium"
  },
  appButton: {
    marginTop: 10,
    alignSelf: "center"
  },
  lineSeperator: {
    height: 1,
    backgroundColor: "#fff",
    width: 200,
    marginVertical: 10
  },
  text: {
    fontSize: Fonts.signUp.defaultText,
    color: Colors.signUp.defaultText,
    textAlign: "center",
    marginTop: 10
  }
};
