import React from "react";
import { View, Platform } from "react-native";
import { Item, Input, Toast } from "native-base";
import { Query, Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import { MediumText, BoldText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import Fonts from "../../assets/Fonts";
import { ReduxContext } from "../../context/ReduxContext";
import LoadingView from "../../components/LoadingView";
import UIRenderer from "../../components/UIRenderer";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import PaymentMode from "../../components/PaymentMode";

class ContractInfo extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isPaymentModalVisible: false,
      onCardPaymentSelection: () => null
    };
  }

  static propTypes = {
    loading: PropTypes.bool,
    visits: PropTypes.any,
    isValidatingConsultation: PropTypes.bool,
    validateConsultation: PropTypes.func
  };

  validateConsultation = (mode, bankDirectFunc) => {
    let {
      navigation: {
        state: {
          params: {
            contract_details: { _id }
          }
        },
        navigate,
        goBack
      },
      validateConsultation,
      setPaystack,
      contract: { contract_id }
    } = this.props;

    this.setState({
      isPaymentModalVisible: false
    });

    validateConsultation({ data: { id: contract_id, mode } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (bankDirectFunc && code != 106) {
          bankDirectFunc(result);
        } else {
          this.setState(
            {
              isPaymentModalVisible: false
            },
            () => {
              if (code == 100) {
                setPaystack({ ...entity });
                navigate("paystack", {
                  operation: "createPaymentValidate",
                  refetchQueries: [
                    { operation: "getContract", config: { ID: _id } }
                  ],
                  onSuccess: () => goBack()
                });
              } else if (code == 106) {
                Util.logout(this.props.navigation);
              } else {
                Platform.OS !== "ios"
                  ? Alert.alert(
                      "Error Message",
                      desc,
                      [{ text: "Okay", onPress: () => null }],
                      { cancelable: false }
                    )
                  : Toast.show({
                      text: desc,
                      textStyle: ToastStyle.toast,
                      buttonStyle: ToastStyle.buttonStyle,
                      buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                      duration: 30000,
                      type: "danger",
                      buttonText: "OKAY"
                    });
              }
            }
          );
        }
      })
      .catch(err => {
        this.setState({ isPaymentModalVisible: false });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          duration: 10000,
          type: "danger",
          buttonText: "OKAY"
        });
      });
  };

  renderBadge = status => {
    let backgroundColor = status == 1 ? "green" : "orange";
    return (
      <View style={{ ...styles.badge, backgroundColor }}>
        <MediumText style={{ ...styles.text, ...styles.label }}>
          {status == 1 ? `ACTIVE` : `PENDING`}
        </MediumText>
      </View>
    );
  };

  renderBottom = () => {
    let {
      contract: { patient_name, patient_mobile, consultation_address }
    } = this.props;

    return (
      <View style={{ marginTop: 20 }}>
        <BoldText style={{ fontSize: 15 }}> Patient Details </BoldText>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            value={patient_name}
            placeholder="Patient Full Name"
            placeholderTextColor="#333"
            style={styles.textInput}
          />
        </Item>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            placeholder="Mobile No. of Patient"
            placeholderTextColor="#333"
            style={styles.textInput}
            value={patient_mobile}
            keyboardType="numeric"
          />
        </Item>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            placeholder="Home Address of Patient"
            placeholderTextColor="#333"
            value={consultation_address}
            style={styles.textInput}
          />
        </Item>
      </View>
    );
  };

  render() {
    let {
        navigation: {
          state: {
            params: {
              contract_details: { _id }
            }
          },
          navigate
        },
        loading,
        visits,
        isValidatingConsultation,
        contract: { contract_id, consultation_price, care_category, status },
        contract
      } = this.props,
      { isPaymentModalVisible, onCardPaymentSelection } = this.state,
      _visits = visits || {},
      _visitDetails = _visits.visits || {};

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {isValidatingConsultation && <LoadingView />}
        <React.Fragment>
          <PaymentMode
            visible={isPaymentModalVisible}
            onClose={() => this.setState({ isPaymentModalVisible: false })}
            onCardPaymentSelection={onCardPaymentSelection}
          />
          <AppHeader title={`Contract-${_id}`} />
          <UIRenderer
            loading={Object.keys(_visitDetails).length == 0 && loading}
            error={Object.keys(_visitDetails).length == 0 && this.props.error}
            onReload={() => this.props.refetchQuery()}
            timeElapseToMountIndicator={1000}
            isRefreshing={loading}
            containerType="plain"
            containerStyle={{ paddingTop: 10 }}
          >
            <View style={styles.row}>
              <MediumText style={styles.text}>Contract Status:</MediumText>
              {this.renderBadge(status)}
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Contract ID:</MediumText>
              <MediumText style={styles.text}> {contract_id} </MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Payment Status:</MediumText>
              {status !== 1 ? (
                <View style={styles.makePaymentContainer}>
                  <MediumText style={styles.text}>NOT PAID</MediumText>
                  <AppButton
                    style={{
                      ...styles.appButton,
                      ...styles.endContractButton,
                      marginLeft: 10
                    }}
                    title="Make Payment"
                    onPress={() =>
                      this.setState({
                        isPaymentModalVisible: true,
                        onCardPaymentSelection: (mode, bankDirectFunc) =>
                          this.validateConsultation(mode, bankDirectFunc)
                      })
                    }
                  />
                </View>
              ) : (
                <MediumText style={styles.text}>PAID</MediumText>
              )}
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Date of Next Visit:</MediumText>
              <MediumText style={{ ...styles.text, textAlign: "right" }}>
                {_visitDetails.next_visit == 1
                  ? _visitDetails.date
                  : `You do not have any scheduled visit`}
              </MediumText>
            </View>
            {_visitDetails.status == 0 &&
              status == 1 && (
                <AppButton
                  style={{ ...styles.appButton, ...styles.verifyVisitBtn }}
                  title="Verify visit"
                  onPress={() =>
                    navigate("verifyVisit", {
                      visit_details: contract,
                      next_visit: {
                        price: _visitDetails.price,
                        visit_id: _visitDetails._id
                      }
                    })
                  }
                />
              )}
            <View style={styles.row}>
              <MediumText style={styles.text}>Treatment Category:</MediumText>
              <MediumText style={styles.text}> {care_category} </MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>
                Initial Visit Agreed Price:
              </MediumText>
              <MediumText style={styles.text}>
                {Util.formatToNaira(consultation_price)}
              </MediumText>
            </View>
            {this.renderBottom()}
            <AppButton
              style={styles.viewVisitBtn}
              title="View visit plan"
              onPress={() => navigate("viewplansList", { contract_id: _id })}
            />
          </UIRenderer>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_ContractInfo = props => {
  let {
    navigation: {
      state: {
        params: {
          contract_details: { _id }
        }
      }
    }
  } = props;

  return (
    <ReduxContext.Consumer>
      {({ screenProps: { setPaystack } }) => (
        <Mutation operation="createConsultationPayment">
          {(mutationState, mutate) => (
            <Query
              operation="getNextVisit"
              options={{
                config: { params: { contract_id: _id } },
                fetchPolicy: "network-only"
              }}
            >
              {({ data, loading, error }, fetchMore, refetchQuery) => {
                return (
                  <Query
                    operation="getContract"
                    options={{ config: { ID: _id } }}
                  >
                    {contractDetails => (
                      <ContractInfo
                        loading={loading}
                        refetchQuery={refetchQuery}
                        visits={data && data.entity}
                        error={error}
                        isValidatingConsultation={mutationState.loading}
                        validateConsultation={mutate}
                        setPaystack={setPaystack}
                        contract={
                          (contractDetails.data &&
                            contractDetails.data.entity &&
                            contractDetails.data.entity.contracts) ||
                          {}
                        }
                        {...props}
                      />
                    )}
                  </Query>
                );
              }}
            </Query>
          )}
        </Mutation>
      )}
    </ReduxContext.Consumer>
  );
});

const styles = {
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 15
  },
  text: {
    fontSize: Fonts.smallText,
    color: "#333"
  },
  makePaymentContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  label: {
    color: "#fff"
  },
  badge: {
    marginLeft: 10,
    padding: 10,
    borderRadius: 10
  },
  endContractButton: {
    backgroundColor: "red"
  },
  appButton: {
    paddingVertical: 0,
    paddingHorizontal: 5,
    height: 30,
    borderRadius: 5
  },
  textInput: {
    fontSize: Fonts.smallText,
    fontFamily: "Quicksand-Medium"
  },
  inputContainer: {
    borderBottomColor: "#333"
  },
  viewVisitBtn: {
    alignSelf: "center",
    marginVertical: 10
  },
  verifyVisitBtn: {
    marginBottom: 15
  }
};
