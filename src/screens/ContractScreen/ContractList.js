import React from "react";
import PropTypes from "prop-types";
import { Query } from "react-kunyora";
import { connect } from "react-redux";
import moment from "moment";
import { Text } from "react-native";

import { FlatListItem } from "../../components/ListItem";
import NavigationProvider from "../../containers/NavigationProvider";
import AppList from "../../components/AppList";
import Filter from "../../components/Contract/Filter";

class ContractList extends React.PureComponent {
  state = {
    listFilterer: undefined
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    accountType: PropTypes.string,
    contracts: PropTypes.object,
    error: PropTypes.any,
    isFilterModalVisible: PropTypes.bool,
    hideFilterModal: PropTypes.func
  };

  filterList = (state, array) =>
    array.filter((c, i) => {
      if (state == "active") {
        return c.status == 1;
      } else if (state == "pending") {
        return c.status == 0;
      }
      return c;
    });

  orderList = state => {
    this.setState(
      {
        listFilterer:
          state == "active" || state == "pending"
            ? items => this.filterList(state, items)
            : undefined
      },
      () => this.props.hideFilterModal()
    );
  };

  render() {
    let {
      navigation: { navigate },
      loading,
      contracts,
      accountType,
      refetchQuery
    } = this.props;

    let _contracts =
      contracts &&
      contracts.contracts
        .sort((a, b) => {
          return new Date(a.created).getTime() - new Date(b.created).getTime();
        })
        .reverse();

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <Filter
          isVisible={this.props.isFilterModalVisible}
          onClose={this.orderList}
        />
        <React.Fragment>
          <AppList
            dataArray={_contracts}
            storageKey="@contractList"
            reduxActionID="setContractList"
            reduxStateID="contractList"
            listFilterer={this.state.listFilterer}
            loading={loading}
            error={this.props.error}
            refetchQuery={() => refetchQuery()}
            timeElapseToMountIndicator={1000}
            containerType="plain"
            fetchMore={() => null}
            isFetchingMore={this.props.isInitialDataSet && this.props.loading}
            renderRow={list => {
              let {
                patient: { patient_id, first_name },
                contract_id,
                created,
                care_category,
                carer_name
              } = list;

              return (
                <FlatListItem
                  type="icon"
                  iconName="ios-person"
                  title={
                    accountType == "carer"
                      ? `${first_name}`
                      : carer_name.trim().split(" ")[0]
                  }
                  description={
                    accountType == "carer"
                      ? `Patient ID: ${patient_id}`
                      : `Contract ID: ${contract_id}`
                  }
                  date={moment(created).format("MMM Do, YYYY")}
                  tag={care_category}
                  onPress={() =>
                    navigate(
                      accountType == "carer"
                        ? "carerContractDetails"
                        : "contractInfo",
                      { contract_details: list }
                    )
                  }
                />
              );
            }}
          />
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _ContractList = props => (
  <Query operation="getContract" options={{ fetchPolicy: "network-only" }}>
    {({ data, loading, error, isInitialDataSet }, fetchMore, refetchQuery) => (
      <ContractList
        contracts={data && data.entity}
        loading={loading}
        refetchQuery={refetchQuery}
        error={error}
        isInitialDataSet={isInitialDataSet}
        {...props}
      />
    )}
  </Query>
);

function mapStateToProps(state) {
  return {
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_ContractList);
