import React from "react";
import { View } from "react-native";
import { Input, Item, Icon } from "native-base";
import { Query } from "react-kunyora";
import PropTypes from "prop-types";
import Call from "react-native-phone-call";

import { MediumText, BoldText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import Fonts from "../../assets/Fonts";
import UIRenderer from "../../components/UIRenderer";
import { Util } from "../../utils";

class CarerContractDetails extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    visits: PropTypes.any
  };

  makeCall = phoneNumber => {
    const args = { number: phoneNumber, prompt: false };
    Call(args).catch(err => alert(err));
  };

  renderBadge = status => {
    let backgroundColor = status == 1 ? "green" : "orange";
    return (
      <View style={{ ...styles.badge, backgroundColor }}>
        <MediumText style={{ ...styles.text, ...styles.label }}>
          {status == 1 ? ` Active` : ` PENDING`}
        </MediumText>
      </View>
    );
  };

  renderBottom = () => {
    let {
      navigation: {
        state: {
          params: {
            contract_details: {
              patient: { first_name },
              patient_mobile,
              consultation_address,
              _id,
              patient_name
            },
            next_visit,
            contract_details
          }
        },
        navigate
      }
    } = this.props;

    return (
      <View style={{ marginTop: 20 }}>
        <BoldText style={{ fontSize: 15 }}> Patient Details </BoldText>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            placeholder="Patient Full Name"
            value={`${patient_name}`}
            placeholderTextColor="#333"
            style={styles.textInput}
          />
        </Item>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            placeholder="Mobile No. of Patient"
            placeholderTextColor="#333"
            value={patient_mobile}
            style={styles.textInput}
            keyboardType="numeric"
          />
        </Item>
        <Item style={styles.inputContainer}>
          <Input
            editable={false}
            placeholder="Home Address of Patient"
            placeholderTextColor="#333"
            value={consultation_address}
            style={styles.textInput}
          />
        </Item>
        <View style={styles.callContainer}>
          <AppButton
            title={`Call ${first_name.trim()}`}
            iconName="md-call"
            style={styles.callBtn}
            onPress={() => this.makeCall(patient_mobile)}
          />
        </View>
        <View style={styles.bottomButton}>
          <AppButton
            style={styles.appButton}
            title="Create Visit Plan"
            onPress={() =>
              navigate("carerCreateVisitPlan", {
                patient_name: `${first_name}`,
                contract_id: _id
              })
            }
          />
          <AppButton
            style={styles.appButton}
            title="View Visit Plans"
            onPress={() => navigate("viewplansList", { contract_id: _id })}
          />
        </View>
      </View>
    );
  };

  render() {
    let {
        navigation: {
          state: {
            params: {
              contract_details: {
                contract_id,
                status,
                consultation_visit,
                consultation_price,
                care_category
              },
              contract_details
            }
          },
          navigate
        },
        loading,
        error,
        visits,
        refetchQuery
      } = this.props,
      _visits = visits || {},
      _visitDetails = _visits.visits || {};

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title={`Contract ${contract_id}`} />
          <UIRenderer
            loading={Object.keys(_visitDetails).length == 0 && loading}
            error={Object.keys(_visitDetails).length == 0 && error}
            timeElapseToMountIndicator={1000}
            onReload={() => refetchQuery()}
            isRefreshing={loading}
            containerType="plain"
            containerStyle={{ paddingTop: 10 }}
          >
            <View style={styles.row}>
              <MediumText style={styles.text}>Contract Status:</MediumText>
              {this.renderBadge(status)}
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Contract ID:</MediumText>
              <MediumText style={styles.text}>{contract_id}</MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Payment Status:</MediumText>
              <MediumText style={styles.text}>
                {status == 1 ? `  PAID` : `  Awaiting Patient to make payment`}
              </MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Date of Next Visit:</MediumText>
              <MediumText style={styles.text}>
                {_visitDetails.next_visit == 1
                  ? _visitDetails.date
                  : `You do not have any scheduled visit`}
              </MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>Treatment Category:</MediumText>
              <MediumText style={styles.text}> {care_category}</MediumText>
            </View>
            <View style={styles.row}>
              <MediumText style={styles.text}>
                Initial Visit Agreed Price:
              </MediumText>
              <MediumText style={styles.text}>
                {Util.formatToNaira(consultation_price)}
              </MediumText>
            </View>
            {this.renderBottom()}
          </UIRenderer>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CarerContractDetails = props => {
  let {
    navigation: {
      state: {
        params: {
          contract_details: { _id },
          contract_details
        }
      }
    }
  } = props;

  return (
    <Query
      operation="getNextVisit"
      options={{ config: { params: { contract_id: _id } } }}
    >
      {({ data, loading, error }, fetchMore, refetchQuery) => {
        return (
          <CarerContractDetails
            loading={loading}
            visits={data && data.entity}
            error={error}
            refetchQuery={refetchQuery}
            {...props}
          />
        );
      }}
    </Query>
  );
});

const styles = {
  row: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 15,
    justifyContent: "space-between"
  },
  text: {
    fontSize: Fonts.smallText,
    color: "#333"
  },
  label: {
    color: "#fff"
  },
  badge: {
    marginLeft: 10,
    padding: 10,
    borderRadius: 10
  },
  appButton: {
    marginTop: 10,
    alignSelf: "center",
    marginHorizontal: 5,
    marginVertical: 10,
    paddingHorizontal: 10
  },
  bottomButton: {
    flexDirection: "row",
    justifyContent: "center"
  },
  textInput: {
    fontSize: Fonts.smallText,
    fontFamily: "Quicksand-Medium"
  },
  inputContainer: {
    borderBottomColor: "#333"
  },
  callContainer: {
    flex: 1,
    flexDirection: "row"
  },
  callBtn: {
    backgroundColor: "green",
    marginVertical: 10,
    flex: 1
  }
};
