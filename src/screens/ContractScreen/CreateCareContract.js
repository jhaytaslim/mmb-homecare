import React from "react";
import {
  View,
  Picker,
  Platform,
  DatePickerAndroid,
  DatePickerIOS,
  Alert,
  PermissionsAndroid,
  ToastAndroid
} from "react-native";
import { Item, Input, Icon, Toast, Content } from "native-base";
import { Mutation, Query } from "react-kunyora";
import PropTypes from "prop-types";
import moment from "moment";
import RNFetchBlob from "react-native-fetch-blob";

import { MediumText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import PaddingProvider from "../../containers/PaddingProvider";
import AppHeader from "../../components/AppHeader";
import Fonts from "../../assets/Fonts";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class CreateCareContract extends React.PureComponent {
  state = {
    care_category: "",
    care_type: "Initial Consultation",
    consultation_price: "0",
    consultation_visit:
      Platform.OS == "android"
        ? `Consultation visit date | ${moment().format(
            "YYYY"
          )}-${moment().format("MM")}-${moment().format("DD")}`
        : new Date(),
    patient_name: "",
    patient_mobile: "",
    consultation_address: ""
  };

  static propTypes = {
    createContract: PropTypes.func,
    loading: PropTypes.bool,
    specialties: PropTypes.any,
    specialtiesLoading: PropTypes.bool
  };

  setDate = () => {
    if (Platform.OS == "android") {
      DatePickerAndroid.open({
        date: new Date(this.state.consultation_visit.split("|")[1].trim())
      })
        .then(({ action, year, month, day }) => {
          if (action !== DatePickerAndroid.dismissedAction) {
            let _month =
                (month + 1).toString().length == 1
                  ? `0${month + 1}`
                  : month + 1,
              _day = day.toString().length == 1 ? `0${day}` : day;
            this.setState({
              consultation_visit: `consultation_visit | ${year}-${_month}-${_day}`
            });
          }
        })
        .catch(({ code, message }) => {
          //@Todo send to app center
        });
    }
  };

  downloadPriceGuideOnAndroid = () => {
    ToastAndroid.showWithGravity(
      "Starting Price Guide download...",
      ToastAndroid.LONG,
      ToastAndroid.CENTER
    );

    RNFetchBlob.config({
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        title: "The MMB Homecare Price Guide",
        description:
          "Price guide and listing for services on the MMB Homecare platform",
        mime: "application/pdf",
        mediaScannable: true,
        path: `${RNFetchBlob.fs.dirs.DownloadDir}/priceGuide.pdf`
      }
    })
      .fetch("GET", "https://www.mmbhomecare.com/documents/priceGuide.pdf")
      .then(res => {
        ToastAndroid.showWithGravity(
          "Price Guide downloaded in Download directory",
          ToastAndroid.SHORT,
          ToastAndroid.CENTER
        );

        RNFetchBlob.android.actionViewIntent(res.path(), "application/pdf");
      })
      .catch(err => {
        Alert.alert(
          "Error Message",
          err,
          [{ text: "Okay", onPress: () => null }],
          { cancelable: false }
        );
      });
  };

  downloadPriceGuideOnIOS = () => {
    RNFetchBlob.config({
      fileCache: true,
      path: `${RNFetchBlob.fs.dirs.DocumentDir}/priceGuide.pdf`
    })
      .fetch("GET", "https://www.mmbhomecare.com/documents/priceGuide.pdf")
      .then(res => {
        RNFetchBlob.ios.openDocument(res.path());
      })
      .catch(err =>
        Alert.alert(
          "Error Message",
          err,
          [{ text: "Okay", onPress: () => null }],
          { cancelable: false }
        )
      );
  };

  downloadPriceGuide = () => {
    if (Platform.OS == "android") {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      ).then(isPermissionGranted => {
        if (!isPermissionGranted) {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: "MMB HomeCare File Permission",
              message:
                "MMB Homecare needs access to your file manager to save the price guide"
            }
          )
            .then(granted => {
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.downloadPriceGuideOnAndroid();
              } else {
                alert(
                  "File Permission Denied, Please turn on permission to allow file download"
                );
              }
            })
            .catch(err => alert("File Permission denied"));
        } else {
          this.downloadPriceGuideOnAndroid();
        }
      });
    } else {
      this.downloadPriceGuideOnIOS();
    }
  };

  createContract = () => {
    let {
        createContract,
        navigation: {
          state: {
            params: { convo_id }
          },
          goBack
        }
      } = this.props,
      {
        care_category,
        consultation_price,
        patient_name,
        patient_mobile,
        consultation_address
      } = this.state;

    if (care_category.trim() == "") {
      alert("Cannot create a contract without a care category");
      return;
    }

    if (
      patient_mobile.trim() == "" ||
      /^[0-9\+\-]{6,}$/.test(patient_mobile) == false
    ) {
      alert("Mobile number can only contain numbers and (+,-) signs");
      return;
    }

    if (Number(consultation_price) == 0) {
      alert("Consultation price cannot be 0");
      return;
    }

    if (patient_name.trim() == "") {
      alert("The patient name cannot be empty");
      return;
    }

    if (consultation_address.trim() == "") {
      alert("The consultation address cannot be empty");
      return;
    }

    createContract({
      data: {
        ...this.state,
        consultation_visit: this.state.consultation_visit.split("|")[1].trim(),
        convo_id
      }
    })
      .then(result => {
        let {
          status: { code, desc }
        } = result;
        if (code == 100) {
          Toast.show({
            text: `${desc}`,
            type: "success",
            textStyle: ToastStyle.toast,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle,
            duration: 10000,
            buttonText: "OKAY"
          });
          goBack();
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                duration: 30000,
                type: "danger",
                buttonText: "OKAY"
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          duration: 10000,
          type: "danger",
          buttonText: "OKAY"
        })
      );
  };

  render() {
    let { specialties, loading } = this.props,
      _specialties = specialties || {},
      _entitySpecialities = _specialties.entity || {},
      _specialtiesArr = _entitySpecialities.specialties || [];

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Create Care Contract" />
          <Content>
            <PaddingProvider style={{ backgroundColor: "#e44746" }}>
              <MediumText
                style={{
                  ...styles.title,
                  ...styles.text,
                  textAlign: "center",
                  marginTop: 10
                }}
              >
                Care Category
              </MediumText>
              <Picker
                selectedValue={this.state.care_category}
                style={styles.picker}
                mode="dropdown"
                onValueChange={care_category =>
                  this.setState({ care_category })
                }
              >
                {["select a category", ..._specialtiesArr].map(
                  (specialties, i) => {
                    let label = specialties.replace(/<[^>]*>/gi, "");
                    return (
                      <Picker.Item
                        key={i}
                        label={label}
                        value={label == "select a category" ? "" : label}
                      />
                    );
                  }
                )}
              </Picker>

              <MediumText
                style={{ ...styles.title, ...styles.text, textAlign: "center" }}
              >
                Care Type
              </MediumText>
              <Picker
                style={styles.picker}
                selectedValue={this.state.care_type}
                mode="dropdown"
                onValueChange={care_type => this.setState({ care_type })}
              >
                <Picker.Item
                  label="Initial Consultation"
                  value="Initial Consultation"
                />
              </Picker>

              <View style={styles.container}>
                <MediumText style={{ ...styles.title, ...styles.text }}>
                  Initial Consultation Price (N)
                </MediumText>
                <Item>
                  <Input
                    value={this.state.consultation_price}
                    onChangeText={consultation_price =>
                      this.setState({ consultation_price })
                    }
                    style={styles.input}
                    keyboardType="numeric"
                    placeholder="e.g. 1000"
                    placeholderTextColor="#fff"
                  />
                </Item>
              </View>
              <MediumText
                style={styles.priceGuide}
                onPress={this.downloadPriceGuide}
              >
                Download Price Guide
              </MediumText>
              <View style={{ ...styles.container, ...styles.dateContainer }}>
                <MediumText style={{ ...styles.title, ...styles.text }}>
                  Consultation Date
                </MediumText>
                {Platform.OS == "android" ? (
                  <NativeTouchableFeedback onPress={() => this.setDate()}>
                    <View style={styles.datePicker}>
                      <MediumText style={{ color: "#fff" }}>
                        {this.state.consultation_visit}
                      </MediumText>
                    </View>
                  </NativeTouchableFeedback>
                ) : (
                  <DatePickerIOS
                    date={this.state.consultation_visit}
                    onDateChange={consultation_visit =>
                      this.setState({ consultation_visit })
                    }
                  />
                )}
              </View>
              <MediumText style={{ ...styles.text, ...styles.tagLine }}>
                Patient Details
              </MediumText>
              <Item>
                <Input
                  value={this.state.patient_name}
                  onChangeText={patient_name => this.setState({ patient_name })}
                  style={styles.input}
                  placeholder="Patient Full Name"
                  placeholderTextColor="#fff"
                />
              </Item>
              <Item>
                <Input
                  value={this.state.patient_mobile}
                  onChangeText={patient_mobile =>
                    this.setState({ patient_mobile })
                  }
                  keyboardType="numeric"
                  style={styles.input}
                  placeholder="Mobile No. of Patient"
                  placeholderTextColor="#fff"
                />
              </Item>
              <Item>
                <Input
                  style={styles.input}
                  value={this.state.consultation_address}
                  onChangeText={consultation_address =>
                    this.setState({ consultation_address })
                  }
                  placeholder="Home Address of Patient"
                  placeholderTextColor="#fff"
                />
              </Item>
              <AppButton
                onPress={this.createContract}
                style={styles.appButton}
                title="Create"
              />
            </PaddingProvider>
          </Content>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CreateCareContract = props => (
  <Mutation operation="createContract">
    {(mutationState, mutate) => (
      <Query operation="getCarerSpecialties">
        {result => (
          <CreateCareContract
            createContract={mutate}
            loading={mutationState.loading}
            specialties={result.data}
            specialtiesLoading={result.loading}
            {...props}
          />
        )}
      </Query>
    )}
  </Mutation>
));

const styles = {
  container: {
    alignItems: "center"
  },
  icon: {
    color: "#fff"
  },
  dateContainer: {
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#fff"
  },
  title: {
    fontSize: 16,
    marginTop: 10
  },
  text: {
    color: "#fff"
  },
  input: {
    fontSize: Fonts.smallText,
    color: "#fff",
    textAlign: "center"
  },
  tagLine: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 17
  },
  appButton: {
    alignSelf: "center",
    marginVertical: 10
  },
  picker: {
    flex: 1,
    color: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  datePicker: {
    marginTop: 10
  },
  priceGuide: {
    marginVertical: 10,
    color: "#fff",
    textDecorationLine: "underline",
    textAlign: "center"
  }
};
