import React from "react";
import { View, StyleSheet, Alert, Platform } from "react-native";
import { Item, Input, Icon, Picker, Toast, Content, Label } from "native-base";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Mutation } from "react-kunyora";

import { MediumText, BoldText, LightText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import LoadingView from "../../components/LoadingView";
import { ReduxContext } from "../../context/ReduxContext";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import PaymentMode from "../../components/PaymentMode";
import TabsNavigationContext from "../../context/TabsNavigationContext";

class CarerPaymentInformation extends React.PureComponent {
  constructor(props) {
    super(props);

    let { currentCarer } = props;

    this.state = {
      account_type: currentCarer.account_type || "",
      account_name: currentCarer.account_name || "",
      account_number: currentCarer.account_number || "",
      bank_name: currentCarer.bank_name || "",
      other_info: currentCarer.other_info || "",
      isPaymentModalVisible: false,
      onCardPaymentSelection: () => null
    };
  }

  static propTypes = {
    accountType: PropTypes.string,
    currentCarer: PropTypes.object,
    currentUser: PropTypes.object,
    loading: PropTypes.bool,
    isPaymentVerificationLoading: PropTypes.bool,
    verifyPayment: PropTypes.func,
    setPaystack: PropTypes.func,
    paystackLoading: PropTypes.bool
  };

  static banks = [
    "Select Bank",
    "Access Bank",
    "Fidelity Bank",
    "GT bank",
    "Stanbic IBTC Bank",
    "Keystone Bank",
    "Skye Bank",
    "Spring Bank",
    "Sterling Bank",
    "Diamond Bank",
    "UBA",
    "First Bank",
    "Union Bank",
    "Ecobank",
    "Zenith Bank",
    "First City Monumental Bank",
    "Wema Bank",
    "Heritage Bank",
    "Stanbic Chartered Bank"
  ];

  verifyPayment = (mode, bankDirectFunc) => {
    let {
      verifyPayment,
      navigation: { navigate },
      setPaystack
    } = this.props;

    this.setState({ isPaymentModalVisible: false });

    verifyPayment({ data: { type: "CARER_VERIFY", mode } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (bankDirectFunc && code != 106) {
          bankDirectFunc(result);
        } else {
          this.setState(
            {
              isPaymentModalVisible: false
            },
            () => {
              if (code == 100) {
                setPaystack({ ...entity });
                navigate("paystack", {
                  operation: "createPaymentValidate",
                  refetchQueries: [{ operation: "getCarer" }],
                  onSuccess: () => navigate("carerPaymentInfo")
                });
              } else if (code == 106) {
                Util.logout(this.props.navigation);
              } else {
                Toast.show({
                  text: `${desc}`,
                  textStyle: ToastStyle.toast,
                  duration: 10000,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                  type: "danger",
                  buttonText: "okay"
                });
              }
            }
          );
        }
      })
      .catch(err => {
        this.setState({ isPaymentModalVisible: false });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      });
  };

  //@Todo Add form client side validation to the code
  updatePaymentInformation = () => {
    let {
        navigation: { navigate },
        updatePayment,
        currentCarer: { verify_status }
      } = this.props,
      extra_message = "";

    if (verify_status == 0) {
      extra_message = "Please proceed to make payments to verify your account";
    } else if (verify_status == 3) {
      extra_message = "Account has been blocked";
    } else {
      extra_message = "and carer setup process completed";
    }

    updatePayment({ data: { ...this.state } })
      .then(result => {
        let {
          status: { code, desc }
        } = result;
        if (code == 100) {
          Toast.show({
            text: `${desc} , ${extra_message}`,
            textStyle: ToastStyle.toast,
            duration: 20000,
            type: "success",
            buttonText: "okay",
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
          this.props.onChangeTab(0);
          navigate("tabRoutes");
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                type: "danger",
                buttonText: "okay"
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          type: "danger",
          buttonText: "okay",
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  getVerificationStatus = status => {
    let type = undefined,
      color = undefined;

    switch (status) {
      case 0:
        type = "NOT ACTIVE";
        color = "red";
        break;
      case 1:
        type = "VERIFIED";
        color = "green";
        break;
      case 2:
        type = "PENDING";
        color = "#e65100";
        break;
      case 3:
        type = "BLOCKED";
        color = "red";
        break;
      default:
        type = "LOADING...";
        color = "green";
        break;
    }
    return { type, color };
  };

  render() {
    let {
        navigation: { navigate },
        navigation,
        accountType,
        currentCarer: { verify_status, carer_id },
        currentUser: { created },
        loading,
        paystackLoading
      } = this.props,
      {
        account_type,
        account_name,
        account_number,
        bank_name,
        other_info,
        isPaymentModalVisible,
        onCardPaymentSelection
      } = this.state;

    return (
      <NavigationProvider navigation={navigation}>
        {(loading || paystackLoading) && <LoadingView />}
        <React.Fragment>
          <PaymentMode
            visible={isPaymentModalVisible}
            onClose={() => this.setState({ isPaymentModalVisible: false })}
            onCardPaymentSelection={onCardPaymentSelection}
          />
          <AppHeader
            title="Payment Information"
            onPressBackButton={() => navigate("carerSetup")}
          />
          <PaddingProvider type="plain">
            <Content showsVerticalScrollIndicator={false}>
              <MediumText style={styles.heading}>
                Account Information
              </MediumText>

              {verify_status == 0 && (
                <MediumText style={styles.newUserMessage}>
                  A Sum of ₦10,000 will be required to process your
                  verification. ₦5,000 will be processed for the first 300
                  carers.
                </MediumText>
              )}

              <View style={styles.marginTopXs}>
                <View style={[styles.flRow, styles.marginTopXs, styles.jcBtw]}>
                  <MediumText style={styles.blackText}>
                    Carer Verification Status:
                  </MediumText>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <BoldText
                      style={{
                        color: this.getVerificationStatus(verify_status).color
                      }}
                    >
                      {this.getVerificationStatus(verify_status).type}
                    </BoldText>
                  </View>
                </View>
                {parseInt(verify_status) == 0 && (
                  <AppButton
                    style={styles.verifyBtn}
                    title="Make Payment"
                    onPress={() =>
                      this.setState({
                        isPaymentModalVisible: true,
                        onCardPaymentSelection: (mode, bankDirectFunc) =>
                          this.verifyPayment(mode, bankDirectFunc)
                      })
                    }
                  />
                )}
                <View style={[styles.flRow, styles.marginTopXs, styles.jcBtw]}>
                  <MediumText style={styles.blackText}>
                    Account Type:
                  </MediumText>
                  <MediumText style={styles.blackText}>
                    {accountType}
                  </MediumText>
                </View>
                <View style={[styles.flRow, styles.marginTopXs, styles.jcBtw]}>
                  <MediumText style={styles.blackText}>Carer ID:</MediumText>
                  <MediumText style={styles.blackText}>{carer_id}</MediumText>
                </View>
                <View style={[styles.flRow, styles.marginTopXs, styles.jcBtw]}>
                  <MediumText style={styles.blackText}>Date Joined:</MediumText>
                  <MediumText style={styles.blackText}>
                    {created.split(" ")[0].trim()}
                  </MediumText>
                </View>
              </View>

              <View>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Bank Name </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.bank_name}
                    onValueChange={bank_name => this.setState({ bank_name })}
                  >
                    {CarerPaymentInformation.banks.map((bank, i) => (
                      <Picker.Item
                        key={i}
                        label={bank}
                        value={bank == "Select Bank" ? "" : bank}
                      />
                    ))}
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Account Type </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.account_type}
                    onValueChange={account_type =>
                      this.setState({ account_type })
                    }
                  >
                    <Picker.Item label="Select account type" value="" />
                    <Picker.Item label="Savings" value="savings" />
                    <Picker.Item label="Current" value="current" />
                  </Picker>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Account Name </Label>
                    <Input
                      value={this.state.account_name}
                      style={styles.textInput}
                      onChangeText={account_name =>
                        this.setState({ account_name })
                      }
                      placeholder="Account Name"
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Account Number </Label>
                    <Input
                      value={this.state.account_number}
                      style={styles.textInput}
                      onChangeText={account_number =>
                        this.setState({ account_number })
                      }
                      placeholder="Account Number"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Other Info </Label>
                    <Input
                      value={this.state.other_info}
                      style={styles.textInput}
                      onChangeText={other_info => this.setState({ other_info })}
                      placeholder="Other Info"
                    />
                  </Item>
                </View>
                <View style={styles.appButtonContainer}>
                  <View style={styles.updateBtnContainer}>
                    <AppButton
                      title="Update"
                      style={StyleSheet.flatten([
                        styles.appButton,
                        styles.carerBtnSpacing,
                        styles.updateBtn
                      ])}
                      onPress={this.updatePaymentInformation}
                    />
                  </View>
                  <AppButton
                    onPress={() => this.props.navigation.navigate("carerSetup")}
                    style={StyleSheet.flatten([
                      styles.appButton,
                      styles.carerBtnSpacing,
                      styles.backBtn
                    ])}
                    title="back"
                  />
                </View>
              </View>
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _CarerPaymentInformation = props => (
  <ReduxContext.Consumer>
    {({ screenProps: { setPaystack } }) => (
      <TabsNavigationContext.Consumer>
        {({ onChangeTab }) => (
          <Mutation
            operation="updatePaymentInformation"
            options={{ refetchQueries: [{ operation: "getCarer" }] }}
          >
            {(mutationState, mutate) => (
              <Mutation operation="createCarerPayments">
                {(carerPaymentState, verificationPayment) => (
                  <CarerPaymentInformation
                    loading={mutationState.loading}
                    paystackLoading={carerPaymentState.loading}
                    updatePayment={mutate}
                    isPaymentVerificationLoading={carerPaymentState.loading}
                    verifyPayment={verificationPayment}
                    {...props}
                    setPaystack={setPaystack}
                    onChangeTab={onChangeTab}
                  />
                )}
              </Mutation>
            )}
          </Mutation>
        )}
      </TabsNavigationContext.Consumer>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state) {
  return {
    currentCarer: state.currentCarer,
    currentUser: state.currentUser,
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_CarerPaymentInformation);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 20
  },
  newUserMessage: {
    marginTop: 20,
    color: "green",
    textAlign: "center"
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  appButton: {
    marginVertical: 20,
    paddingHorizontal: 15
  },
  label: {
    color: "#333",
    fontSize: 13,
    fontFamily: "Quicksand-Light"
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  appButtonContainer: {
    marginTop: 10,
    marginVertical: 10
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  verifyBtn: {
    paddingVertical: 0,
    paddingHorizontal: 5,
    height: 30,
    borderRadius: 5,
    alignSelf: "flex-end"
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
});
