import React from "react";
import { View, StyleSheet, Alert, Platform } from "react-native";
import {
  List,
  Item,
  Input,
  Icon,
  Button,
  Picker,
  Form,
  Textarea,
  Toast,
  Content,
  Label
} from "native-base";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Query, Mutation } from "react-kunyora";

import { BoldText, LightText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import TabsNavigationContext from "../../context/TabsNavigationContext";

class ProfileDetailsTwo extends React.PureComponent {
  constructor(props) {
    super(props);

    let {
      currentUser: {
        religion,
        address,
        next_of_kin_name,
        next_of_kin_mobile,
        next_of_kin_address,
        next_of_kin_relationship
      }
    } = props;

    this.state = {
      religion: religion || "",
      address: address || "",
      next_of_kin_name: next_of_kin_name || "",
      next_of_kin_mobile: next_of_kin_mobile || "",
      next_of_kin_address: next_of_kin_address || "",
      next_of_kin_relationship: next_of_kin_relationship || "",
      error: ""
    };
  }

  static propTypes = {
    currentUser: PropTypes.object,
    accountType: PropTypes.string,
    updateUser: PropTypes.func,
    loading: PropTypes.bool
  };

  onReligionChange = value => {
    this.setState({
      religion: value
    });
  };

  _onValidateTextValue = (text, type) => {
    this.setState({ address: text });
  };

  updateUser = () => {
    let {
        updateUser,
        navigation: {
          state: {
            params: { userDetails }
          }
        }
      } = this.props,
      {
        religion: sr,
        address: ha,
        next_of_kin_relationship,
        next_of_kin_name,
        next_of_kin_mobile,
        next_of_kin_address
      } = this.state,
      data = {
        ...userDetails,
        religion: sr || "",
        address: ha || "",
        next_of_kin_relationship,
        next_of_kin_name,
        next_of_kin_mobile,
        next_of_kin_address
      };

    updateUser({ data })
      .then(result => {
        let {
          status: { code, desc }
        } = result;
        if (parseInt(code) == 100) {
          Toast.show({
            text: desc,
            textStyle: ToastStyle.toast,
            type: "success",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  render() {
    let {
      navigation: { navigate },
      navigation,
      loading,
      onChangeTab
    } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader
            title="Profile"
            onPressBackButton={() => {
              onChangeTab(0);
              navigate("tabRoutes");
            }}
          />
          <Content>
            <PaddingProvider type="plain">
              <BoldText style={styles.heading}>PROFILE DETAILS</BoldText>

              <View style={styles.marginTopXs}>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Religion </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.religion.toLowerCase()}
                    onValueChange={this.onReligionChange}
                  >
                    <Picker.Item label="Select religion" value="" />
                    <Picker.Item label="Christianity" value="christianity" />
                    <Picker.Item label="Islam" value="islam" />
                    <Picker.Item label="Atheism" value="atheism" />
                    <Picker.Item label="Traditional" value="traditional" />
                    <Picker.Item label="No Religion" value="no religion" />
                  </Picker>
                </View>

                <View style={styles.marginTopXs}>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.address ? styles.error : null}
                  >
                    <Label style={styles.label}> Home Address </Label>
                    <Input
                      placeholder="Home Address"
                      value={this.state.address}
                      style={styles.textInput}
                      onChangeText={home =>
                        this._onValidateTextValue(home, "home address")
                      }
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Next of Kin name </Label>
                    <Input
                      value={this.state.next_of_kin_name}
                      style={styles.textInput}
                      onChangeText={value =>
                        this.setState({ next_of_kin_name: value })
                      }
                      placeholder="Next of Kin Full Name"
                    />
                  </Item>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}>
                    Relationship with next of kin
                  </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.next_of_kin_relationship.toLowerCase()}
                    onValueChange={itemValue =>
                      this.setState({ next_of_kin_relationship: itemValue })
                    }
                  >
                    <Picker.Item label="Relationship to next of kin" value="" />
                    <Picker.Item label="Father" value="father" />
                    <Picker.Item label="Mother" value="mother" />
                    <Picker.Item label="Guardian" value="guardian" />
                    <Picker.Item label="Sibling" value="sibling" />
                    <Picker.Item label="Spouse" value="spouse" />
                  </Picker>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Phone Number of next of kin
                    </Label>
                    <Input
                      style={styles.textInput}
                      value={this.state.next_of_kin_mobile}
                      onChangeText={itemValue =>
                        this.setState({ next_of_kin_mobile: itemValue })
                      }
                      placeholder="Phone Number of Next of Kin"
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Home Address of next of kin
                    </Label>
                    <Input
                      value={this.state.next_of_kin_address}
                      style={styles.textInput}
                      onChangeText={value =>
                        this.setState({ next_of_kin_address: value })
                      }
                      placeholder="Next of Kin Address"
                    />
                  </Item>
                </View>
              </View>
              {this.state.error ? (
                <BoldText
                  style={{
                    color: "red",
                    textAlign: "center",
                    paddingVertical: 10
                  }}
                >
                  {this.state.error}
                </BoldText>
              ) : null}
              <View style={styles.appButtonContainer}>
                <View style={styles.updateBtnContainer}>
                  <AppButton
                    title="Save & Continue"
                    style={StyleSheet.flatten([
                      styles.appButton,
                      styles.carerBtnSpacing,
                      styles.updateBtn
                    ])}
                    onPress={this.updateUser}
                  />
                </View>
                <AppButton
                  onPress={() => {
                    onChangeTab(0);
                    this.props.navigation.navigate("tabRoutes");
                  }}
                  style={StyleSheet.flatten([
                    styles.appButton,
                    styles.carerBtnSpacing,
                    styles.backBtn
                  ])}
                  title="back"
                />
              </View>
            </PaddingProvider>
          </Content>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _ProfileDetailsTwo = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <Mutation
        operation="updateUser"
        options={{ refetchQueries: [{ operation: "getUserDetails" }] }}
      >
        {(mutateState, mutate) => (
          <ProfileDetailsTwo
            updateUser={mutate}
            loading={mutateState.loading}
            onChangeTab={onChangeTab}
            {...props}
          />
        )}
      </Mutation>
    )}
  </TabsNavigationContext.Consumer>
);

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_ProfileDetailsTwo);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 20
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  label: {
    color: "#333",
    fontSize: 13,
    fontFamily: "Quicksand-Light"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  iconColor: {
    color: "#7c8a8c"
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  appButtonContainer: {
    marginVertical: 10
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  appButton: {
    alignSelf: "center",
    paddingHorizontal: 15
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
});
