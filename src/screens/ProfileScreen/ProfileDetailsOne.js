import React from "react";
import {
  View,
  StyleSheet,
  AsyncStorage,
  Picker,
  Alert,
  Platform
} from "react-native";
import { Item, Input, CheckBox, Toast, Content, Label } from "native-base";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Mutation, Query } from "react-kunyora";

import { BoldText, LightText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class ProfileDetailsOne extends React.PureComponent {
  constructor(props) {
    super(props);

    let {
      currentUser: {
        first_name,
        gender,
        middle_name,
        last_name,
        marital_status,
        nationality,
        mobile,
        mobile2,
        email,
        occupation,
        state,
        city
      }
    } = props;

    this.state = {
      first_name: first_name || "",
      gender: {
        male: gender == "male" ? true : false,
        female: gender == "female" ? true : false
      },
      middle_name: middle_name || "",
      last_name: last_name || "",
      email: email || "",
      marital_status: marital_status || "select marital status",
      nationality: nationality || "Nigeria",
      mobile: mobile ? mobile.replace(/\s/gi, "") : "",
      mobile2: mobile2 ? mobile2.replace(/\s/gi, "") : "",
      occupation: occupation || "",
      error: "",
      stateName: (state && state.state_id) || "",
      city: (city && city.local_id) || "",
      cities: [],
      isCityLoading: false,
      count: 0
    };
  }

  static propTypes = {
    currentUser: PropTypes.object,
    accountType: PropTypes.string,
    loading: PropTypes.bool,
    updateUser: PropTypes.func
  };

  componentDidMount() {
    let {
      currentUser: { state }
    } = this.props;
    if (
      state &&
      state.state_id &&
      state.state_id !== "Select state of origin"
    ) {
      this.fetchCities(state.state_id);
    }
  }

  componentDidUpdate(prevProps) {
    let {
      currentUser: {
        first_name,
        gender,
        middle_name,
        last_name,
        email,
        marital_status,
        nationality,
        mobile,
        mobile2,
        occupation,
        state,
        cities,
        city
      }
    } = this.props;

    if (prevProps.currentUser != this.props.currentUser) {
      this.setState({
        first_name: first_name || "",
        gender: {
          male: gender == "male" ? true : false,
          female: gender == "female" ? true : false
        },
        middle_name: middle_name || "",
        last_name: last_name || "",
        email: email || "",
        marital_status: marital_status || "select marital status",
        nationality: nationality || "Nigeria",
        mobile: mobile ? mobile.replace(/\s/gi, "") : "",
        mobile2: mobile2 ? mobile2.replace(/\s/gi, "") : "",
        occupation: occupation || "",
        stateName: (state && state.state_id) || "",
        city: (city && city.local_id) || ""
      });
    }
  }

  getCities = itemValue => {
    this.setState(
      prevState => {
        return {
          isCityLoading: prevState.count != 0 ? true : false
        };
      },
      () => {
        if (itemValue != "Select state of origin" && this.state.count != 0) {
          this.fetchCities(itemValue);
        } else {
          this.setState(prevState => ({
            count: prevState.count + 1
          }));
        }
      }
    );
  };

  fetchCities = itemValue => {
    let { fetchCities } = this.props;
    fetchCities({ data: { state_id: itemValue } })
      .then(data => {
        this.setState(prevState => ({
          isCityLoading: false,
          count: prevState.count + 1
        }));

        let {
          status: { code, desc },
          entity
        } = data;
        if (code == 100) {
          let { cities } = entity;
          this.setState({ cities, stateName: itemValue });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err => {
        this.setState(prevState => ({
          isCityLoading: false,
          count: prevState.count + 1
        }));

        Toast.show({
          text: `No network connection: ${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      });
  };

  _onValidateTextValue = (text, type) => {
    isAlphabet = /^[a-zA-Z]*$/;
    isEmail = /^[^\s@]+@[^\s@.]+\.[^\s@]+$/;
    isNumber = /[0-9+]+/;

    if (type === "first_name") {
      if (isAlphabet.test(text.trim())) {
        this.setState({ first_name: text.trim() });
      } else {
        this.setState({ first_name: false });
      }
    }

    if (type === "middle_name") {
      if (isAlphabet.test(text.trim())) {
        this.setState({ middle_name: text.trim() });
      } else {
        this.setState({ middle_name: false });
      }
    }

    if (type === "last_name") {
      if (isAlphabet.test(text.trim())) {
        this.setState({ last_name: text });
      } else {
        this.setState({ last_name: false });
      }
    }

    if (type === "email") {
      if (isEmail.test(text.trim())) {
        this.setState({ email: text.trim() });
      } else {
        this.setState({ email: false });
      }
    }

    if (type == "mobile2" || type == "mobile") {
      if (isNumber.test(text) || text.length == 0) {
        this.setState({ [type]: text });
      } else {
        Toast.show({
          text:
            "The mobile number field can only contain numbers, spaces or + sign",
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      }
    } else {
      this.setState({ [type]: text });
    }
  };

  onCityChange = value => {
    this.setState({
      city: value
    });
  };

  _selectGender = option => {
    if (option === "male") {
      this.setState({
        gender: {
          male: !this.state.male,
          female: false
        }
      });
    } else {
      this.setState({
        gender: {
          male: false,
          female: !this.state.female
        }
      });
    }
  };

  updateUser = () => {
    let {
      navigation: { navigate },
      currentUser
    } = this.props;

    const {
      first_name: fn,
      middle_name: mn,
      last_name: ln,
      email: e,
      gender: { male, female },
      marital_status: ms,
      nationality: sn,
      mobile: pn,
      occupation: oc,
      mobile2: an,
      city: sc,
      stateName: ss
    } = this.state;

    if (
      fn.length !== undefined &&
      ln.length !== undefined &&
      e.length !== undefined &&
      sn.length !== 0 &&
      pn.length !== 0 &&
      oc.length !== 0
    ) {
      let userDetails = {};
      if (male === true) {
        userDetails = {
          ...currentUser,
          first_name: fn,
          middle_name: mn || "",
          last_name: ln,
          email: e,
          city: sc || "",
          state: ss || "",
          gender: "male",
          marital_status: ms,
          nationality: sn,
          mobile: pn,
          occupation: oc,
          mobile2: an
        };
      } else {
        userDetails = {
          ...currentUser,
          first_name: fn,
          middle_name: mn || "",
          last_name: ln,
          email: e,
          gender: "female",
          marital_status: ms,
          nationality: sn,
          mobile: pn,
          occupation: oc,
          mobile2: an,
          city: sc || "",
          state: ss || ""
        };
      }

      if (ss.length != 0 && sc.length != 0) {
        this.props
          .updateUser({ data: userDetails })
          .then(result => {
            let {
              status: { code, desc }
            } = result;
            if (parseInt(code) == 100) {
              Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "success",
                buttonText: "okay",
                duration: 10000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonSuccessTextStyle
              });
              navigate("profileDetailsTwo", { userDetails });
            } else if (code == 106) {
              Util.logout(this.props.navigation);
            } else {
              Platform.OS !== "ios"
                ? Alert.alert(
                    "Error Message",
                    desc,
                    [{ text: "Okay", onPress: () => null }],
                    { cancelable: false }
                  )
                : Toast.show({
                    text: desc,
                    textStyle: ToastStyle.toast,
                    type: "danger",
                    buttonText: "okay",
                    duration: 30000,
                    buttonStyle: ToastStyle.buttonStyle,
                    buttonTextStyle: ToastStyle.buttonErrorTextStyle
                  });
            }
          })
          .catch(err =>
            Toast.show({
              text: `${err}`,
              textStyle: ToastStyle.toast,
              type: "danger",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonErrorTextStyle
            })
          );
      } else {
        Platform.OS != "ios"
          ? Alert.alert(
              "Error Message",
              "State and city cannot be empty",
              [{ text: "Okay", onPress: () => null }],
              { cancelable: false }
            )
          : Toast.show({
              text: "State and city cannot be empty",
              textStyle: ToastStyle.toast,
              type: "danger",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonErrorTextStyle
            });
      }
    } else {
      this.setState({ error: "fill in the correct details" });
    }
  };

  render() {
    let {
      navigation: { navigate, goBack },
      navigation,
      loading,
      states
    } = this.props;

    let { gender, cities, isCityLoading } = this.state;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          {(loading || isCityLoading) && <LoadingView />}
          <AppHeader title="Profile" onPressBackButton={() => goBack()} />
          <Content>
            <PaddingProvider type="plain">
              <BoldText style={styles.heading}>PROFILE DETAILS</BoldText>
              <View>
                <View>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.first_name ? styles.error : null}
                  >
                    <Label style={styles.label}>First Name</Label>
                    <Input
                      placeholder="First Name"
                      style={styles.textInput}
                      value={this.state.first_name}
                      onChangeText={first_name =>
                        this._onValidateTextValue(first_name, "first_name")
                      }
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.middle_name ? styles.error : null}
                  >
                    <Label style={styles.label}>Middle Name</Label>
                    <Input
                      placeholder="Middle Name"
                      style={styles.textInput}
                      value={this.state.middle_name}
                      onChangeText={middle_name =>
                        this._onValidateTextValue(middle_name, "middle_name")
                      }
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.last_name ? styles.error : null}
                  >
                    <Label style={styles.label}>Last Name</Label>
                    <Input
                      value={this.state.last_name}
                      style={styles.textInput}
                      placeholder="Last Name"
                      onChangeText={last_name =>
                        this._onValidateTextValue(last_name, "last_name")
                      }
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.email ? styles.error : null}
                  >
                    <Label style={styles.label}>Email</Label>
                    <Input
                      value={this.state.email}
                      editable={false}
                      style={styles.textInput}
                      placeholder="Email"
                      onChangeText={email =>
                        this._onValidateTextValue(email, "email")
                      }
                      keyboardType="email-address"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    stackedLabel
                    last
                    style={!this.state.mobile ? styles.error : null}
                  >
                    <Label style={styles.label}>Phone Number</Label>
                    <Input
                      placeholder="Phone Number"
                      style={styles.textInput}
                      editable={false}
                      value={this.state.mobile}
                      onChangeText={mobile =>
                        this._onValidateTextValue(mobile, "mobile")
                      }
                      keyboardType="numeric"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    stackedLabel
                    last
                    style={!this.state.mobile ? styles.error : null}
                  >
                    <Label style={styles.label}>Alternate Phone Number</Label>
                    <Input
                      placeholder="Alternate phone number"
                      value={this.state.mobile2}
                      style={styles.textInput}
                      onChangeText={mobile2 =>
                        this._onValidateTextValue(mobile2, "mobile2")
                      }
                      keyboardType="numeric"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item
                    last
                    stackedLabel
                    style={!this.state.occupation ? styles.error : null}
                  >
                    <Label style={styles.label}>Current Occupation</Label>
                    <Input
                      value={this.state.occupation}
                      placeholder="Current Occupation"
                      style={styles.textInput}
                      onChangeText={occupation =>
                        this._onValidateTextValue(occupation, "occupation")
                      }
                    />
                  </Item>
                </View>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Marital status </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.marital_status.toLowerCase()}
                    onValueChange={marital_status =>
                      this.setState({ marital_status })
                    }
                  >
                    <Picker.Item label="select marital status" value="" />
                    <Picker.Item label="Single" value="single" />
                    <Picker.Item label="Married" value="married" />
                    <Picker.Item label="Divorced" value="divorced" />
                    <Picker.Item label="Seperated" value="seperated" />
                    <Picker.Item label="Widow" value="widow" />
                    <Picker.Item label="Widower" value="widower" />
                  </Picker>
                </View>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Nationality </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.nationality.toLowerCase()}
                  >
                    <Picker.Item label="Nigeria" value="nigeria" />
                  </Picker>
                </View>
              </View>
              <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                <LightText style={styles.label}> State of Origin </LightText>
                <Picker
                  mode="dropdown"
                  selectedValue={this.state.stateName}
                  onValueChange={itemValue => this.getCities(itemValue)}
                >
                  {[
                    {
                      name: "Select state of origin",
                      state_id: ""
                    },
                    ...states
                  ].map((state, i) => (
                    <Picker.Item
                      key={i}
                      label={state.name}
                      value={state.state_id}
                    />
                  ))}
                </Picker>
              </View>
              <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                <LightText style={styles.label}> City of Origin </LightText>
                <Picker
                  mode="dropdown"
                  selectedValue={this.state.city}
                  onValueChange={this.onCityChange}
                >
                  {[
                    {
                      local_name: "Select city of origin",
                      local_id: ""
                    },
                    ...cities
                  ].map((city, i) => (
                    <Picker.Item
                      key={i}
                      label={city.local_name}
                      value={city.local_id}
                    />
                  ))}
                </Picker>
              </View>
              <View
                style={[
                  styles.marginTopXs,
                  styles.flRow,
                  { alignItems: "center" }
                ]}
              >
                <LightText style={styles.label}> Gender: </LightText>
                <View style={[styles.flRow, styles.jcBtw, styles.centered]}>
                  <CheckBox
                    color={gender.male ? "#00b0cf" : "#666"}
                    checked={gender.male}
                    onPress={() => this._selectGender("male")}
                  />
                  <BoldText style={{ marginLeft: 15 }}>Male</BoldText>
                </View>
                <View
                  style={[
                    { marginLeft: 20 },
                    styles.flRow,
                    styles.jcBtw,
                    styles.centered
                  ]}
                >
                  <CheckBox
                    color={gender.female ? "#00b0cf" : "#666"}
                    checked={gender.female}
                    onPress={() => this._selectGender("female")}
                  />
                  <BoldText style={{ marginLeft: 15 }}>Female</BoldText>
                </View>
              </View>
              {this.state.error ? (
                <BoldText
                  style={{
                    color: "red",
                    textAlign: "center",
                    paddingVertical: 10
                  }}
                >
                  {this.state.error}
                </BoldText>
              ) : null}
              <View style={styles.appButtonContainer}>
                <View style={styles.updateBtnContainer}>
                  <AppButton
                    title="Save & Continue"
                    style={StyleSheet.flatten([
                      styles.appButton,
                      styles.carerBtnSpacing,
                      styles.updateBtn
                    ])}
                    onPress={this.updateUser}
                  />
                </View>
                <AppButton
                  onPress={() => this.props.navigation.goBack()}
                  style={StyleSheet.flatten([
                    styles.appButton,
                    styles.carerBtnSpacing,
                    styles.backBtn
                  ])}
                  title="back"
                />
              </View>
            </PaddingProvider>
          </Content>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    accountType: state.accountType
  };
}

const _ProfileDetailsOne = props => (
  <Query operation="getStates">
    {states => {
      let _states = states.data || {},
        _entityStates = _states.entity || {},
        _statesArr = _entityStates.states || [];

      return (
        <Mutation operation="createCitiesInStates">
          {(citiesFetchState, fetchCities) => (
            <Mutation
              operation="updateUser"
              options={{ refetchQueries: [{ operation: "getUserDetails" }] }}
            >
              {(mutateState, mutate) => (
                <ProfileDetailsOne
                  loading={mutateState.loading}
                  updateUser={mutate}
                  states={_statesArr}
                  fetchCities={fetchCities}
                  {...props}
                />
              )}
            </Mutation>
          )}
        </Mutation>
      );
    }}
  </Query>
);

export default connect(mapStateToProps)(_ProfileDetailsOne);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 15
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 20
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  iconColor: {
    color: "#7c8a8c"
  },
  appButton: {
    marginTop: 20,
    paddingHorizontal: 15
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  appButtonContainer: {
    marginVertical: 10
  },
  label: {
    color: "#333",
    fontSize: 13,
    fontFamily: "Quicksand-Light"
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  error: {
    borderBottomColor: "red"
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
});
