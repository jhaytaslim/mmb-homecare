import React from "react";
import { View, StyleSheet, Alert, Platform } from "react-native";
import {
  Item,
  Input,
  Icon,
  Label,
  Picker,
  Textarea,
  Toast,
  Content
} from "native-base";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Mutation, Query } from "react-kunyora";

import { MediumText, LightText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

//@Patch remove the specilaity hack and make more inlined
class CarerIntroduction extends React.PureComponent {
  constructor(props) {
    super(props);

    let { currentCarer } = props;

    this.state = {
      spec1:
        (currentCarer.specialties &&
          currentCarer.specialties.length > 0 &&
          currentCarer.specialties.split(",").length >= 1 &&
          currentCarer.specialties.split(",")[0].trim()) ||
        "",
      spec2:
        (currentCarer.specialties &&
          currentCarer.specialties.length > 0 &&
          currentCarer.specialties.split(",").length >= 2 &&
          currentCarer.specialties.split(",")[1].trim()) ||
        "",
      spec3:
        (currentCarer.specialties &&
          currentCarer.specialties.length > 0 &&
          currentCarer.specialties.split(",").length >= 3 &&
          currentCarer.specialties.split(",")[2].trim()) ||
        "",
      con_price: currentCarer.con_price || "",
      intro_title: currentCarer.intro_title || "",
      intro_body: currentCarer.intro_body || "",
      state: (currentCarer.state && currentCarer.state.state_id) || "",
      city: (currentCarer.lga && currentCarer.lga.local_id) || "",
      cities: [],
      loading: props.loading,
      hasUpdateBeenClicked: false
    };
  }

  static propTypes = {
    loading: PropTypes.bool,
    updateCarer: PropTypes.func,
    currentCarer: PropTypes.object,
    accountType: PropTypes.string,
    states: PropTypes.array,
    statesLoading: PropTypes.bool,
    citiesLoading: PropTypes.bool,
    fetchCities: PropTypes.func,
    specialties: PropTypes.any
  };

  componentDidMount() {
    if (Object.keys(this.props.currentCarer).length > 0) {
      this.fetchCities(this.state.state);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.loading != this.props.loading) {
      this.setState({ loading: this.props.loading });
    }

    if (
      prevProps.currentCarer != this.props.currentCarer &&
      !this.state.hasUpdateBeenClicked
    ) {
      this.updateStateWithInitialData();
    }
  }

  updateStateWithInitialData = () => {
    let { currentCarer } = this.props;

    this.setState(
      {
        spec1:
          (currentCarer.specialties &&
            currentCarer.specialties.length > 0 &&
            currentCarer.specialties.split(",").length >= 1 &&
            currentCarer.specialties.split(",")[0].trim()) ||
          "",
        spec2:
          (currentCarer.specialties &&
            currentCarer.specialties.length > 0 &&
            currentCarer.specialties.split(",").length >= 2 &&
            currentCarer.specialties.split(",")[1].trim()) ||
          "",
        spec3:
          (currentCarer.specialties &&
            currentCarer.specialties.length > 0 &&
            currentCarer.specialties.split(",").length >= 3 &&
            currentCarer.specialties.split(",")[2].trim()) ||
          "",
        con_price: currentCarer.con_price || "",
        intro_title: currentCarer.intro_title || "",
        intro_body: currentCarer.intro_body || "",
        state: (currentCarer.state && currentCarer.state.state_id) || "",
        city: (currentCarer.lga && currentCarer.lga.local_id) || "",
        cities: [],
        loading: this.props.loading
      },
      () => this.fetchCities(this.state.state)
    );
  };

  fetchCities = value => {
    let { fetchCities } = this.props;
    this.setState({ state: value });
    fetchCities({ data: { state_id: value } })
      .then(data => {
        let {
          status: { code, desc },
          entity
        } = data;
        if (code == 100) {
          let { cities } = entity;
          this.setState({ cities });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Toast.show({
            text: desc,
            textStyle: ToastStyle.toast,
            duration: 10000,
            buttonText: "okay",
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            buttonStyle: ToastStyle.buttonStyle,
            type: "danger"
          });
        }
      })
      .catch(err => {
        this.setState({ loading: false });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          buttonText: "okay",
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          buttonStyle: ToastStyle.buttonStyle,
          type: "danger"
        });
      });
  };

  updateCarerIntroduction = () => {
    let {
        updateCarer,
        navigation: { navigate }
      } = this.props,
      {
        con_price,
        intro_body,
        intro_title,
        state,
        city,
        spec1,
        spec2,
        spec3
      } = this.state,
      data = {
        con_price,
        intro_body,
        intro_title,
        state,
        city,
        spec: [spec1, spec2, spec3]
      };

    this.setState({ hasUpdateBeenClicked: true });
    updateCarer({ data })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            duration: 10000,
            type: "success",
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle,
            buttonText: "okay"
          });
          navigate("carerPaymentInfo");
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                type: "danger",
                buttonText: "okay"
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          type: "danger",
          buttonText: "okay"
        })
      );
  };

  render() {
    let {
        navigation: { navigate },
        navigation,
        accountType,
        specialties,
        citiesLoading,
        states
      } = this.props,
      { loading } = this.state,
      _specialties = specialties || {},
      _entitySpecialities = _specialties.entity || {},
      _specialtiesArr = _entitySpecialities.specialties || [];

    return (
      <NavigationProvider navigation={navigation}>
        {(loading || citiesLoading) && <LoadingView />}
        <React.Fragment>
          <AppHeader
            title="Carer Introduction"
            onPressBackButton={() => navigate("carerSetup")}
          />
          <PaddingProvider type="plain">
            <Content showsVerticalScrollIndicator={false}>
              <MediumText style={styles.heading}>
                Select Health Categories
              </MediumText>
              <View>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> First Specialty </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.spec1}
                    onValueChange={spec1 => this.setState({ spec1 })}
                  >
                    {["Select specialty 1", ..._specialtiesArr].map(
                      (specialties, i) => {
                        let label = specialties.replace(/<[^>]*>/gi, "");
                        return (
                          <Picker.Item
                            key={i}
                            label={label}
                            value={label != "Select specialty 1" ? label : ""}
                          />
                        );
                      }
                    )}
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Second Specialty </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.spec2}
                    onValueChange={spec2 => this.setState({ spec2 })}
                  >
                    {["Select specialty 2", ..._specialtiesArr].map(
                      (specialties, i) => {
                        let label = specialties.replace(/<[^>]*>/gi, "");
                        return (
                          <Picker.Item
                            key={i}
                            label={label}
                            value={label != "Select specialty 2" ? label : ""}
                          />
                        );
                      }
                    )}
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Third Specialty </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.spec3}
                    onValueChange={spec3 => this.setState({ spec3 })}
                  >
                    {["Select specialty 3", ..._specialtiesArr].map(
                      (specialties, i) => {
                        let label = specialties.replace(/<[^>]*>/gi, "");
                        return (
                          <Picker.Item
                            key={i}
                            label={label}
                            value={label != "Select specialty 3" ? label : ""}
                          />
                        );
                      }
                    )}
                  </Picker>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Consultation Price </Label>
                    <Input
                      keyboardType="numeric"
                      value={this.state.con_price}
                      onChangeText={con_price => this.setState({ con_price })}
                      placeholder="Consultation Price N"
                      style={styles.textInput}
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}> Introductory title </Label>
                    <Input
                      value={this.state.intro_title}
                      onChangeText={intro_title =>
                        this.setState({ intro_title })
                      }
                      placeholder="Introductory title"
                      style={styles.textInput}
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <LightText style={styles.label}>Introductory Body</LightText>
                  <Textarea
                    value={this.state.intro_body}
                    onChangeText={intro_body => this.setState({ intro_body })}
                    rowSpan={5}
                    bordered
                    style={styles.textInput}
                    placeholder="Introductory Body (not more than 350 Characters)"
                  />
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <Label style={styles.label}>
                    {" "}
                    State Where you currently work{" "}
                  </Label>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.state}
                    onValueChange={this.fetchCities}
                  >
                    {[{ name: "Select a state", state_id: "" }, ...states].map(
                      (state, i) => {
                        return (
                          <Picker.Item
                            key={i}
                            label={state.name}
                            value={state.state_id}
                          />
                        );
                      }
                    )}
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <Label style={styles.label}>
                    City Where you currently work
                  </Label>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.city}
                    onValueChange={city => this.setState({ city })}
                  >
                    {[
                      { local_name: "Select a city", local_id: "" },
                      ...this.state.cities
                    ].map((cityName, i) => (
                      <Picker.Item
                        key={i}
                        label={cityName.local_name}
                        value={cityName.local_id}
                      />
                    ))}
                  </Picker>
                </View>
                <View style={styles.appButtonContainer}>
                  <View style={styles.updateBtnContainer}>
                    <AppButton
                      title="Update"
                      style={StyleSheet.flatten([
                        styles.appButton,
                        styles.carerBtnSpacing,
                        styles.updateBtn
                      ])}
                      onPress={this.updateCarerIntroduction}
                    />
                  </View>
                  <AppButton
                    onPress={() => this.props.navigation.navigate("carerSetup")}
                    style={StyleSheet.flatten([
                      styles.appButton,
                      styles.carerBtnSpacing,
                      styles.backBtn
                    ])}
                    title="back"
                  />
                </View>
              </View>
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _CarerIntroduction = props => (
  <Query operation="getStates">
    {states => {
      let _states = states.data || {},
        _entityStates = _states.entity || {},
        _statesArr = _entityStates.states || [];

      return (
        <Mutation operation="createCitiesInStates">
          {(citiesState, mutateCity) => (
            <Mutation
              operation="updateCarerIntroduction"
              options={{ refetchQueries: [{ operation: "getCarer" }] }}
            >
              {(mutationState, mutate) => (
                <Query operation="getCarerSpecialties">
                  {specialties => (
                    <CarerIntroduction
                      loading={mutationState.loading}
                      updateCarer={mutate}
                      states={_statesArr}
                      statesLoading={states.loading}
                      citiesLoading={citiesState.loading}
                      fetchCities={mutateCity}
                      specialties={specialties.data}
                      {...props}
                    />
                  )}
                </Query>
              )}
            </Mutation>
          )}
        </Mutation>
      );
    }}
  </Query>
);

function mapStateToProps(state) {
  return {
    currentCarer: state.currentCarer,
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_CarerIntroduction);

const styles = StyleSheet.create({
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  label: {
    color: "#333",
    fontSize: 13,
    fontFamily: "Quicksand-Light"
  },
  marginTopXs: {
    marginTop: 20
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  appButton: {
    marginVertical: 20,
    paddingHorizontal: 15
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  appButtonContainer: {
    marginTop: 10
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
});
