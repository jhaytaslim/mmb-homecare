import React from "react";
import {
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import { Item, Input, Icon, Picker, Toast, Content } from "native-base";
import PropTypes from "prop-types";
import FilePickerManager from "react-native-file-picker";
import { connect } from "react-redux";
import { Mutation } from "react-kunyora";

import { BoldText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import ToastStyle from "../../assets/ToastStyle";

const DOCUMENTS = [
  {
    id: "reg_cert",
    title: "Registration Certificate"
  },
  {
    id: "edu_cert",
    title: "Education Certificate"
  },
  {
    id: "license",
    title: "Practice License"
  },
  {
    id: "nysc",
    title: "NYSC Discharge Certificate"
  },
  {
    id: "ref",
    title: "Referee Information"
  },
  {
    id: "nat_id",
    title: "National ID/Work Permit (Optional)"
  }
];

class CarerVerificationDocuments extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        reg_cert:
          (props.currentCarer &&
            props.currentCarer.reg_cert && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined,
        edu_cert:
          (props.currentCarer &&
            props.currentCarer.edu_cert && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined,
        license:
          (props.currentCarer &&
            props.currentCarer.license && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined,
        nysc:
          (props.currentCarer &&
            props.currentCarer.nysc_cert && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined,
        ref:
          (props.currentCarer &&
            props.currentCarer.referee && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined,
        nat_id:
          (props.currentCarer &&
            props.currentCarer.work_permit && {
              uri: undefined,
              name: "uploaded"
            }) ||
          undefined
      },
      error: "",
      success: ""
    };
  }

  static propTypes = {
    uploadDocuments: PropTypes.func,
    loading: PropTypes.bool
  };

  selectFile = document => {
    FilePickerManager.showFilePicker(null, response => {
      if (response.didCancel) {
        //@Just kill process silently
      } else if (response.error) {

      } else {
        this.setState({
          data: {
            ...this.state.data,
            [document]: {
              uri: response.uri,
              type: response.type,
              name: response.fileName
            }
          }
        });
      }
    });
  };

  uploadToServer = () => {
    let formData = new FormData();
    const { reg_cert, edu_cert, license, ref, nysc, nat_id } = this.state.data;

    formData.append(
      "reg_cert",
      reg_cert && reg_cert.uri ? reg_cert : undefined
    );
    formData.append(
      "edu_cert",
      edu_cert && edu_cert.uri ? edu_cert : undefined
    );
    formData.append("license", license && license.uri ? license : undefined);
    formData.append("ref", ref && ref.uri ? ref : undefined);
    formData.append("nysc", nysc && nysc.uri ? nysc : undefined);
    formData.append("nat_id", nat_id && nat_id.uri ? nat_id : undefined);

    this.props
      .uploadDocuments({
        data: formData,
        headers: { "Content-Type": undefined }
      })
      .then(res => {
        this.setState({
          success: "Upload was successful",
          data: {
            ...this.state.data,
            reg_cert: reg_cert
              ? { ...this.state.data.reg_cert, name: "uploaded" }
              : undefined,
            edu_cert: edu_cert
              ? { ...this.state.data.edu_cert, name: "uploaded" }
              : undefined,
            license: license
              ? { ...this.state.data.license, name: "uploaded" }
              : undefined,
            ref: ref ? { ...this.state.data.ref, name: "uploaded" } : undefined,
            nysc: nysc
              ? { ...this.state.data.nysc, name: "uploaded" }
              : undefined,
            nat_id: nat_id
              ? { ...this.state.data.nat_id, name: "uploaded" }
              : undefined
          }
        });
        this.props.navigation.navigate("carerIntro");
      })
      .catch(err => {
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
        this.setState({ success: "Uploading failed" });
      });
  };

  render() {
    let {
      navigation: { navigate },
      navigation
    } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          <AppHeader
            title="Professional Details"
            onPressBackButton={() => navigate("carerSetup")}
          />
          {this.props.loading && <LoadingView />}
          <Content style={{ paddingHorizontal: 10 }}>
            <BoldText style={styles.heading}>
              Verification of Documents
            </BoldText>

            {this.state.success === 100 ? (
              <BoldText
                style={{ color: "green", textAlign: "center", fontSize: 16 }}
              >
                Upload was successful
              </BoldText>
            ) : (
              <BoldText
                style={{ color: "red", textAlign: "center", fontSize: 16 }}
              >
                {this.state.success}
              </BoldText>
            )}

            <View>
              {DOCUMENTS.map(document => {
                return (
                  <View style={styles.marginTopXs} key={document.id}>
                    <TouchableOpacity
                      style={[
                        styles.flRow,
                        styles.jcBtw,
                        styles.pickerBottomStyle,
                        { alignItems: "center" }
                      ]}
                      onPress={() => this.selectFile(document.id)}
                    >
                      {this.state.data[document.id] ? (
                        <Icon
                          active
                          name="check"
                          type="MaterialCommunityIcons"
                          style={{ color: "green", fontSize: 12 }}
                        />
                      ) : null}

                      <BoldText style={{ color: "#000" }}>
                        {document.title}
                        {this.state.data[document.id] && (
                          <BoldText
                            style={{
                              color:
                                this.state.data[document.id].name == "uploaded"
                                  ? "green"
                                  : "#333"
                            }}
                          >
                            [{this.state.data[document.id].name}]
                          </BoldText>
                        )}
                      </BoldText>

                      <Icon
                        active
                        name="upload"
                        type="MaterialCommunityIcons"
                        style={styles.iconColor}
                      />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
            <View style={styles.appButtonContainer}>
              <View style={styles.updateBtnContainer}>
                <AppButton
                  title="Update"
                  style={StyleSheet.flatten([
                    styles.appButton,
                    styles.carerBtnSpacing,
                    styles.updateBtn
                  ])}
                  onPress={() => this.uploadToServer()}
                />
              </View>
              <AppButton
                title="back"
                style={StyleSheet.flatten([
                  styles.appButton,
                  styles.carerBtnSpacing,
                  styles.backBtn
                ])}
                onPress={() => this.props.navigation.navigate("carerSetup")}
              />
            </View>
          </Content>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _CarerVerificationDocuments = props => (
  <Mutation
    operation="createUploadDocuments"
    options={{ refetchQueries: [{ operation: "getCarer" }] }}
  >
    {(mutationState, mutate) => (
      <CarerVerificationDocuments
        uploadDocuments={mutate}
        loading={mutationState.loading}
        {...props}
      />
    )}
  </Mutation>
);

function mapStateToProps(state) {
  return {
    currentCarer: state.currentCarer
  };
}

export default connect(mapStateToProps)(_CarerVerificationDocuments);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  appButtonContainer: {
    marginVertical: 10
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 30
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  iconColor: {
    color: "#7c8a8c"
  },
  appButton: {
    alignSelf: "center",
    marginTop: 20
  }
});
