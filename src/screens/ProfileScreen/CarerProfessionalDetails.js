import React from "react";
import {
  View,
  StyleSheet,
  DatePickerAndroid,
  Platform,
  DatePickerIOS,
  Alert
} from "react-native";
import { Item, Input, Icon, Picker, Toast, Content, Label } from "native-base";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Mutation, Query } from "react-kunyora";

import NavigationProvider from "../../containers/NavigationProvider";
import PaddingProvider from "../../containers/PaddingProvider";
import AppHeader from "../../components/AppHeader";
import { BoldText, MediumText, LightText } from "../../components/AppText";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class CarerProfessionalDetails extends React.PureComponent {
  constructor(props) {
    super(props);

    let { currentCarer } = props,
      prof_body_first =
        Platform.OS == "android"
          ? `Date First Registered | ${currentCarer.prof_body_first}` ||
            `Date First Registered | ${moment().format(
              "YYYY"
            )}-${moment().format("MM")}-${moment().format("DD")}`
          : currentCarer.prof_body_first
            ? new Date(`${currentCarer.prof_body_first} 00:00:00`)
            : new Date(),
      prof_body_renewal_due =
        Platform.OS == "android"
          ? `Date License Renewal Due | ${
              currentCarer.prof_body_renewal_due
            }` ||
            `Date License Renewal Due | ${moment().format(
              "YYYY"
            )}-${moment().format("MM")}-${moment().format("DD")}`
          : currentCarer.prof_body_renewal_due
            ? new Date(`${currentCarer.prof_body_renewal_due} 00:00:00`)
            : new Date();

    this.state = {
      category: currentCarer.category || "",
      specialty: currentCarer.specialty || "",
      prof_body: currentCarer.prof_body || "",
      prof_body_number: currentCarer.prof_body_number || "",
      prof_body_first,
      prof_body_renewal_due,
      ho: currentCarer.ho || "",
      job_title: currentCarer.job_title || "",
      ho_type: currentCarer.ho_type || "",
      ho_mobile: currentCarer.ho_mobile || "",
      ho_email: currentCarer.ho_email || "",
      ho_address: currentCarer.ho_address || "",
      specialties:
        (currentCarer.category &&
          CarerProfessionalDetails.specialty[currentCarer.category]) ||
        []
    };
  }

  static propTypes = {
    specialties: PropTypes.array,
    loading: PropTypes.bool,
    updateInformation: PropTypes.func,
    accountType: PropTypes.string
  };

  static specialty = {
    doctor: [
      "Consultant",
      "Senior Registrar",
      "Registrar",
      "Medical Officer",
      "House Officer"
    ],
    physiotherapist: ["physiotherapist", "Senior Physiotherapist"],
    nurse: [
      "Chief Matron",
      "Matron",
      "Senior Nursing Sister",
      "Nursing Sister",
      "Midwife",
      "Staff Nurse"
    ],
    ["Care Assistant"]: ["Care Assistant"],
    ["Medical Laboratory Scientist"]: ["Medical Laboratory Scientist"]
  };

  updateProfessionalDetails = () => {
    let {
      navigation: { navigate },
      updateInformation
    } = this.props;

    updateInformation({
      data: {
        ...this.state,
        prof_body_first: this.state.prof_body_first.split("|")[1].trim(),
        prof_body_renewal_due: this.state.prof_body_renewal_due
          .split("|")[1]
          .trim()
      }
    })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          Toast.show({
            text: desc,
            textStyle: ToastStyle.toast,
            duration: 10000,
            type: "success",
            buttonText: "okay",
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
          navigate("verificationDocs");
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  setDate = (type, title) => {
    if (Platform.OS == "android") {
      DatePickerAndroid.open({
        date: new Date(this.state[type].split("|")[1].trim())
      })
        .then(({ action, year, month, day }) => {
          if (action !== DatePickerAndroid.dismissedAction) {
            let _month =
                (month + 1).toString().length == 1
                  ? `0${month + 1}`
                  : month + 1,
              _day = day.toString().length == 1 ? `0${day}` : day;
            this.setState({ [type]: `${title} | ${year}-${_month}-${_day}` });
          }
        })
        .catch(({ code, message }) => {
          //@Todo send to app center
        });
    }
  };

  render() {
    let { loading, navigation, accountType } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          <AppHeader
            title="Professional Details"
            onPressBackButton={() => navigation.navigate("carerSetup")}
          />
          {loading && <LoadingView />}
          <PaddingProvider type="plain">
            <Content>
              <BoldText style={styles.heading}>Professional Details</BoldText>

              <View>
                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Carer Category </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.category}
                    onValueChange={category =>
                      this.setState({
                        category,
                        specialties:
                          CarerProfessionalDetails.specialty[category]
                      })
                    }
                  >
                    <Picker.Item label="Select Carer category" value="" />
                    <Picker.Item label="Doctor" value="doctor" />
                    <Picker.Item label="Nurse" value="nurse" />
                    <Picker.Item
                      label="Physiotherapist"
                      value="physiotherapist"
                    />
                    <Picker.Item
                      label="Care Assistant"
                      value="Care Assistant"
                    />
                    <Picker.Item
                      label="Medical Laboratory Scientist"
                      value="Medical Laboratory Scientist"
                    />
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}> Carer specialty </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.specialty}
                    onValueChange={specialty => this.setState({ specialty })}
                  >
                    {[
                      "Select a carer Specialty",
                      ...this.state.specialties
                    ].map((_specialty, i) => (
                      <Picker.Item
                        key={i}
                        label={_specialty}
                        value={
                          _specialty == "Select Specialty" ? "" : _specialty
                        }
                      />
                    ))}
                  </Picker>
                </View>

                <View style={[styles.marginTopXs, styles.pickerBottomStyle]}>
                  <LightText style={styles.label}>
                    Professional Body you belong to
                  </LightText>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.prof_body}
                    onValueChange={prof_body => this.setState({ prof_body })}
                  >
                    <Picker.Item
                      label="Medical and Dental Council of Nigeria"
                      value="Medical and Dental Council of Nigeria"
                    />
                    <Picker.Item
                      label="Nursing & Midwifery Council of Nigeria"
                      value="Nursing & Midwifery Council of Nigeria"
                    />
                    <Picker.Item
                      label="Medical Rehabilitation Therapy (Registration) Board of Nigeria"
                      value="Medical Rehabilitation Therapy (Registration) Board of Nigeria"
                    />
                    <Picker.Item label="Other" value="other" />
                  </Picker>
                </View>

                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Professional Registration Number
                    </Label>
                    <Input
                      style={styles.textInput}
                      value={this.state.prof_body_number}
                      onChangeText={prof_body_number =>
                        this.setState({ prof_body_number })
                      }
                      placeholder="Professional Registration Number"
                    />
                  </Item>
                </View>

                <View style={styles.marginTopXs}>
                  <LightText style={styles.label}>
                    First registration date of Licence
                  </LightText>
                  {Platform.OS == "android" ? (
                    <NativeTouchableFeedback
                      onPress={() =>
                        this.setDate("prof_body_first", "Date First Registered")
                      }
                    >
                      <View style={styles.datePicker}>
                        <MediumText style={styles.dateText}>
                          {this.state.prof_body_first}
                        </MediumText>
                      </View>
                    </NativeTouchableFeedback>
                  ) : (
                    <DatePickerIOS
                      date={this.state.prof_body_first}
                      onDateChange={prof_body_first =>
                        this.setState({ prof_body_first })
                      }
                    />
                  )}
                </View>
                <View style={styles.marginTopXs}>
                  <LightText style={styles.label}>
                    Date Licence Renewal Due
                  </LightText>
                  {Platform.OS == "android" ? (
                    <NativeTouchableFeedback
                      onPress={() =>
                        this.setDate(
                          "prof_body_renewal_due",
                          "Date License Renewal Due"
                        )
                      }
                    >
                      <View style={styles.datePicker}>
                        <MediumText style={styles.dateText}>
                          {this.state.prof_body_renewal_due}
                        </MediumText>
                      </View>
                    </NativeTouchableFeedback>
                  ) : (
                    <DatePickerIOS
                      date={this.state.prof_body_renewal_due}
                      onDateChange={prof_body_renewal_due =>
                        this.setState({ prof_body_renewal_due })
                      }
                    />
                  )}
                </View>
              </View>

              <BoldText style={styles.heading}>
                Health Organization (HO)
              </BoldText>

              <View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Name of Health Organization
                    </Label>
                    <Input
                      value={this.state.ho}
                      style={styles.textInput}
                      onChangeText={ho => this.setState({ ho })}
                      placeholder="Name of Health Organization"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>Job Title</Label>
                    <Input
                      value={this.state.job_title}
                      style={styles.textInput}
                      onChangeText={job_title => this.setState({ job_title })}
                      placeholder="Job Title"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Type of Health Organization
                    </Label>
                    <Input
                      value={this.state.ho_type}
                      style={styles.textInput}
                      onChangeText={ho_type => this.setState({ ho_type })}
                      placeholder="Type of Health Organization"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Phone Number of Health Organization
                    </Label>
                    <Input
                      value={this.state.ho_mobile}
                      style={styles.textInput}
                      keyboardType="numeric"
                      onChangeText={ho_mobile => this.setState({ ho_mobile })}
                      placeholder="Phone Number (HO)"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Email of Health Organization
                    </Label>
                    <Input
                      value={this.state.ho_email}
                      keyboardType="email-address"
                      style={styles.textInput}
                      onChangeText={ho_email => this.setState({ ho_email })}
                      placeholder="Email (HO)"
                    />
                  </Item>
                </View>
                <View style={styles.marginTopXs}>
                  <Item last stackedLabel>
                    <Label style={styles.label}>
                      Address of Health Organization/ Work place
                    </Label>
                    <Input
                      value={this.state.ho_address}
                      style={styles.textInput}
                      onChangeText={ho_address => this.setState({ ho_address })}
                      placeholder="Address of HO/Work Address"
                    />
                  </Item>
                </View>
              </View>
              <View style={styles.appButtonContainer}>
                <View style={styles.updateBtnContainer}>
                  <AppButton
                    title="Update"
                    style={StyleSheet.flatten([
                      styles.appButton,
                      styles.carerBtnSpacing,
                      styles.updateBtn
                    ])}
                    onPress={this.updateProfessionalDetails}
                  />
                </View>
                <AppButton
                  onPress={() => this.props.navigation.navigate("carerSetup")}
                  style={StyleSheet.flatten([
                    styles.appButton,
                    styles.carerBtnSpacing,
                    styles.backBtn
                  ])}
                  title="back"
                />
              </View>
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _CarerProfessionalDetails = props => (
  <Mutation
    operation="updateProfessionalInformation"
    options={{ refetchQueries: [{ operation: "getCarer" }] }}
  >
    {(mutationState, mutate) => (
      <CarerProfessionalDetails
        loading={mutationState.loading}
        updateInformation={mutate}
        {...props}
      />
    )}
  </Mutation>
);

function mapStateToProps(state) {
  return {
    currentCarer: state.currentCarer,
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_CarerProfessionalDetails);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 20
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  appButton: {
    marginTop: 20,
    paddingHorizontal: 15
  },
  carerBtnSpacing: {
    marginHorizontal: 5
  },
  appButtonContainer: {
    marginTop: 10,
    marginVertical: 10
  },
  updateBtnContainer: {
    flexDirection: "row"
  },
  updateBtn: {
    flex: 1
  },
  label: {
    color: "#333",
    fontSize: 13,
    fontFamily: "Quicksand-Light"
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    paddingHorizontal: 60,
    alignSelf: "center"
  },
  datePicker: {
    borderBottomWidth: 1,
    borderBottomColor: "#bdbdbd",
    paddingLeft: 5,
    paddingBottom: 10
  },
  dateText: {
    fontSize: 16,
    color: "#333"
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
});
