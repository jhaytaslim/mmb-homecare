import React from "react";
import { View, Alert, Platform } from "react-native";
import { Item, Input, Icon, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import PaddingProvider from "../../containers/PaddingProvider";
import Colors from "../../assets/Colors";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class CashWithdraw extends React.PureComponent {
  state = {
    amount: "",
    token: ""
  };

  static propTypes = {
    otpLoading: PropTypes.bool,
    withdrawalLoading: PropTypes.bool,
    sendOtp: PropTypes.func,
    sendWithdrawal: PropTypes.func
  };

  sendOtp = () => {
    this.props
      .sendOtp()
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            type: "success",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                duration: 30000,
                buttonText: "okay",
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          duration: 10000,
          buttonText: "okay",
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  sendWithdrawal = () => {
    let { amount, token } = this.state;

    if (amount.length != 0 && token.length != 0) {
      this.props
        .sendWithdrawal({ data: this.state })
        .then(result => {
          let {
            status: { code, desc }
          } = result;

          if (code == 100) {
            Toast.show({
              text: `${desc}`,
              textStyle: ToastStyle.toast,
              type: "success",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonSuccessTextStyle
            });
          } else if (code == 106) {
            Util.logout(this.props.navigation);
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  textStyle: ToastStyle.toast,
                  type: "danger",
                  buttonText: "okay",
                  duration: 30000,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle
                });
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            textStyle: ToastStyle.toast,
            type: "danger",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle
          })
        );
    } else {
      Alert.alert(
        "Error Message",
        `Token  or amount field cannot be empty`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let { otpLoading, withdrawalLoading } = this.props,
      { amount, token } = this.state;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {(otpLoading || withdrawalLoading) && <LoadingView />}
        <React.Fragment>
          <PaddingProvider>
            <View style={{ marginTop: 20 }}>
              <Item style={styles.item} rounded>
                <Icon active name="md-cash" style={styles.icon} />
                <Input
                  value={amount}
                  onChangeText={amount => this.setState({ amount })}
                  placeholderTextColor={Colors.roundedInputPlaceholderColor}
                  placeholder="Amount to withdraw"
                />
              </Item>
              <View style={styles.otpInputContainer}>
                <Item style={[styles.item, styles.otpInput]} rounded>
                  <Icon active name="md-clock" style={styles.icon} />
                  <Input
                    value={token}
                    onChangeText={token => this.setState({ token })}
                    placeholderTextColor={Colors.roundedInputPlaceholderColor}
                    placeholder="OTP"
                  />
                </Item>
                <AppButton title="Send OTP code" onPress={this.sendOtp} />
              </View>
              <AppButton
                style={styles.appButton}
                onPress={this.sendWithdrawal}
                title="Complete Withdrawal"
              />
              <AppButton
                style={styles.btnBlueBg}
                onPress={() =>
                  this.props.navigation.navigate("withdrawalHistory")
                }
                title="View Withdrawal History"
              />
            </View>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CashWithdraw = props => (
  <Mutation operation="createWithdrawalOtp">
    {(OtpState, sendOtp) => (
      <Mutation operation="createWithdrawal">
        {(withdrawalState, sendWithdrawal) => (
          <CashWithdraw
            otpLoading={OtpState.loading}
            withdrawalLoading={withdrawalState.loading}
            sendOtp={sendOtp}
            sendWithdrawal={sendWithdrawal}
            {...props}
          />
        )}
      </Mutation>
    )}
  </Mutation>
));

const styles = {
  item: {
    marginBottom: 10,
    backgroundColor: "#fff"
  },
  otpInputContainer: {
    flexDirection: "row",
    marginTop: 20,
    justifyContent: "space-between"
  },
  otpInput: {
    flex: 1,
    marginRight: 10
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10
  },
  btnBlueBg: {
    alignSelf: "center",
    backgroundColor: "#0f4da8",
    marginTop: 10
  },
  sendOtpButton: {
    backgroundColor: "red"
  },
  icon: {
    color: Colors.roundedInputIconColor
  }
};
