import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { List, ListItem, Left, Right, Icon } from "native-base";
import PropTypes from "prop-types";
import { Query } from "react-kunyora";
import moment from "moment";

import { BoldText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import AppList from "../../components/AppList";
import { FlatListItem } from "../../components/ListItem";
import { Util } from "../../utils";

class WithdrawalHistoryScreen extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    withdrawalHistory: PropTypes.any,
    loading: PropTypes.bool,
    error: PropTypes.any
  };

  render() {
    let {
      navigation: { navigate },
      navigation,
      loading,
      error,
      withdrawalHistory,
      isInitialDataSet,
      refetchQuery,
      fetchMore
    } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          <AppHeader title="Withdrawal History" />
          <AppList
            loading={loading}
            error={error}
            dataArray={withdrawalHistory && withdrawalHistory.withdrawals}
            fetchMore={() => null}
            storageKey="@withdrawalHistory"
            reduxActionID="setWithdrawalHistory"
            reduxStateID="withdrawalHistory"
            refetchQuery={() => refetchQuery()}
            isFetchingMore={isInitialDataSet && loading}
            timeElapsedToMountIndicator={1000}
            containerType="plain"
            renderRow={list => (
              <FlatListItem
                type="icon"
                iconName="person"
                title={Util.formatToNaira(list.amount)}
                description={
                  list.status == 0 ? "Awaiting Confirmation" : "Confirmed"
                }
                descriptionStyle={
                  list.status == 0 ? styles.awaiting : styles.confirmed
                }
                tag={
                  list.date_verified
                    ? `Verfied on: ${moment(list.date_verified).format(
                        "YYYY-MM-DD"
                      )}`
                    : " "
                }
                date={moment(list.created).format("YYYY-MM-DD")}
                onPress={() => null}
              />
            )}
          />
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_WithdrawalHistoryScreen = props => (
  <Query operation="getWithdrawals">
    {({ data, error, loading, isInitialDataSet }, fetchMore, refetchQuery) => (
      <WithdrawalHistoryScreen
        loading={loading}
        refetchQuery={refetchQuery}
        isInitialDataSet={isInitialDataSet}
        fetchMore={fetchMore}
        error={error}
        withdrawalHistory={data && data.entity}
        {...props}
      />
    )}
  </Query>
));

const styles = StyleSheet.create({
  awaiting: {
    color: "orange",
    fontSize: 16
  },
  confirmed: {
    color: "green",
    fontSize: 16
  }
});
