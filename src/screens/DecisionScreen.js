import React from "react";
import { connect } from "react-redux";

import MySubscription from "./SubscriptionScreen/MySubscription";
import CashWithdraw from "./WithdrawalScreen/CashWithdraw";

class DecisionScreen extends React.PureComponent {
  render() {
    let { accountType } = this.props;
    return accountType == "carer" ? (
      <CashWithdraw {...this.props} />
    ) : accountType == "patient" ? (
      <MySubscription {...this.props} />
    ) : null;
  }
}

function mapStateToProps(state) {
  return {
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(DecisionScreen);
