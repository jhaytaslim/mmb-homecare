import React from "react";
import { StyleSheet, View } from "react-native";
import moment from "moment";

import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import { Util } from "../../utils";

export default class PaymentDetails extends React.PureComponent {
  render() {
    let {
      navigation: {
        state: {
          params: { payment }
        }
      }
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title={payment.contract_id.contract_id} />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <View style={styles.container}>
              <MediumText style={styles.text}> Patient: </MediumText>
              <MediumText style={styles.text}>
                {payment.contract_id.patient_name}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Type: </MediumText>
              <MediumText style={styles.text}>{payment.type}</MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Amount: </MediumText>
              <MediumText style={styles.text}>
                {Util.formatToNaira(payment.roi)}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Commission: </MediumText>
              <MediumText style={styles.text}>{payment.commission}</MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Completed Date: </MediumText>
              <MediumText style={styles.text}>
                {moment(payment.date_confirmed).format("Do MMM, YYYY")}
              </MediumText>
            </View>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginRight: 12,
    marginBottom: 10
  },
  text: {
    fontSize: 13,
    color: "#333"
  }
});
