import React from "react";
import { View } from "react-native";
import { Item, Input, Icon } from "native-base";

import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import Colors from "../../assets/Colors";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";

export default class CashWithdraw extends React.PureComponent {
  render() {
    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title="Withdraw Funds" />
          <PaddingProvider>
            <View style={{marginTop: 20}}>
            <Item style={styles.item} rounded>
              <Icon active name="md-cash" style={styles.icon} />
              <Input
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Amount to withdraw"
              />
            </Item>
            <View style={styles.otpInputContainer}>
              <Item style={[styles.item, styles.otpInput]} rounded>
                <Icon active name="md-clock" style={styles.icon} />
                <Input
                  placeholderTextColor={Colors.roundedInputPlaceholderColor}
                  placeholder="OTP"
                />
              </Item>
              <AppButton title="send Code" onPress={() => alert(4)} />
            </View>
            <AppButton
              style={{ ...styles.appButton, ...styles.sendOtpButton }}
              onPress={() => null}
              title="Send OTP Code to Mobile"
            />
            <AppButton
              style={styles.appButton}
              onPress={() => null}
              title="Initiate Withdrawal"
            />
            </View>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const styles = {
  item: {
    marginBottom: 10,
    backgroundColor: "#fff"
  },
  otpInputContainer: {
    flexDirection: "row",
    marginTop: 20,
    justifyContent: "space-between"
  },
  otpInput: {
    flex: 1,
    marginRight: 10
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10
  },
  sendOtpButton: {
    backgroundColor: "red"
  },
  icon: {
    color: Colors.roundedInputIconColor
  }
};
