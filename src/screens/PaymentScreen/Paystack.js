import React from "react";
import { View, StyleSheet, Alert, Platform } from "react-native";
import { Mutation } from "react-kunyora";
import { connect } from "react-redux";
import { Item, Input, Toast, Content } from "native-base";
import RNPaystack from "react-native-paystack";

import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import AppHeader from "../../components/AppHeader";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class PaystackPayment extends React.PureComponent {
  state = {
    cardNumber: "",
    expiryMonth: "",
    expiryYear: "",
    cvc: "",
    loading: false
  };

  onChargeCard = () => {
    let { cardNumber, expiryMonth, expiryYear, cvc } = this.state,
      {
        navigation: {
          state: {
            params: { onSuccess }
          }
        }
      } = this.props;

    if (!/^[0-9]+$/gi.test(cardNumber.trim())) {
      Alert.alert(
        "Error Message",
        "The card number can only contain numbers",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    } else if (
      !/^[0-9]{2}$/gi.test(expiryMonth.trim()) ||
      !/^[0-9]{2}$/gi.test(expiryYear.trim())
    ) {
      Alert.alert(
        "Error Message",
        "The year and the month can only be numbers of length 2",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    } else if (!/^[0-9]+$/gi.test(cvc.trim())) {
      Alert.alert(
        "Error Message",
        "The cvc can only contain numbers",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    }

    this.setState({ loading: true });

    RNPaystack.chargeCard({
      cardNumber,
      expiryMonth,
      expiryYear,
      cvc,
      email: this.props.paystack.email,
      amountInKobo: parseInt(this.props.paystack.amount),
      reference: this.props.paystack.ref
    })
      .then(response => {
        this.props
          .mutate({ data: { code: response.reference } })
          .then(res => {
            let {
              status: { code, desc }
            } = res;

            this.setState({ loading: false });
            if (code == 100) {
              Toast.show({
                text: `The transaction was made successfully`,
                textStyle: ToastStyle.toast,
                duration: 10000,
                buttonText: "okay",
                buttonTextStyle: ToastStyle.buttonSuccessTextStyle,
                buttonStyle: ToastStyle.buttonStyle,
                type: "success"
              });
              onSuccess();
            } else if (code == 106) {
              Util.logout(this.props.navigation);
            }
          })
          .catch(err => {
            this.setState({ loading: false });
            Toast.show({
              text: `${err}`,
              textStyle: ToastStyle.toast,
              duration: 10000,
              buttonText: "okay",
              buttonTextStyle: ToastStyle.buttonErrorTextStyle,
              buttonStyle: ToastStyle.buttonStyle,
              type: "danger"
            });
          });
      })
      .catch(error => {
        this.setState({
          loading: false
        });

        Platform.OS !== "ios"
          ? Alert.alert(
              "Error Message",
              `${error}`,
              [{ text: "Okay", onPress: () => null }],
              { cancelable: false }
            )
          : Toast.show({
              text: `${error}`,
              textStyle: ToastStyle.toast,
              duration: 30000,
              buttonText: "okay",
              buttonTextStyle: ToastStyle.buttonErrorTextStyle,
              buttonStyle: ToastStyle.buttonStyle,
              type: "danger"
            });
      });
  };

  render() {
    let {
      navigation: { navigate },
      loading,
      paystack: { info }
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <AppHeader title={info.toUpperCase() || `PAYMENT`} />
        {(this.state.loading || loading) && <LoadingView />}
        <View style={[styles.cardStyles]}>
          <Content showsVerticalScrollIndicator={false}>
            <View style={styles.marginTopXs}>
              <Item last>
                <Input
                  keyboardType="numeric"
                  placeholder="CARD NUMBER"
                  onChangeText={text => this.setState({ cardNumber: text })}
                />
              </Item>
            </View>
            <View style={styles.marginTopXs}>
              <Item last>
                <Input
                  keyboardType="numeric"
                  placeholder="MM"
                  onChangeText={text => this.setState({ expiryMonth: text })}
                />
              </Item>
            </View>
            <View style={styles.marginTopXs}>
              <Item last>
                <Input
                  keyboardType="numeric"
                  placeholder="YY"
                  onChangeText={text => this.setState({ expiryYear: text })}
                />
              </Item>
            </View>
            <View style={styles.marginTopXs}>
              <Item last>
                <Input
                  keyboardType="numeric"
                  placeholder="CVV"
                  onChangeText={text => this.setState({ cvc: text })}
                />
              </Item>
            </View>
            <AppButton
              title="Pay"
              style={styles.appButton}
              onPress={this.onChargeCard}
            />
          </Content>
        </View>
      </NavigationProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    token: state.jwt,
    paystack: state.paystack
  };
}

const _PaystackPayment = props => {
  let {
    navigation: {
      state: {
        params: { operation, refetchQueries }
      }
    }
  } = props;

  return (
    <Mutation operation={operation} options={{ refetchQueries }}>
      {(mutationState, mutate) => (
        <PaystackPayment
          {...props}
          mutate={mutate}
          loading={mutationState.loading}
        />
      )}
    </Mutation>
  );
};

export default connect(mapStateToProps)(_PaystackPayment);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  heading: {
    color: "#000000",
    fontSize: 24,
    textAlign: "center",
    marginTop: 20
  },
  pickerBottomStyle: {
    borderBottomColor: "#97a6a9",
    borderBottomWidth: 1
  },
  marginTopXs: {
    marginTop: 20
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  verified: {
    backgroundColor: "#00dc5f",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 50
  },
  blackText: {
    color: "#000"
  },
  iconColor: {
    color: "#7c8a8c"
  },
  appButton: {
    alignSelf: "center",
    marginTop: 20,
    backgroundColor: "red"
  },
  paymentInput: {
    backgroundColor: "#fff",
    padding: 10
  },
  cardStyles: {
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    paddingTop: 20,
    flex: 1
  }
});
