import React from "react";
import { List } from "native-base";
import PropTypes from "prop-types";
import { Query } from "react-kunyora";
import moment from "moment";

import AppHeader from "../../components/AppHeader";
import { FlatListItem } from "../../components/ListItem";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import AppList from "../../components/AppList";

class PaymentList extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    payments: PropTypes.any,
    loading: PropTypes.bool,
    error: PropTypes.any
  };

  render() {
    let {
      navigation: { navigate },
      loading,
      error,
      payments,
      isInitialDataSet,
      refetchQuery,
      fetchMore
    } = this.props;

    let _payments = (payments && payments.payments) || undefined;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title="Payments" />
          <AppList
            containerType="plain"
            error={error}
            loading={loading}
            refetchQuery={() => refetchQuery()}
            fetchMore={() => null}
            storageKey="@paymentList"
            reduxActionID="setPaymentList"
            reduxStateID="paymentList"
            dataArray={_payments}
            timeElapseToMountIndicator={1000}
            isFetchingMore={isInitialDataSet && loading}
            renderRow={list => (
              <FlatListItem
                type="icon"
                iconName="md-pin"
                title={list.contract_id.contract_id}
                description={list.contract_id.patient_name}
                tag={list.type || ""}
                date={moment(list.date_confirmed).format("MMM Do")}
                onPress={() => navigate("paymentDetails", { payment: list })}
              />
            )}
          />
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_PaymentList = props => (
  <Query operation="getCarerPayments" options={{ fetchPolicy: "network-only" }}>
    {({ data, error, loading, isInitialDataSet }, fetchMore, refetchQuery) => (
      <PaymentList
        payments={data && data.entity}
        error={error}
        isInitialDataSet={isInitialDataSet}
        loading={loading}
        fetchMore={fetchMore}
        refetchQuery={refetchQuery}
        {...props}
      />
    )}
  </Query>
));
