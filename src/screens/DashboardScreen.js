import React from "react";
import {
  View,
  StyleSheet,
  TouchableHighlight,
  AsyncStorage
} from "react-native";
import { List, Item, Input, Icon, Button } from "native-base";
import PropTypes from "prop-types";
import { Query, Mutation } from "react-kunyora";
import { connect } from "react-redux";
import axios from "axios";

import PaddingProvider from "../containers/PaddingProvider";
import { MediumText, BoldText } from "../components/AppText";
import NavigationProvider from "../containers/NavigationProvider";
import NativeTouchableFeedback from "../components/NativeTouchableFeedback";
import UIRenderer from "../components/UIRenderer";
import { ReduxContext } from "../context/ReduxContext";
import { Util } from "../utils";
import NotificationHandler from "../containers/NotificationHandler";
import { newMessagesNotification } from "../reducers/notifier";
import Colors from "../assets/Colors";
import TabsNavigationContext from "../context/TabsNavigationContext";

class DashboardScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userType: this.props.accountType,
      searchValue: ""
    };
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    error: PropTypes.any,
    data: PropTypes.shape({
      status: PropTypes.shape({
        code: PropTypes.number,
        desc: PropTypes.string
      }),
      entity: PropTypes.shape({
        me: PropTypes.object
      })
    }),
    setAccountType: PropTypes.func,
    setUser: PropTypes.func,
    setCarer: PropTypes.func,
    setContractList: PropTypes.func,
    carerBalance: PropTypes.any,
    refetchCarerQuery: PropTypes.func,
    updateNewMessageNotificationState: PropTypes.func,
    setUnreadMessages: PropTypes.func,
    setConversations: PropTypes.func,
    setUncheckedNewContracts: PropTypes.func,
    uncheckedNewContracts: PropTypes.any
  };

  componentDidMount() {
    this.timer = setInterval(() => {
      axios({
        method: "GET",
        baseURL: "https://www.mmbhomecare.com/api/user/ping"
      })
        .then(data => null)
        .catch(err => null);
    }, 6000);
  }

  componentDidUpdate(prevProps) {
    let { pushNotificationData: prevpushNotificationData } = prevProps,
      { pushNotificationData: newpushNotificationData } = this.props,
      _id = prevpushNotificationData ? prevpushNotificationData.id : undefined;

    if (newpushNotificationData && newpushNotificationData.id != _id) {
      //@Its definately a push: Therefore handle data
      Util.handleBackgroundPushNotification(
        this.props.navigation,
        newpushNotificationData,
        this.props.accountType
      );
    }
    this.updateUnreadNotificationMessages(prevProps);
    this.updateUncheckedNewContracts(prevProps);
    this.saveMessageInChat(prevProps);
    this.saveUserInfo(prevProps);
    this.saveCarerInfo(prevProps);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  saveMessageInChat = prevProps => {
    let { newMessagesNotification: prevNewMessagesNotification } = prevProps,
      {
        newMessagesNotification: newNewMessagesNotification,
        updateNewMessageNotificationState
      } = this.props;

    if (
      newNewMessagesNotification &&
      newNewMessagesNotification != prevNewMessagesNotification
    ) {
      AsyncStorage.getItem("@chats").then(chats => {
        let prevChats = {},
          { convo_id } = newNewMessagesNotification[0];

        if (chats) {
          prevChats = JSON.parse(chats);
        }

        let prevChatStream =
            (prevChats[convo_id] && prevChats[convo_id].chats) || {},
          prevPagination = (prevChats[convo_id] &&
            prevChats[convo_id].pagination) || {
            total_records: 0,
            current_page: 0,
            total_pages: 1
          },
          _storedChats = {
            ...prevChats,
            [convo_id]: {
              ...prevChats[convo_id],
              chats: {
                ...Util.flattenArray(
                  newNewMessagesNotification,
                  {},
                  prevChats[convo_id] || {},
                  {}
                ),
                ...prevChatStream
              },
              pagination: {
                ...prevPagination,
                total_records: Number(prevPagination.total_records) + 1
              }
            }
          };

        AsyncStorage.setItem("@chats", JSON.stringify(_storedChats)).then(
          data => null
        );
      });
    }
  };

  updateUnreadNotificationMessages = prevProps => {
    let { newMessagesNotification: prevNewMessagesNotification } = prevProps,
      {
        newMessagesNotification: newNewMessagesNotification,
        updateNewMessageNotificationState,
        setUnreadMessages,
        unreadMessages,
        conversations,
        setConversations
      } = this.props;

    if (
      newNewMessagesNotification &&
      newNewMessagesNotification != prevNewMessagesNotification
    ) {
      let _newConversations = conversations.map((convo, i) => {
        if (convo.convo_id == newNewMessagesNotification[0].convo_id) {
          return {
            ...convo,
            last_message: {
              ...convo.last_message,
              message: newNewMessagesNotification[0].message,
              created: newNewMessagesNotification[0].created
            }
          };
        }
        return convo;
      });

      AsyncStorage.multiSet([
        [
          "@unreadMessages",
          JSON.stringify([...newNewMessagesNotification, ...unreadMessages])
        ],
        ["@conversations", JSON.stringify(_newConversations)]
      ])
        .then(data => {
          setConversations(_newConversations);
          updateNewMessageNotificationState(null);
          setUnreadMessages([...newNewMessagesNotification, ...unreadMessages]);
        })
        .catch(err => null);
    }
  };

  updateUncheckedNewContracts = prevProps => {
    let { uncheckedNewContracts: prevUncheckedNewContracts } = prevProps,
      {
        uncheckedNewContracts: newUncheckedNewContracts,
        contractList,
        setContractList,
        setUncheckedNewContracts
      } = this.props;

    if (
      newUncheckedNewContracts &&
      newUncheckedNewContracts != prevUncheckedNewContracts
    ) {
      AsyncStorage.multiSet([
        ["@setUncheckedNewContracts", JSON.stringify(newUncheckedNewContracts)],
        [
          "@contractList",
          JSON.stringify([...contractList, ...newUncheckedNewContracts])
        ]
      ])
        .then(data => {
          setContractList([newUncheckedNewContracts[0], ...contractList]);
        })
        .catch(err => null);
    }
  };

  saveUserInfo = prevProps => {
    let { data } = this.props,
      _staleData = prevProps.data || {},
      _staleEntity = _staleData.entity || {},
      _staleMe = _staleEntity.me || {};

    if (data) {
      let {
          status: { code }
        } = data,
        {
          setAccountType,
          setUser,
          navigation: { navigate },
          refetchCarerQuery
        } = this.props;

      if (code != 106) {
        let {
          entity: {
            me,
            me: { account_type }
          }
        } = data;

        if (_staleMe.prev_login !== me.prev_login) {
          AsyncStorage.multiSet([
            ["@userType", account_type],
            ["@user", JSON.stringify(me)]
          ]).then(result => {
            setAccountType(account_type);
            setUser(me);
            refetchCarerQuery();

            this.setState({
              userType: account_type
            });
          });
        }
      } else {
        Util.logout(this.props.navigation);
      }
    }
  };

  saveCarerInfo = prevProps => {
    let { carer } = this.props,
      _staleData = prevProps.data || {},
      _staleEntity = _staleData.entity || {},
      _staleMe = _staleEntity.carer || {};

    if (carer) {
      let {
          status: { code }
        } = carer,
        {
          setCarer,
          navigation: { navigate }
        } = this.props;

      if (code != 106) {
        let { entity } = carer;
        if (_staleMe.carer_id != entity.carer.carer_id) {
          AsyncStorage.setItem("@carer", JSON.stringify(entity.carer)).then(
            result => {
              setCarer(entity.carer);
            }
          );
        }
      } else {
        Util.logout(this.props.navigation);
      }
    }
  };

  renderSearchInput = () => {
    let { searchValue } = this.state,
      {
        navigation: { navigate }
      } = this.props;

    return (
      <Item rounded style={styles.searchInputStyles}>
        <Input
          value={searchValue}
          onChangeText={value => this.setState({ searchValue: value })}
          placeholder="Search by Carer ID e.g HM-098765"
          keyboardType="web-search"
          onSubmitEditing={() =>
            navigate("carerList", { carer_id: searchValue })
          }
          style={{
            textAlign: "center",
            fontFamily: "Quicksand-Medium",
            fontSize: 14
          }}
        />
        <Icon
          type="Ionicons"
          name="ios-search"
          onPress={() => navigate("carerList", { carer_id: searchValue })}
        />
      </Item>
    );
  };

  renderPatientDashboard = () => {
    let {
      navigation: { navigate },
      onChangeTab
    } = this.props;
    return (
      <React.Fragment>
        <View style={styles.marginTopSm}>{this.renderSearchInput()}</View>
        <View style={styles.dashboardIconCon}>
          <View>
            <View style={styles.centered}>
              <Button
                onPress={() => navigate("carerList")}
                style={[styles.btnBlue, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-paper-outline"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Carers List</MediumText>
            </View>
            <View>
              {this.props.unreadMessages.length > 0 && (
                <View style={styles.messageCountContainer}>
                  <BoldText style={styles.messageCount}>
                    {this.props.unreadMessages.length}
                  </BoldText>
                </View>
              )}
              <View style={[styles.centered, { marginTop: 20 }]}>
                <Button
                  onPress={() => {
                    onChangeTab(1);
                  }}
                  style={[styles.btnBlue, styles.customBtnStyle]}
                >
                  <Icon
                    type="Ionicons"
                    name="ios-mail-open"
                    style={styles.btnIconLarge}
                  />
                </Button>
                <MediumText style={styles.text}>Inbox</MediumText>
              </View>
            </View>
            <View style={[styles.centered, { marginTop: 20 }]}>
              <Button style={[styles.btnBlue, styles.customBtnStyle]}>
                <Icon
                  type="Ionicons"
                  name="ios-settings"
                  style={styles.btnIconLarge}
                  onPress={() => navigate("settings")}
                />
              </Button>
              <MediumText style={styles.text}>Settings</MediumText>
            </View>
          </View>

          <View>
            <View style={styles.centered}>
              <Button
                onPress={() => navigate("profile")}
                style={[styles.btnRed, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-person"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Profile</MediumText>
            </View>
            <View style={[styles.centered, { marginTop: 20 }]}>
              <Button
                onPress={() => onChangeTab(4)}
                style={[styles.btnBlue, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-notifications-outline"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Notification</MediumText>
            </View>
            <View style={[styles.centered, { marginTop: 20 }]}>
              <Button style={[styles.btnRed, styles.customBtnStyle]}>
                <Icon
                  type="Ionicons"
                  name="ios-help-circle-outline"
                  style={styles.btnIconLarge}
                  onPress={() => navigate("help")}
                />
              </Button>
              <MediumText style={styles.text}>Help</MediumText>
            </View>
          </View>

          <View>
            <View style={styles.centered}>
              <Button
                onPress={() => onChangeTab(2)}
                style={[styles.btnRed, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-thumbs-up-outline"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Subscription</MediumText>
            </View>
            <View>
              {this.props.uncheckedNewContracts &&
                this.props.uncheckedNewContracts.length > 0 && (
                  <View
                    style={[
                      styles.messageCountContainer,
                      { backgroundColor: Colors.brandColor }
                    ]}
                  >
                    <BoldText style={styles.messageCount}>
                      {this.props.uncheckedNewContracts.length}
                    </BoldText>
                  </View>
                )}
              <View style={[styles.centered, { marginTop: 20 }]}>
                <Button
                  onPress={() => onChangeTab(3)}
                  style={[styles.btnRed, styles.customBtnStyle]}
                >
                  <Icon
                    type="Ionicons"
                    name="ios-heart"
                    style={styles.btnIconLarge}
                  />
                </Button>
                <MediumText style={styles.text}>Contracts</MediumText>
              </View>
            </View>
          </View>
        </View>
      </React.Fragment>
    );
  };

  renderCarerDashboard = () => {
    let {
      navigation: { navigate },
      carerBalance,
      onChangeTab
    } = this.props;

    return (
      <React.Fragment>
        <NativeTouchableFeedback onPress={() => navigate("paymentList")}>
          <View
            style={[
              styles.flRow,
              styles.marginTopXs,
              {
                alignItems: "center",
                backgroundColor: "#fff",
                padding: 15,
                borderRadius: 50
              }
            ]}
          >
            <View>
              <Icon
                type="Ionicons"
                name="ios-cash-outline"
                style={{ color: "#3ce14d", fontSize: 30 }}
              />
            </View>
            <View>
              <MediumText style={{ fontSize: 40, color: "#00e1ea" }}>
                {Util.formatToNaira(carerBalance || 0)}
              </MediumText>
              <View style={{ alignItems: "flex-end" }}>
                <MediumText style={{ fontSize: 15, color: "#00e1ea" }}>
                  Balance
                </MediumText>
              </View>
            </View>
            <View>
              <Icon
                type="Ionicons"
                name="ios-arrow-forward-outline"
                style={{ fontSize: 30 }}
              />
            </View>
          </View>
        </NativeTouchableFeedback>
        <View style={styles.dashboardIconCon}>
          <View>
            <View style={styles.centered}>
              <Button
                onPress={() => navigate("profile")}
                style={[styles.btnRed, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-person"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Profile</MediumText>
            </View>
            <View style={styles.centered}>
              <Button
                onPress={() => onChangeTab(4)}
                style={[styles.btnRed, styles.customBtnStyle]}
              >
                <Icon
                  type="FontAwesome"
                  name="bell"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Notifications</MediumText>
            </View>
            <View style={styles.centered}>
              <Button
                onPress={() => onChangeTab(2)}
                style={[styles.btnBlue, styles.customBtnStyle]}
              >
                <Icon
                  type="SimpleLineIcons"
                  name="wallet"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Withdrawals</MediumText>
            </View>
          </View>

          <View>
            <View style={styles.centered}>
              <Button
                onPress={() => navigate("carerSetup")}
                style={[styles.btnRed, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-build"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Setup</MediumText>
            </View>
            <View style={styles.centered}>
              <Button
                style={[styles.btnBlue, styles.customBtnStyle]}
                onPress={() => onChangeTab(3)}
              >
                <Icon
                  type="Ionicons"
                  name="md-heart"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Contracts</MediumText>
            </View>
            <View style={styles.centered}>
              <Button
                style={[styles.btnRed, styles.customBtnStyle]}
                onPress={() => navigate("settings")}
              >
                <Icon
                  type="Ionicons"
                  name="ios-settings"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Settings</MediumText>
            </View>
          </View>

          <View>
            <View>
              {this.props.unreadMessages.length > 0 && (
                <View style={[styles.messageCountContainer, { top: -8 }]}>
                  <BoldText style={styles.messageCount}>
                    {this.props.unreadMessages.length}
                  </BoldText>
                </View>
              )}
              <View style={styles.centered}>
                <Button
                  style={[styles.btnBlue, styles.customBtnStyle]}
                  onPress={() => onChangeTab(1)}
                >
                  <Icon
                    type="FontAwesome"
                    name="envelope"
                    style={styles.btnIconLarge}
                  />
                </Button>
                <MediumText style={styles.text}>Inbox</MediumText>
              </View>
            </View>
            <View style={styles.centered}>
              <Button
                onPress={() => navigate("paymentList")}
                style={[styles.btnBlue, styles.customBtnStyle]}
              >
                <Icon
                  type="Ionicons"
                  name="ios-cart"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Income</MediumText>
            </View>
            <View style={styles.centered}>
              <Button
                style={[styles.btnBlue, styles.customBtnStyle]}
                onPress={() => navigate("help")}
              >
                <Icon
                  type="Ionicons"
                  name="ios-help-circle"
                  style={styles.btnIconLarge}
                />
              </Button>
              <MediumText style={styles.text}>Help</MediumText>
            </View>
          </View>
        </View>
      </React.Fragment>
    );
  };

  renderDashboardUI = () => {
    let {
        navigation: { navigate }
      } = this.props,
      { userType } = this.state;

    if (userType == "patient") {
      return this.renderPatientDashboard();
    } else if (userType == "carer") {
      return this.renderCarerDashboard();
    }
  };

  render() {
    let { navigation, accountType, refetchQuery } = this.props,
      { userType } = this.state;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          <UIRenderer
            loading={!accountType && this.props.loading}
            isRefreshing={this.props.loading}
            error={!accountType && this.props.error}
            timeElapseToMountIndicator={1000}
            onReload={() => {
              refetchQuery();
              this.props.refetchCarerQuery();
            }}
          >
            {this.renderDashboardUI()}
          </UIRenderer>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    accountType: state.accountType,
    conversations: state.conversations,
    contractList: state.contractList,
    pushNotificationData: state.pushNotificationData,
    unreadMessages: state.unreadMessages,
    newMessagesNotification: state.newMessagesNotification,
    carerBalance: state.currentCarer.carer_balance,
    uncheckedNewContracts: state.uncheckedNewContracts
  };
}

const _DashboardScreen = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <ReduxContext.Consumer>
        {({
          screenProps: {
            setAccountType,
            setUser,
            setCarer,
            updateNewMessageNotificationState,
            setUnreadMessages,
            setConversations,
            setContractList,
            setUncheckedNewContracts
          }
        }) => (
          <Query
            operation="getUserDetails"
            options={{ fetchPolicy: "network-only" }}
          >
            {({ data, error, loading }, fetchMore, refetchQuery) => {
              let _data = data || {},
                _status = _data.status || {},
                _code = _status.code || undefined,
                _entity = _data.entity || {},
                _me = _entity.me || {},
                _accountType = _me.account_type || undefined;

              return (
                <Query
                  operation="getCarer"
                  skip={_code == 100 && _accountType == "carer" ? false : true}
                >
                  {(result, carerFetchMore, carerRefetchFn) => (
                    <DashboardScreen
                      loading={loading}
                      error={error}
                      data={data}
                      setAccountType={setAccountType}
                      setUser={setUser}
                      setCarer={setCarer}
                      setConversations={setConversations}
                      carer={result.data}
                      refetchQuery={refetchQuery}
                      setContractList={setContractList}
                      refetchCarerQuery={carerRefetchFn}
                      updateNewMessageNotificationState={
                        updateNewMessageNotificationState
                      }
                      setUnreadMessages={setUnreadMessages}
                      onChangeTab={onChangeTab}
                      setUncheckedNewContracts={setUncheckedNewContracts}
                      {...props}
                    />
                  )}
                </Query>
              );
            }}
          </Query>
        )}
      </ReduxContext.Consumer>
    )}
  </TabsNavigationContext.Consumer>
);

export default connect(mapStateToProps)(_DashboardScreen);

const styles = StyleSheet.create({
  marginTopSm: {
    marginTop: 50
  },
  elevate: {
    elevation: 2
  },
  marginTopXs: {
    marginTop: 20
  },
  searchInputStyles: {
    paddingHorizontal: 15,
    backgroundColor: "#fff"
  },
  flRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  centered: {
    justifyContent: "center",
    marginBottom: 20,
    width: 82,
    zIndex: 10
  },
  dashboardIconCon: {
    paddingHorizontal: 15,
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  btnRed: {
    backgroundColor: "#ff4c49"
  },
  btnBlue: {
    backgroundColor: "#00d9f1"
  },
  customBtnStyle: {
    height: 70,
    marginBottom: 5,
    borderRadius: 10,
    width: 82,
    alignItems: "center",
    justifyContent: "center"
  },
  btnIconLarge: {
    fontSize: 34
  },
  text: {
    fontSize: 12,
    color: "#fff",
    textAlign: "center"
  },
  messageCountContainer: {
    height: 26,
    width: 26,
    borderRadius: 13,
    position: "absolute",
    right: -3,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: 8,
    zIndex: 10000,
    backgroundColor: "#f44336"
  },
  messageCount: {
    fontSize: 13,
    color: "#fff"
  }
});
