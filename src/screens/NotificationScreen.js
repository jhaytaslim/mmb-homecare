import React from "react";
import { View, StyleSheet, AsyncStorage } from "react-native";
import { Query } from "react-kunyora";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { MediumText } from "../components/AppText";
import UnOrderedStepIndicator, {
  IndicatorRow
} from "../components/Notifications/UnOrderedStepIndicator";
import PaddingProvider from "../containers/PaddingProvider";
import NavigationProvider from "../containers/NavigationProvider";
import NativeTouchableFeedback from "../components/NativeTouchableFeedback";
import UIRenderer from "../components/UIRenderer";
import { ReduxContext } from "../context/ReduxContext";

class NotificationScreen extends React.PureComponent {
  state = {
    currentTab: "activity"
  };

  static propTypes = {
    notificationData: PropTypes.any,
    contractsData: PropTypes.any,
    notificationsLoading: PropTypes.bool,
    contractsLoading: PropTypes.bool,
    screenProps: PropTypes.object
  };

  componentDidUpdate(prevProps) {
    let { notificationData, contractsData, screenProps } = this.props,
      {
        notificationData: prevNotificationData,
        contractsData: prevContractsData
      } = prevProps;

    let _notificationData =
        (notificationData &&
          notificationData.entity &&
          notificationData.entity.activities) ||
        null,
      _contractData =
        (contractsData &&
          contractsData.entity &&
          contractsData.entity.activities) ||
        null,
      _prevNotificationData =
        (prevNotificationData &&
          prevNotificationData.entity &&
          prevNotificationData.entity.activities) ||
        null,
      _prevContractData =
        (prevContractsData &&
          prevContractsData.entity &&
          prevContractsData.entity.activities) ||
        null;

    if (notificationData != prevNotificationData) {
      AsyncStorage.setItem(
        "@notificationLogs",
        JSON.stringify(_notificationData)
      ).then(data => {
        screenProps.setNotificationLogs(_notificationData);
      });
    }

    if (contractsData != prevContractsData) {
      AsyncStorage.setItem("@contractLogs", JSON.stringify(_contractData)).then(
        data => {
          screenProps.setContractLogs(_contractData);
        }
      );
    }
  }

  switchTab = tab => {
    this.setState(
      {
        currentTab: tab
      },
      () => {
        this.pager.setPage(this.state.currentTab == "activity" ? 0 : 1);
      }
    );
  };

  onPageSelected = e => {
    let {
      nativeEvent: { position }
    } = e;
    this.setState({
      currentTab: parseInt(position) == 0 ? "activity" : "booking"
    });
  };

  renderActivityTab = () => {
    let { currentTab } = this.state;
    return (
      <NativeTouchableFeedback onPress={() => this.switchTab("activity")}>
        <View
          style={[styles.tab, currentTab == "activity" ? styles.activeTab : {}]}
        >
          <MediumText
            style={[
              styles.tabLabel,
              currentTab == "activity" ? styles.activeTabLabel : {}
            ]}
          >
            My Activity Log
          </MediumText>
        </View>
      </NativeTouchableFeedback>
    );
  };

  renderBookingTab = () => {
    let { currentTab } = this.state;
    return (
      <NativeTouchableFeedback onPress={() => this.switchTab("booking")}>
        <View
          style={[styles.tab, currentTab == "booking" ? styles.activeTab : {}]}
        >
          <MediumText
            style={[
              styles.tabLabel,
              currentTab == "booking" ? styles.activeTabLabel : {}
            ]}
          >
            My Booking Notifications
          </MediumText>
        </View>
      </NativeTouchableFeedback>
    );
  };

  render() {
    let { currentTab } = this.state,
      {
        notificationsLoading,
        contractsLoading,
        contractLogs,
        notificationLogs
      } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <UIRenderer
            loading={
              !contractLogs &&
              !notificationLogs &&
              (notificationsLoading || contractsLoading)
            }
            isRefreshing={notificationsLoading || contractsLoading}
            error={!contractLogs && !notificationLogs && this.props.error}
            timeElapseToMountIndicator={1000}
            containerType="plain"
            onReload={() => {
              this.props.refetchNofications();
              this.props.refetchContracts();
            }}
            containerStyle={{ paddingTop: 10 }}
            contentContainerStyle={{ flex: 1 }}
          >
            <UnOrderedStepIndicator
              activityItems={notificationLogs}
              onPageSelected={this.onPageSelected}
              bookingItems={contractLogs}
              renderItem={(item, i) => (
                <IndicatorRow
                  activity={item.remark}
                  time={item.created}
                  key={i}
                />
              )}
            />
          </UIRenderer>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _NotificationScreen = props => (
  <ReduxContext.Consumer>
    {({ screenProps }) => (
      <Query
        operation="getNotifications"
        options={{ fetchPolicy: "network-only" }}
      >
        {(notifications, fetchMoreNotifications, refetchNotificationQuery) => (
          <Query
            operation="getContractsLogs"
            options={{ fetchPolicy: "network-only" }}
          >
            {(contracts, fetchMoreContracts, refetchContracts) => {
              return (
                <NotificationScreen
                  notificationData={notifications.data}
                  contractsData={contracts.data}
                  notificationsLoading={notifications.loading}
                  contractsLoading={contracts.loading}
                  error={contracts.error || notifications.error}
                  refetchNofications={refetchNotificationQuery}
                  refetchContracts={refetchContracts}
                  {...props}
                  screenProps={screenProps}
                />
              );
            }}
          </Query>
        )}
      </Query>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state) {
  return {
    notificationLogs: state.notificationLogs,
    contractLogs: state.contractLogs
  };
}

export default connect(mapStateToProps)(_NotificationScreen);

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: "row"
  },
  tab: {
    paddingVertical: 2,
    alignItems: "center",
    justifyContent: "center",
    flex: 0.5,
    borderRadius: 20
  },
  activeTab: {
    backgroundColor: "#00b0cf"
  },
  activeTabLabel: {
    color: "#fff"
  },
  tabLabel: {
    color: "#333"
  }
});
