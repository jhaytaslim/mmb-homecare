import React from "react";
import { StyleSheet, View, Alert, Platform } from "react-native";
import { Item, Input, Textarea, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Colors from "../assets/Colors";
import AppHeader from "../components/AppHeader";
import AppButton from "../components/AppButton";
import PaddingProvider from "../containers/PaddingProvider";
import NavigationProvider from "../containers/NavigationProvider";
import LoadingView from "../components/LoadingView";
import ToastStyle from "../assets/ToastStyle";
import { Util } from "../utils";

class HelpScreen extends React.PureComponent {
  state = {
    Comment: "",
    subject: ""
  };

  static propTypes = {
    loading: PropTypes.bool,
    contactAdmin: PropTypes.func,
    currentUser: PropTypes.object
  };

  contactAdmin = () => {
    let {
        currentUser: { first_name, last_name, email }
      } = this.props,
      { subject, Comment } = this.state,
      data = {
        first_name,
        last_name,
        email,
        subject: subject.trim().length > 0 ? subject : "Need Help",
        Comment
      };

    if (Comment.trim().length == 0) {
      Alert.alert(
        "Error Message",
        `You cannot send an empty message`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    } else {
      this.props
        .contactAdmin({ data })
        .then(result => {
          let {
            status: { code, desc }
          } = result;
          if (code == 106) {
            Util.logout(this.props.navigation);
          } else {
            Toast.show({
              text: `${desc}`,
              textStyle: ToastStyle.toast,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonSuccessTextStyle,
              duration: 10000,
              type: "success",
              buttonText: "OKAY"
            });
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            textStyle: ToastStyle.toast,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            duration: 10000,
            type: "danger",
            buttonText: "OKAY"
          })
        );
    }
  };

  render() {
    let {
        currentUser: { first_name, last_name, email },
        navigation: { goBack },
        loading
      } = this.props,
      { subject, Comment } = this.state;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Send us a mesage" />
          <PaddingProvider style={{ marginTop: 10 }}>
            <Item style={styles.item} rounded>
              <Input
                value={`${first_name} ${last_name}`}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Your Name"
                editable={false}
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.item} rounded>
              <Input
                value={email}
                editable={false}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Email Address"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.item} rounded>
              <Input
                onChangeText={subject => this.setState({ subject })}
                value={subject}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Enter the Subject"
                style={styles.textInput}
              />
            </Item>
            <Textarea
              placeholderTextColor={Colors.roundedInputPlaceholderColor}
              rowSpan={5}
              onChangeText={Comment => this.setState({ Comment })}
              value={Comment}
              bordered
              style={[styles.item, styles.textInput, { borderRadius: 5 }]}
              placeholder="How can we help you?"
            />
            <View style={styles.footer}>
              <View style={styles.sendContainer}>
                <AppButton
                  title="Send"
                  onPress={this.contactAdmin}
                  style={{ flex: 1 }}
                />
              </View>
              <AppButton
                title="Cancel"
                onPress={() => goBack()}
                style={{
                  backgroundColor: "red",
                  marginTop: 10,
                  alignSelf: "center"
                }}
              />
            </View>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _HelpScreen = props => (
  <Mutation operation="createContactAdmin">
    {(mutationState, mutate) => (
      <HelpScreen
        loading={mutationState.loading}
        contactAdmin={mutate}
        {...props}
      />
    )}
  </Mutation>
);

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps)(_HelpScreen);

const styles = StyleSheet.create({
  item: {
    marginBottom: 10,
    backgroundColor: "#fff"
  },
  footer: {
    marginVertical: 10
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  },
  sendContainer: {
    flexDirection: "row"
  }
});
