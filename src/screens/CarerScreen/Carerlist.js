import React from "react";
import { List } from "native-base";
import PropTypes from "prop-types";
import { StyleSheet, View } from "react-native";
import { Query } from "react-kunyora";
import { connect } from "react-redux";

import { RoundedListItem } from "../../components/ListItem";
import { LightText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import Icon from "../../components/Icon";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import UnauthorizedModal from "../../components/Carer/UnauthorizedModal";
import Filter from "../../components/Filter";
import AppList from "../../components/AppList";

class CarerList extends React.PureComponent {
  state = {
    isModalVisible: false,
    pageCount: 0,
    isFilterModalVisible: false
  };

  static propTypes = {
    loading: PropTypes.bool.isRequired,
    error: PropTypes.any,
    data: PropTypes.any,
    subscriptionStatus: PropTypes.number,
    fetchMore: PropTypes.func,
    refetchQuery: PropTypes.func,
    total_pages: PropTypes.any
  };

  fetchMore = event => {
    let {
        contentOffset: { y },
        contentSize: { height },
        layoutMeasurement
      } = event.nativeEvent,
      { fetchMore, total_pages } = this.props,
      { pageCount } = this.state;

    if (height - layoutMeasurement.height == y && pageCount + 1 < total_pages) {
      fetchMore({
        config: {
          params: {
            page: pageCount + 1
          }
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          this.setState(prevState => ({
            pageCount: prevState.pageCount + 1
          }));

          return {
            ...fetchMoreResult,
            entity: {
              ...fetchMoreResult,
              carers: [
                ...previousResult.entity.carers,
                ...fetchMoreResult.entity.carers
              ]
            }
          };
        }
      });
    }
  };

  determineEngagementStatus = carer => {
    let {
      subscriptionStatus,
      navigation: { navigate }
    } = this.props;

    if (subscriptionStatus == 0) {
      this.setState({ isModalVisible: true });
    } else {
      navigate("engageCarer", { carer });
    }
  };

  fetchCarerByFilter = filters => {
    this.setState({ isFilterModalVisible: false }, () => {
      this.props.refetchQuery({
        params: {
          page: this.state.pageCount,
          state: filters.state,
          specialties: filters.specialty,
          category: filters.category
        }
      });
    });
  };

  render() {
    let {
      navigation: { navigate },
      loading,
      data,
      isInitialDataSet
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <Filter
          isVisible={this.state.isFilterModalVisible}
          onClose={this.fetchCarerByFilter}
        />
        <UnauthorizedModal
          isVisible={this.state.isModalVisible}
          onSubscribe={() =>
            this.setState({ isModalVisible: false }, () =>
              navigate("subscribe")
            )
          }
          onClose={() => this.setState({ isModalVisible: false })}
        />
        <AppHeader
          title="Verified Health Workers"
          titleHeaderRightComponent={
            <Icon
              name="filter"
              type="font-awesome-icon"
              style={styles.icon}
              onPress={() => this.setState({ isFilterModalVisible: true })}
            />
          }
        />
        <AppList
          loading={loading && !isInitialDataSet}
          dummyMode={true}
          error={this.props.error}
          dataArray={data && data.entity && data.entity.carers}
          timeElapseToMountIndicator={1000}
          fetchMore={this.fetchMore}
          containerStyle={{ paddingHorizontal: 15, marginTop: 10 }}
          refetchQuery={() => this.props.refetchQuery()}
          isFetchingMore={this.props.loading && this.props.isInitialDataSet}
          renderRow={carer => {

            return (
              <RoundedListItem
                isVerified={carer.verify_status == 1 ? true : false}
                name={`${carer.user_id.first_name}`}
                carerCategory={`${carer.category
                  .charAt(0)
                  .toUpperCase()}${carer.category.slice(1)}`}
                healthCategory={carer.user_id.occupation}
                ratingsCount={parseInt(carer.rating_ || 0)}
                photo={carer.user_id.imageUrl}
                location={carer.state && carer.state.name}
                price={carer.con_price}
                slug={carer.specialties}
                isOnline={carer.is_online}
                style={styles.listItem}
                photoSize={78}
                onPress={() => this.determineEngagementStatus(carer)}
              />
            );
          }}
        />
      </NavigationProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    subscriptionStatus: state.currentUser.subscription_status
  };
}

const _CarerList = props => {
  let {
      navigation: { state }
    } = props,
    _params = state.params || {},
    _carer_id = _params.carer_id,
    config = undefined;

  if (_carer_id != undefined) {
    config = { params: { carer_id: _carer_id } };
  }

  return (
    <Query operation="getCarers" options={{ config }}>
      {(
        { data, error, loading, isInitialDataSet },
        fetchMore,
        refetchQuery
      ) => (
        <CarerList
          loading={loading}
          error={error}
          data={data}
          {...props}
          fetchMore={fetchMore}
          refetchQuery={refetchQuery}
          isInitialDataSet={isInitialDataSet}
          total_pages={
            data &&
            data.entity &&
            data.entity.carers &&
            data.entity.carers.total_pages
          }
        />
      )}
    </Query>
  );
};

export default connect(mapStateToProps)(_CarerList);

const styles = StyleSheet.create({
  listItem: {
    marginBottom: 10
  },
  icon: {
    color: "#fff",
    fontSize: Fonts.appHeader.icons
  }
});
