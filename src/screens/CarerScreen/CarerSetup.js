import React from "react";
import { View, StyleSheet, Alert } from "react-native";
import { connect } from "react-redux";

import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";

class CarerSetup extends React.PureComponent {
  navigateUser = (routeName, status) => {
    let {
      navigation: { navigate }
    } = this.props;

    if (status == 1 || status == 2) {
      navigate(routeName);
    } else {
      Alert.alert(
        "Message",
        "Sorry, you have to complete all previous sections before proceeding",
        [{ text: "Okay!", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let {
      navigation: { navigate, goBack },
      navigation,
      currentCarer: { veri_status, prof_status, intro_status, docu_status }
    } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        <React.Fragment>
          <AppHeader
            title="Carer Setup"
            onPressBackButton={() => navigate("tabRoutes")}
          />
          <PaddingProvider type="plain" style={styles.content}>
            <NativeTouchableFeedback
              onPress={() => navigate("professionalDetails")}
            >
              <View style={styles.section}>
                <MediumText
                  style={[styles.title, prof_status == 1 && styles.activeTitle]}
                >
                  Carer Professional Details
                </MediumText>
                {prof_status == 1 && (
                  <View style={styles.badge}>
                    <MediumText style={styles.badgeText}>COMPLETED</MediumText>
                  </View>
                )}
              </View>
            </NativeTouchableFeedback>

            <NativeTouchableFeedback
              onPress={() => this.navigateUser("verificationDocs", docu_status)}
            >
              <View style={styles.section}>
                <MediumText
                  style={[styles.title, docu_status == 1 && styles.activeTitle]}
                >
                  Verification Documents
                </MediumText>
                {docu_status == 1 && (
                  <View style={styles.badge}>
                    <MediumText style={styles.badgeText}>COMPLETED</MediumText>
                  </View>
                )}
              </View>
            </NativeTouchableFeedback>

            <NativeTouchableFeedback
              onPress={() => this.navigateUser("carerIntro", intro_status)}
            >
              <View style={styles.section}>
                <MediumText
                  style={[
                    styles.title,
                    intro_status == 1 && styles.activeTitle
                  ]}
                >
                  Carer Introduction
                </MediumText>
                {intro_status == 1 && (
                  <View style={styles.badge}>
                    <MediumText style={styles.badgeText}>COMPLETED</MediumText>
                  </View>
                )}
              </View>
            </NativeTouchableFeedback>

            <NativeTouchableFeedback
              onPress={() => this.navigateUser("carerPaymentInfo", veri_status)}
            >
              <View style={styles.section}>
                <MediumText
                  style={[styles.title, veri_status == 1 && styles.activeTitle]}
                >
                  Payment Information
                </MediumText>
                {veri_status == 1 && (
                  <View style={styles.badge}>
                    <MediumText style={styles.badgeText}>COMPLETED</MediumText>
                  </View>
                )}
              </View>
            </NativeTouchableFeedback>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentCarer: state.currentCarer
  };
}

export default connect(mapStateToProps)(CarerSetup);

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 0
  },
  section: {
    backgroundColor: "#fff",
    flexDirection: "row",
    paddingHorizontal: 15,
    height: 60,
    alignItems: "center",
    justifyContent: "space-between"
  },
  title: {
    color: "#8594a3",
    fontSize: 15
  },
  activeTitle: {
    color: "#00b0cf"
  },
  badge: {
    borderRadius: 3,
    borderWidth: 1,
    borderColor: "#00b0cf",
    height: 25,
    alignItems: "center",
    width: 100,
    justifyContent: "center"
  },
  badgeText: {
    fontSize: 10,
    color: "#00b0cf"
  }
});
