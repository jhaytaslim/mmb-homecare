import React from "react";
import { View, Alert, Platform } from "react-native";
import PropTypes from "prop-types";
import StarRating from "react-native-star-rating";
import { Mutation } from "react-kunyora";
import { Content, Toast } from "native-base";

import AppButton from "../../components/AppButton";
import { LightText } from "../../components/AppText";
import Colors from "../../assets/Colors";
import Fonts from "../../assets/Fonts";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import ProfileDisplayCard from "../../components/ProfileDisplayCard";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import TabsNavigationContext from "../../context/TabsNavigationContext";

class CarerVerify extends React.PureComponent {
  state = {
    rating: 0
  };

  static propTypes = {
    loading: PropTypes.bool,
    verifyVisit: PropTypes.func
  };

  verifyVisit = () => {
    let { verifyVisit } = this.props,
      {
        navigation: {
          state: {
            params: {
              next_visit: { visit_id }
            }
          },
          navigate
        }
      } = this.props;

    if (this.state.rating) {
      verifyVisit({
        data: { visit_id, rating: this.state.rating }
      })
        .then(result => {
          let {
            status: { code, desc }
          } = result;

          if (code == 100) {
            Toast.show({
              text: `${desc}`,
              textStyle: ToastStyle.toast,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonSuccessTextStyle,
              type: "success",
              buttonText: "OKAY",
              duration: 10000
            });
            this.props.onChangeTab(0);
            navigate("tabRoutes");
          } else if (code == 106) {
            Util.logout(this.props.navigation);
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  textStyle: ToastStyle.toast,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                  type: "danger",
                  buttonText: "OKAY",
                  duration: 30000
                });
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            textStyle: ToastStyle.toast,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            type: "danger",
            buttonText: "OKAY",
            duration: 10000
          })
        );
    } else {
      Alert.alert(
        "Error Message",
        "You need to rate the Carer to verify visit",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let {
      navigation: {
        state: {
          params: {
            visit_details: { care_category, intro_title, carer_name },
            next_visit: { price }
          }
        }
      },
      loading
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Verify Visit" />
          <PaddingProvider style={styles.paddingProvider}>
            <LightText style={styles.typeText}>{care_category}</LightText>
            <ProfileDisplayCard
              amountPaid={Util.formatToNaira(price)}
              serviceFor="Initial Visit"
              name={carer_name}
              title={intro_title || ""}
              renderFooter={
                <View>
                  <LightText
                    style={[styles.largeText, { textAlign: "center" }]}
                  >
                    Rate this Carer
                  </LightText>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={this.state.rating}
                    fullStarColor={"orange"}
                    starSize={20}
                    selectedStar={rating => this.setState({ rating })}
                  />
                </View>
              }
            />
            <AppButton
              style={styles.appButton}
              title="Verify"
              onPress={this.verifyVisit}
            />
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CarerVerify = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <Mutation operation="createVerifyVisit">
        {(mutationState, mutate) => (
          <CarerVerify
            loading={mutationState.loading}
            verifyVisit={mutate}
            onChangeTab={onChangeTab}
            {...props}
          />
        )}
      </Mutation>
    )}
  </TabsNavigationContext.Consumer>
));

const styles = {
  paddingProvider: {
    alignItems: "center"
  },
  typeText: {
    color: "#fff",
    fontSize: 15,
    textAlign: "center",
    marginBottom: 10
  },
  largeText: {
    fontSize: Fonts.profileDisplayCard.smallText,
    color: Colors.profileDisplayCard.largeText,
    paddingVertical: 2
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10
  }
};
