import React from "react";
import { View, Alert, Platform } from "react-native";
import { Input, Item, Textarea, Toast, Content } from "native-base";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";

import AppButton from "../../components/AppButton";
import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import NavigationProvider from "../../containers/NavigationProvider";
import PaddingProvider from "../../containers/PaddingProvider";
import Icon from "../../components/Icon";
import Fonts from "../../assets/Fonts";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class CarerMessage extends React.PureComponent {
  state = {
    location: "",
    message: ""
  };

  static propTypes = {
    loading: PropTypes.bool,
    mutate: PropTypes.func
  };

  startConversation = () => {
    let {
        navigation: {
          state: {
            params: { carer_id }
          },
          navigate
        }
      } = this.props,
      { message } = this.state;

    this.props
      .mutate({ data: { carer_id, message } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (parseInt(code) == 100) {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            duration: 10000,
            type: "success",
            buttonText: "OKAY",
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
          navigate("chats", { convo_info: entity.convo });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                duration: 30000,
                type: "danger",
                buttonText: "OKAY"
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          duration: 10000,
          type: "danger",
          buttonText: "OKAY"
        })
      );
  };

  render() {
    let {
        navigation: {
          state: {
            params: { carer_id, intro_title, con_price }
          },
          navigate
        },
        loading
      } = this.props,
      { message, location } = this.state;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Engage a Carer" />
          <PaddingProvider type="plain">
            <Content showsVerticalScrollIndicator={false}>
              <MediumText style={styles.Text}>Title : {intro_title}</MediumText>
              <MediumText style={styles.Text}>Price : {con_price}</MediumText>
              <Item rounded style={styles.Location}>
                <Icon name="location" type="evil-icon" />
                <Input
                  placeholder="Location"
                  value={location}
                  style={styles.textInput}
                  onChangeText={location => this.setState({ location })}
                />
              </Item>
              <Textarea
                rowSpan={10}
                onChangeText={message => this.setState({ message })}
                value={message}
                bordered
                style={styles.Talk}
                placeholder="Talk to a carer"
              />
              <AppButton
                style={styles.appButton}
                title="Send"
                onPress={this.startConversation}
              />
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CarerMessage = props => (
  <Mutation operation="createStartConversation">
    {(mutationState, mutate) => (
      <CarerMessage
        loading={mutationState.loading}
        mutate={mutate}
        {...props}
      />
    )}
  </Mutation>
));

const styles = {
  Talk: {
    height: 200,
    backgroundColor: "#fff",
    fontFamily: "Quicksand-Medium",
    borderRadius: 5
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  },
  TalkItem: {
    alignItems: "flex-start",
    padding: 20,
    backgroundColor: "rgb(255,255,255)",
    shadowRadius: 5,
    marginBottom: 20
  },
  TalkIcon: {
    marginLeft: 10,
    marginTop: 10,
    height: 20
  },
  Text: {
    fontSize: Fonts.largeText,
    paddingBottom: 10,
    color: "#333"
  },
  Location: {
    borderRadius: 5,
    backgroundColor: "rgb(255,255,255)",
    shadowRadius: 5,
    marginBottom: 20
  },
  appButton: {
    marginVertical: 10,
    alignSelf: "center"
  }
};
