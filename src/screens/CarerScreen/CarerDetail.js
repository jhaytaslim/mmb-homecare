import React from "react";
import { View, Share } from "react-native";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";
import { Content, Toast } from "native-base";

import AppButton from "../../components/AppButton";
import { MediumText } from "../../components/AppText";
import Colors from "../../assets/Colors";
import Fonts from "../../assets/Fonts";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import NavigationProvider from "../../containers/NavigationProvider";
import ProfileDisplayCard from "../../components/ProfileDisplayCard";
import Icon from "../../components/Icon";
import LoadingView from "../../components/LoadingView";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class CarerDetail extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    checkStatus: PropTypes.func
  };

  checkStatus = () => {
    let {
      navigation: {
        state: {
          params: {
            carer: { carer_id, intro_title, con_price }
          }
        },
        navigate
      },
      checkStatus
    } = this.props;

    checkStatus({ data: { carer_id } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (code == 113) {
          navigate("carerMessage", { carer_id, intro_title, con_price });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          navigate("chats", { convo_info: entity.convo });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          type: "danger",
          buttonText: "OKAY",
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          buttonStyle: ToastStyle.buttonStyle
        })
      );
  };

  share = () => {
    let {
      navigation: {
        state: {
          params: {
            carer: {
              specialties,
              carer_id,
              intro_title,
              con_price,
              user_id: { first_name }
            }
          }
        }
      }
    } = this.props;

    Share.share(
      {
        message: `"Check out some services such as ${specialties} from ${first_name} with a consultation price of ${con_price}. Check this out on "@homecare https://www.mmbhomecare.com/carer/${carer_id}`,
        url: `https://www.mmbhomecare.com/carer/${carer_id}`,
        title: intro_title
      },
      {
        dialogTitle: `Share ${intro_title}`
      }
    ).then(Share => {
      //@Todo send to analytics
    });
  };

  render() {
    let {
      navigation: {
        state: {
          params: {
            carer: {
              specialties,
              carer_id,
              intro_title,
              intro_body,
              con_price,
              job_title,
              user_id: { first_name, imageUrl }
            }
          }
        },
        navigate
      },
      loading
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Carer Detail" />
          <PaddingProvider style={styles.paddingProvider}>
            <Content>
              <MediumText style={[styles.typeText, { marginBottom: 10 }]}>
                {specialties}
              </MediumText>
              <ProfileDisplayCard
                amountPaid={Util.formatToNaira(con_price)}
                serviceFor="Initial Consultation Fee"
                src={imageUrl}
                name={`${first_name}`}
                title={intro_title || ""}
                renderFooter={
                  <MediumText style={styles.smallText}>
                    {intro_body || "No addtional information listed"}
                  </MediumText>
                }
              />
              <AppButton
                style={styles.appButton}
                title="Engage Carer"
                onPress={this.checkStatus}
              />
              <NativeTouchableFeedback onPress={this.share}>
                <View style={styles.shareContainer}>
                  <MediumText style={styles.typeText}>Share : </MediumText>
                  <Icon
                    contentContainerStyle={styles.iconContainer}
                    type="ionic-icon"
                    name="md-share"
                    style={styles.share}
                  />
                </View>
              </NativeTouchableFeedback>
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _CarerDetails = props => (
  <Mutation operation="createCheckConversationStatus">
    {(mutationState, mutate) => (
      <CarerDetail
        loading={mutationState.loading}
        checkStatus={mutate}
        {...props}
      />
    )}
  </Mutation>
);

export default _CarerDetails;

const styles = {
  paddingProvider: {
    backgroundColor: "#0096c0",
    alignItems: "center"
  },
  typeText: {
    color: "#fff",
    fontSize: Fonts.smallText,
    textAlign: "center"
  },
  smallText: {
    fontSize: Fonts.profileDisplayCard.smallText,
    color: Colors.profileDisplayCard.smallText,
    textAlign: "center"
  },
  shareContainer: {
    marginTop: 30,
    flexDirection: "row",
    borderRadius: 50,
    borderColor: "rgb(101, 223, 237)",
    borderWidth: 2,
    width: 250,
    height: 50,
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  iconContainer: {
    marginLeft: 10
  },
  appButton: {
    marginTop: 10,
    alignSelf: "center"
  },
  share: {
    color: "#fff",
    fontSize: 20
  }
};
