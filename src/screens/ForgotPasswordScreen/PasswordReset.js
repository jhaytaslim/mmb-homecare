import React from "react";
import {
  StyleSheet,
  View,
  ImageBackground,
  Alert,
  Platform
} from "react-native";
import { Content, Item, Icon, Input, Toast } from "native-base";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";

import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import AppStatusBar from "../../components/AppStatusBar";

class PasswordReset extends React.PureComponent {
  state = {
    pass: "",
    cpass: ""
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    mutate: PropTypes.func
  };

  updatePassword = () => {
    let {
        navigation: {
          state: {
            params: { email, code }
          },
          navigate,
          goBack
        }
      } = this.props,
      { pass, cpass } = this.state;

    if (pass.trim() == cpass.trim() && pass.trim().length > 0) {
      this.props
        .mutate({
          data: {
            pass: pass.trim(),
            cpass: cpass.trim(),
            code: code.trim(),
            email: email.trim()
          }
        })
        .then(result => {
          let {
            status: { code, desc }
          } = result;

          if (parseInt(code) == 100) {
            navigate("login");
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  duration: 30000,
                  textStyle: ToastStyle.toast,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                  type: "danger",
                  buttonText: "OKAY"
                });
            setTimeout(() => goBack(), 2000);
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            duration: 10000,
            textStyle: ToastStyle.toast,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            type: "danger",
            buttonText: "OKAY"
          })
        );
    } else {
      Alert.alert(
        "Error Message",
        "Please make sure the password is not empty and equal to the confirmed",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let {
        navigation: { navigate },
        loading
      } = this.props,
      { pass, cpass } = this.state;

    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={styles.container}
      >
        {loading && <LoadingView />}
        <AppStatusBar />
        <Content>
          <View style={styles.body}>
            <MediumText style={styles.title}>SET UP PASSWORD</MediumText>
            <Item style={styles.inputContainer}>
              <Icon active name="md-lock" style={styles.inputIcon} />
              <Input
                placeholder="New Password*"
                placeholderTextColor="#fff"
                secureTextEntry={true}
                style={styles.textInput}
                value={pass}
                onChangeText={pass => this.setState({ pass })}
              />
            </Item>
            <Item style={styles.inputContainer}>
              <Icon active name="md-lock" style={styles.inputIcon} />
              <Input
                placeholder="Confirm New Password*"
                placeholderTextColor="#fff"
                secureTextEntry={true}
                style={styles.textInput}
                value={cpass}
                onChangeText={cpass => this.setState({ cpass })}
              />
            </Item>
            <AppButton
              title="Update Password"
              style={styles.appButton}
              onPress={this.updatePassword}
            />
          </View>
        </Content>
      </ImageBackground>
    );
  }
}

export default (_PasswordReset = props => (
  <Mutation operation="updatePassword">
    {(mutationState, mutate) => (
      <PasswordReset
        loading={mutationState.loading}
        mutate={mutate}
        {...props}
      />
    )}
  </Mutation>
));

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 30
  },
  title: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.defaultText,
    marginTop: 70
  },
  inputIcon: {
    color: Colors.signUp.input
  },
  inputContainer: {
    marginTop: 20
  },
  textInput: {
    color: Colors.signUp.input,
    fontFamily: "Quicksand-Medium"
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10,
    backgroundColor: "red"
  }
});
