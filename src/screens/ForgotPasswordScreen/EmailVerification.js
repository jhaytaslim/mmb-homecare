import React from "react";
import {
  StyleSheet,
  View,
  ImageBackground,
  Alert,
  Platform
} from "react-native";
import { Content, Item, Icon, Input, Toast } from "native-base";
import PropTypes from "prop-types";
import { Mutation } from "react-kunyora";

import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import AppStatusBar from "../../components/AppStatusBar";

class EmailVerification extends React.PureComponent {
  state = {
    email: ""
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    mutate: PropTypes.func
  };

  resetPassword = () => {
    let { email } = this.state,
      {
        navigation: { navigate }
      } = this.props;

    if (email.trim().length > 0 && /^[^\s@]+@[^\s@.]+\.[^\s@]+$/.test(email)) {
      this.props
        .mutate({ data: { email } })
        .then(result => {
          let {
            status: { code, desc }
          } = result;
          if (parseInt(code) == 100) {
            Alert.alert(
              "Success Message",
              `${desc}, please check your mail or phone for the OTP code sent`,
              [{ text: "Okay", onPress: () => null }],
              { cancelable: false }
            );

            setTimeout(
              () =>
                navigate("mobileVerification", {
                  email,
                  accountType: "passwordReset"
                }),
              3000
            );
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  duration: 30000,
                  textStyle: ToastStyle.toast,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle,
                  type: "danger",
                  buttonText: "OKAY"
                });
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            duration: 10000,
            textStyle: ToastStyle.toast,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            type: "danger",
            buttonText: "OKAY"
          })
        );
    } else {
      Alert.alert(
        "Error Message",
        "Email address is not valid",
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    }
  };

  render() {
    let {
      navigation: { navigate },
      loading
    } = this.props;

    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={styles.container}
      >
        {loading && <LoadingView />}
        <AppStatusBar />
        <Content>
          <View style={styles.body}>
            <MediumText style={styles.title}>FORGOT PASSWORD?</MediumText>
            <Item style={styles.inputContainer}>
              <Icon active name="ios-mail-outline" style={styles.inputIcon} />
              <Input
                placeholder="Email Address"
                placeholderTextColor="#fff"
                value={this.state.email}
                style={styles.textInput}
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <AppButton
              title="Request Reset"
              style={StyleSheet.flatten([
                styles.appButton,
                styles.registerButton
              ])}
              onPress={this.resetPassword}
            />
            <AppButton
              title="Login Instead?"
              style={styles.appButton}
              onPress={() => navigate("login")}
            />
          </View>
        </Content>
      </ImageBackground>
    );
  }
}

export default (_EmailVerification = props => (
  <Mutation operation="createPasswordReset">
    {(mutationState, mutate) => (
      <EmailVerification
        loading={mutationState.loading}
        mutate={mutate}
        {...props}
      />
    )}
  </Mutation>
));

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: 30
  },
  title: {
    color: Colors.signUp.defaultText,
    fontSize: Fonts.signUp.defaultText,
    marginTop: 70
  },
  inputIcon: {
    color: Colors.signUp.input
  },
  inputContainer: {
    marginTop: 20
  },
  textInput: {
    color: Colors.signUp.input,
    fontFamily: "Quicksand-Medium"
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10
  },
  registerButton: {
    backgroundColor: "red"
  }
});
