import React from "react";
import { View, AsyncStorage } from "react-native";
import { Query } from "react-kunyora";
import PropTypes from "prop-types";
import moment from "moment";
import { connect } from "react-redux";

import { LightText, MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import AppButton from "../../components/AppButton";
import SubscriptionList from "../../components/Subscriptions/SubscriptionsList";
import NavigationProvider from "../../containers/NavigationProvider";
import UIRenderer from "../../components/UIRenderer";
import { ReduxContext } from "../../context/ReduxContext";

class MySubscription extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    subscription: PropTypes.object,
    screenProps: PropTypes.object
  };

  state = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
    expired: false
  };

  componentDidMount() {
    this.props._subscription && this.calculateTimeDifference();
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.animationFrame);
  }

  componentDidUpdate(prevProps) {
    let { loading, subscription } = this.props,
      { subscription: prevSubscription } = prevProps,
      _prevEntity = (prevSubscription && prevSubscription.entity) || undefined;

    if (!loading && subscription && subscription.entity) {
      if (_prevEntity != subscription.entity) {
        AsyncStorage.setItem(
          "@subscription",
          JSON.stringify(subscription.entity)
        ).then(result => {
          this.props.screenProps.setSubscription(subscription.entity);
          this.calculateTimeDifference();
        });
      }
    }
  }

  calculateTimeDifference = () => {
    let { _subscription } = this.props;
    if (_subscription && _subscription.active_sub) {
      let {
        _subscription: {
          active_sub: { expiring, created }
        }
      } = this.props;
      let _exp = expiring;
      expiring = `${expiring} 00:00:00`;

      if (!moment(_exp).isBefore(new Date())) {
        let created = new Date();
        created = `${moment(created).format("YYYY")}-${moment(created).format(
          "MM"
        )}-${moment(created).format("DD")} 00:00:00`;

        let startYear = moment(created).format("YYYY"),
          startMonth = moment(created).format("MM") - 1,
          startDay = moment(created).format("D"),
          startHour = moment(created).format("H"),
          startMinutes = moment(created).format("mm"),
          startSeconds = moment(created).format("ss"),
          endYear = moment(expiring).format("YYYY"),
          endMonth = moment(expiring).format("MM") - 1,
          endDay = moment(expiring).format("D"),
          endHour = moment(expiring).format("H"),
          endMinutes = moment(expiring).format("mm"),
          endSeconds = moment(expiring).format("ss"),
          startDateMoment = moment([
            startYear,
            startMonth,
            startDay,
            startMinutes,
            startSeconds
          ]),
          endDateMoment = moment([
            endYear,
            endMonth,
            endDay,
            endMinutes,
            endSeconds
          ]),
          minutesDiff = endDateMoment.diff(startDateMoment, "seconds"),
          days = Math.floor(minutesDiff / 86400),
          hours = Math.floor((minutesDiff % 86400) / 3600),
          minutes = Math.floor((minutesDiff % 86400) / 3600 / 60),
          seconds = Math.floor((minutesDiff % 86400) / 3600 / 60 / 1000);

        this.setState(
          {
            hours,
            seconds,
            minutes,
            days
          },
          () => this.runContinousTimeCalculation()
        );
      } else {
        this.setState({
          expired: true
        });
      }
    }
  };

  runContinousTimeCalculation = () => {
    let startTime = Date.now(),
      { days } = this.state,
      { _subscription } = this.props,
      _entity = _subscription || {},
      _all_sub = _entity.all_sub,
      _activeSub = _entity.active_sub;

    this.animation = timestamp => {
      let elapsed = Math.floor((timestamp - startTime) / 1000);
      if (elapsed < 1) {
        this.animationFrame = requestAnimationFrame(this.animation);
      } else if (days == undefined) {
        cancelAnimationFrame(this.animation);
      } else {
        startTime = Date.now();
        this.setState(
          prevState => {
            return {
              seconds: prevState.seconds == 0 ? 59 : prevState.seconds - 1,
              minutes:
                prevState.seconds == 0
                  ? prevState.minutes == 0
                    ? 59
                    : prevState.minutes - 1
                  : prevState.minutes,
              hours:
                prevState.minutes == 0 && prevState.seconds == 0
                  ? prevState.hours == 0
                    ? 23
                    : prevState.hours - 1
                  : prevState.hours,
              days:
                prevState.seconds == 0 &&
                prevState.minutes == 0 &&
                prevState.hours == 0
                  ? prevState.days == 0
                    ? undefined
                    : prevState.days - 1
                  : prevState.days
            };
          },
          () => {
            if (this.state.days != undefined) {
              this.animationFrame = requestAnimationFrame(this.animation);
            }
          }
        );
      }
    };

    this.animationFrame = requestAnimationFrame(this.animation);
  };

  renderTimeLeft = () => {
    let { days, hours, minutes, seconds } = this.state;

    return (
      <View style={styles.timeLeftContainer}>
        <View>
          <LightText style={styles.timeLeftNumber}>{days}</LightText>
          <LightText style={styles.timeLeftText}> days </LightText>
        </View>
        <LightText style={styles.timeLeftNumber}> : </LightText>
        <View>
          <LightText style={styles.timeLeftNumber}>{hours}</LightText>
          <LightText style={styles.timeLeftText}> Hours </LightText>
        </View>
        <LightText style={styles.timeLeftNumber}> : </LightText>
        <View>
          <LightText style={styles.timeLeftNumber}>{minutes}</LightText>
          <LightText style={styles.timeLeftText}> Minutes </LightText>
        </View>
        <LightText style={styles.timeLeftNumber}> : </LightText>
        <View>
          <LightText style={styles.timeLeftNumber}>{seconds}</LightText>
          <LightText style={styles.timeLeftText}> Seconds </LightText>
        </View>
      </View>
    );
  };

  render() {
    let {
        navigation: { navigate },
        loading,
        _subscription,
        refetchQuery,
        error
      } = this.props,
      { expired } = this.state,
      _entity = _subscription || {},
      _all_sub = _entity.all_sub,
      _activeSub = _entity.active_sub;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <UIRenderer
            loading={Object.keys(_entity).length == 0 && loading}
            isRefreshing={loading}
            timeElapseToMountIndicator={2000}
            onReload={() => refetchQuery()}
            error={Object.keys(_entity).length == 0 && error}
            containerStyle={{ paddingHorizontal: 0 }}
          >
            <View style={styles.container}>
              {_activeSub && _activeSub.status != 1 ? (
                <LightText style={{ color: "#fff", fontSize: 16 }}>
                  No active Subcription for this account
                </LightText>
              ) : expired ? (
                <MediumText
                  style={{
                    color: "#fff",
                    fontSize: 20,
                    textAlign: "center",
                    marginBottom: 20
                  }}
                >
                  The Subcription for this account has expired
                </MediumText>
              ) : !_activeSub ? null : (
                <React.Fragment>
                  <LightText style={styles.mediumText}>
                    {`You're on the ${_activeSub.plan} Package`}
                  </LightText>
                  <LightText style={{ ...styles.smallText, ...styles.topText }}>
                    of
                    <LightText style={styles.mediumText}>{`N${
                      _activeSub.amount
                    }`}</LightText>
                    per {_activeSub.recurring_type.toLowerCase()}
                  </LightText>
                  <View style={styles.lineSeperator} />
                  {this.renderTimeLeft()}
                  <View style={styles.lineSeperator} />
                </React.Fragment>
              )}
              <AppButton
                style={styles.appButton}
                title="All Packages"
                onPress={() => navigate("subscribe")}
              />
            </View>
            {_all_sub &&
              _all_sub.length > 0 && <SubscriptionList dataArray={_all_sub} />}
          </UIRenderer>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _MySubscription = props => (
  <ReduxContext.Consumer>
    {({ screenProps }) => (
      <Query
        operation="getSubscription"
        options={{ fetchPolicy: "network-only" }}
      >
        {({ data, loading, error }, fetchMore, refetchQuery) => (
          <MySubscription
            {...props}
            loading={loading}
            error={error}
            refetchQuery={refetchQuery}
            subscription={data}
            screenProps={screenProps}
          />
        )}
      </Query>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state) {
  return {
    _subscription: state.subscription
  };
}

export default connect(mapStateToProps)(_MySubscription);

const styles = {
  container: {
    padding: 20
  },
  mediumText: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 21
  },
  topText: {
    alignSelf: "center"
  },
  smallText: {
    color: "#fff",
    fontSize: Fonts.smallText
  },
  lineSeperator: {
    height: 1,
    backgroundColor: "#fff",
    marginVertical: 10
  },
  timeLeftContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  timeLeftNumber: {
    fontSize: 50,
    color: "#fff"
  },
  timeLeftText: {
    fontSize: 13,
    color: "#fff",
    alignSelf: "center"
  },
  appButton: {
    backgroundColor: "red",
    alignSelf: "center"
  }
};
