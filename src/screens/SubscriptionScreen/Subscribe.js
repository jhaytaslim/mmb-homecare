import React from "react";
import { View, AsyncStorage, Alert, Platform } from "react-native";
import { Query, Mutation } from "react-kunyora";
import { connect } from "react-redux";
import { Content, Toast } from "native-base";

import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import { LightText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import { ReduxContext } from "../../context/ReduxContext";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import PaymentMode from "../../components/PaymentMode";
import TabsNavigationContext from "../../context/TabsNavigationContext";

class Subscribe extends React.PureComponent {
  state = {
    isPaymentModalVisible: false,
    onCardPaymentSelection: () => null
  };

  createPaySubscription = (planVal, mode, bankDirectFunc) => {
    let { setPaystack, mutate, navigation, onChangeTab } = this.props;

    this.setState({ isPaymentModalVisible: false });
    mutate({ data: { plan: planVal, mode } })
      .then(res => {
        let {
          status: { code, desc },
          entity
        } = res;

        if (bankDirectFunc && code != 106) {
          bankDirectFunc(res);
        } else {
          this.setState({ isPaymentModalVisible: false }, () => {
            if (parseInt(code) == 100) {
              setPaystack({ ...entity });
              navigation.navigate("paystack", {
                operation: "createPaymentVerify",
                refetchQueries: [{ operation: "getSubscription" }],
                onSuccess: () => {
                  onChangeTab(2);
                  navigation.navigate("tabRoutes");
                }
              });
            } else if (code == 106) {
              Util.logout(this.props.navigation);
            } else {
              Platform.OS !== "ios"
                ? Alert.alert(
                    "Error Message",
                    desc,
                    [{ text: "Okay", onPress: () => null }],
                    { cancelable: false }
                  )
                : Toast.show({
                    text: desc,
                    textStyle: ToastStyle.toast,
                    type: "danger",
                    buttonText: "okay",
                    duration: 30000,
                    buttonStyle: ToastStyle.buttonStyle,
                    buttonTextStyle: ToastStyle.buttonErrorTextStyle
                  });
            }
          });
        }
      })
      .catch(err => {
        this.setState({ isPaymentModalVisible: false });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      });
  };

  renderBasicPackage = () => (
    <View style={styles.container}>
      <LightText style={styles.mediumText}>Basic Package</LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        is
        <LightText style={styles.mediumText}> #1000 </LightText>
        per month
      </LightText>
      <View style={styles.lineSeperator} />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Validity
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        1 Month
      </LightText>
      <View style={styles.lineSeperator} />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        No. Of Concurrent Care
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        1
      </LightText>
      <View style={styles.lineSeperator} />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Package Validity
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Full Package Validity: 30 Days
      </LightText>
      <AppButton
        style={styles.appButton}
        onPress={() =>
          this.setState({
            isPaymentModalVisible: true,
            onCardPaymentSelection: (mode, bankDirectFunc) =>
              this.createPaySubscription("BASIC", mode, bankDirectFunc)
          })
        }
        title="Subscribe"
      />
    </View>
  );

  renderPremiumPackage = () => (
    <View style={{ ...styles.container, backgroundColor: "#0096c0" }}>
      <LightText style={styles.mediumText}>Premium Package</LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        is
        <LightText style={styles.mediumText}> #8000 </LightText>
        per annum
      </LightText>
      <View
        style={{ ...styles.lineSeperator, ...styles.premiumLineSeperator }}
      />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Validity
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        12 Month
      </LightText>
      <View
        style={{ ...styles.lineSeperator, ...styles.premiumLineSeperator }}
      />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        No. Of Concurrent Care
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Unlimited
      </LightText>
      <View
        style={{ ...styles.lineSeperator, ...styles.premiumLineSeperator }}
      />
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Package Validity
      </LightText>
      <LightText style={{ ...styles.smallText, ...styles.topText }}>
        Full Package Validity: 365 Days
      </LightText>
      <AppButton
        style={{ ...styles.appButton, ...styles.premiunSubscribeBtn }}
        onPress={() =>
          this.setState({
            isPaymentModalVisible: true,
            onCardPaymentSelection: (mode, bankDirectFunc) =>
              this.createPaySubscription("PREMIUM", mode, bankDirectFunc)
          })
        }
        title="Subscribe"
      />
    </View>
  );

  render() {
    let { loading } = this.props,
      { isPaymentModalVisible, onCardPaymentSelection } = this.state;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          {loading && <LoadingView />}
          <PaymentMode
            visible={isPaymentModalVisible}
            onClose={() => this.setState({ isPaymentModalVisible: false })}
            onCardPaymentSelection={onCardPaymentSelection}
          />

          <AppHeader title="Best Price Guaranteed" />
          <PaddingProvider style={styles.paddingContainer}>
            <Content showVerticalScrollIndicator={false}>
              {this.renderBasicPackage()}
              {this.renderPremiumPackage()}
            </Content>
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_Subscribe = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <ReduxContext.Consumer>
        {({ screenProps: { setPaystack } }) => (
          <Mutation operation="createPaymentSubscribe">
            {(mutationState, mutate) => (
              <Subscribe
                {...props}
                mutate={mutate}
                loading={mutationState.loading}
                setPaystack={setPaystack}
                onChangeTab={onChangeTab}
              />
            )}
          </Mutation>
        )}
      </ReduxContext.Consumer>
    )}
  </TabsNavigationContext.Consumer>
));

const styles = {
  paddingContainer: {
    backgroundColor: "#e44746",
    paddingHorizontal: 0
  },
  container: {
    padding: 20
  },
  mediumText: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 21
  },
  topText: {
    alignSelf: "center"
  },
  smallText: {
    color: "#fff",
    fontSize: Fonts.smallText
  },
  lineSeperator: {
    height: 1,
    backgroundColor: "#a32423",
    marginVertical: 10
  },
  premiumLineSeperator: {
    backgroundColor: "#00649f"
  },
  appButton: {
    alignSelf: "center",
    marginTop: 10
  },
  premiunSubscribeBtn: {
    backgroundColor: "#e44746"
  }
};
