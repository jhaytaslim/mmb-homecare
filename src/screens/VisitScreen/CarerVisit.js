import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";

import AppHeader from "../../components/AppHeader";
import NavigatorProvider from "../../containers/NavigationProvider";
import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import { Util } from "../../utils";

export default class CarerVisit extends React.PureComponent {
  render() {
    let {
      navigation: {
        state: {
          params: {
            visit_plan: {
              contract_id: { patient_name, contract_id, care_category },
              type,
              price_per_visit,
              no_of_visits,
              no_of_visits_verified,
              patient_paid
            },
            visit_plan
          }
        },
        navigate
      }
    } = this.props;

    return (
      <NavigatorProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title={contract_id} />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <View style={styles.container}>
              <MediumText style={styles.text}> Patient: </MediumText>
              <MediumText style={styles.text}> {patient_name} </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Type: </MediumText>
              <MediumText style={styles.text}>
                {type || "Initial Visit"}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Treatment Category: </MediumText>
              <MediumText style={styles.text}> {care_category} </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Price Per Visit: </MediumText>
              <MediumText style={styles.text}>
                {Util.formatToNaira(price_per_visit || "")}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Payment Status: </MediumText>
              <MediumText
                style={{
                  ...styles.text,
                  color: patient_paid != 1 ? "red" : "green"
                }}
              >
                {patient_paid != 1 ? `NOT PAID` : `PAID`}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Duration: </MediumText>
              <MediumText style={styles.text}> {no_of_visits} </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Confirmed Visits: </MediumText>
              <MediumText style={styles.text}>
                {no_of_visits_verified}
              </MediumText>
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Total Price: </MediumText>
              <MediumText style={styles.text}>
                {`N${parseInt(no_of_visits) * parseInt(price_per_visit)}`}
              </MediumText>
            </View>
            {no_of_visits != no_of_visits_verified && (
              <AppButton
                title="Create Visit"
                onPress={() =>
                  navigate("carerCreateVisit", { visit: visit_plan })
                }
                style={styles.appButton}
              />
            )}
          </PaddingProvider>
        </React.Fragment>
      </NavigatorProvider>
    );
  }
}

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 25
  },
  text: {
    fontSize: Fonts.smallText,
    color: "#333"
  },
  appButton: {
    marginTop: 30,
    alignSelf: "center"
  }
};
