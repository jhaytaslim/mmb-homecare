import React from "react";
import { Item, Input, Icon, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";
import {
  View,
  DatePickerAndroid,
  Platform,
  DatePickerIOS,
  Alert
} from "react-native";
import moment from "moment";

import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import NavigationProvider from "../../containers/NavigationProvider";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import Colors from "../../assets/Colors";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import ToastStyle from "../../assets/ToastStyle";
import LoadingView from "../../components/LoadingView";
import { Util } from "../../utils";

class CarerCreateVisit extends React.PureComponent {
  state = {
    next_visit:
      Platform.OS == "android"
        ? `${moment().format("YYYY")}-${moment().format(
            "MM"
          )}-${moment().format("DD")}`
        : new Date()
  };

  static propTypes = {
    loading: PropTypes.bool,
    createVisit: PropTypes.func
  };

  setDate = () => {
    if (Platform.OS == "android") {
      DatePickerAndroid.open({
        date: new Date(this.state.next_visit)
      })
        .then(({ action, year, month, day }) => {
          let _month =
              (month + 1).toString().length == 1 ? `0${month + 1}` : month + 1,
            _day = day.toString().length == 1 ? `0${day}` : day;
          if (action !== DatePickerAndroid.dismissedAction) {
            this.setState({
              next_visit: `${year}-${_month}-${_day}`
            });
          }
        })
        .catch(({ code, message }) => {
          //@Todo send to app center
        });
    }
  };

  createVisit = () => {
    let {
        createVisit,
        navigation: {
          state: {
            params: {
              visit: {
                contract_id: { _id }
              },
              visit
            }
          }
        }
      } = this.props,
      { next_visit } = this.state;

    createVisit({
      data: {
        contract_id: _id,
        visit_plan: visit._id,
        next_visit: this.state.next_visit
      }
    })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            type: "success",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  render() {
    let {
      navigation: {
        state: {
          params: {
            visit: {
              contract_id: { patient_name }
            }
          }
        }
      }
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {this.props.loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Create Visit" />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <Item style={styles.item} rounded>
              <Icon name="ios-person" style={styles.icon} />
              <Input
                editable={false}
                value={patient_name}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Patient"
              />
            </Item>
            <MediumText style={{ marginTop: 10 }}> Visit Date </MediumText>
            {Platform.OS == "android" ? (
              <View style={styles.dateContainer}>
                <MediumText style={styles.dateText}>
                  {this.state.next_visit}
                </MediumText>
                <AppButton
                  title="Change"
                  onPress={this.setDate}
                  style={styles.datePicker}
                />
              </View>
            ) : (
              <DatePickerIOS
                date={this.state.next_visit}
                onDateChange={next_visit => this.setState({ next_visit })}
              />
            )}
            <AppButton
              title="Create"
              onPress={this.createVisit}
              style={styles.appButton}
            />
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CarerCreateVisit = props => {
  let {
    navigation: {
      state: {
        params: {
          visit: {
            contract_id: { _id }
          }
        }
      }
    }
  } = props;

  return (
    <Mutation
      operation="createVisitInVisitPlan"
      options={{
        refetchQueries: [
          {
            operation: "getNextVisit",
            config: { params: { contract_id: _id } }
          }
        ]
      }}
    >
      {(mutationState, mutate) => (
        <CarerCreateVisit
          loading={mutationState.loading}
          createVisit={mutate}
          {...props}
        />
      )}
    </Mutation>
  );
});

const styles = {
  item: {
    marginBottom: 10,
    backgroundColor: "#fff"
  },
  icon: {
    color: Colors.roundedInputIconColor
  },
  appButton: {
    alignSelf: "center",
    marginTop: 20
  },
  datePicker: {
    paddingHorizontal: 10,
    height: 40
  },
  dateText: {
    fontSize: 16,
    color: "#333"
  },
  dateContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
};
