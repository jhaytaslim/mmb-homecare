import React from "react";
import { View, Alert, Platform } from "react-native";
import PropTypes from "prop-types";
import moment from "moment";
import { Mutation, Query } from "react-kunyora";
import { Toast } from "native-base";

import AppHeader from "../../components/AppHeader";
import NavigatorProvider from "../../containers/NavigationProvider";
import { MediumText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import { ReduxContext } from "../../context/ReduxContext";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import PaymentMode from "../../components/PaymentMode";

class PatientVisit extends React.PureComponent {
  constructor(props) {
    super(props);
    let {
      navigation: {
        state: {
          params: {
            visit_plan: { patient_paid }
          }
        }
      }
    } = props;

    this.state = {
      patient_paid,
      isPaymentModalVisible: false,
      onCardPaymentSelection: () => null
    };
  }

  static propTypes = {
    loading: PropTypes.bool,
    visits: PropTypes.any,
    isVisitPlanVerificationLoading: PropTypes.bool,
    verifyVisitPlan: PropTypes.func
  };

  static getDerivedStateFromProps(props, state) {
    let { visitPlan } = props;
    if (
      visitPlan &&
      visitPlan.visit_plan &&
      visitPlan.visit_plan.patient_paid
    ) {
      return { patient_paid: visitPlan.visit_plan.patient_paid };
    } else {
      return null;
    }
  }

  verifyVisitPlan = (mode, bankDirectFunc) => {
    let {
      verifyVisitPlan,
      navigation: {
        navigate,
        goBack,
        state: {
          params: {
            visit_plan: { _id }
          }
        }
      },
      setPaystack
    } = this.props;

    this.setState({ isPaymentModalVisible: false });

    verifyVisitPlan({ data: { id: _id, mode } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (bankDirectFunc && code != 106) {
          bankDirectFunc(result);
        } else {
          this.setState({ isPaymentModalVisible: false }, () => {
            if (code == 100) {
              setPaystack({ ...entity });
              navigate("paystack", {
                operation: "createPaymentValidate",
                refetchQueries: [
                  {
                    operation: "getSingleVisitPlan",
                    config: { params: { plan_id: _id } }
                  }
                ],
                onSuccess: () => goBack()
              });
            } else if (code == 106) {
              Util.logout(this.props.navigation);
            } else {
              Platform.OS !== "ios"
                ? Alert.alert(
                    "Error Message",
                    desc,
                    [{ text: "Okay", onPress: () => null }],
                    { cancelable: false }
                  )
                : Toast.show({
                    text: desc,
                    textStyle: ToastStyle.toast,
                    type: "danger",
                    buttonText: "okay",
                    duration: 30000,
                    buttonStyle: ToastStyle.buttonStyle,
                    buttonTextStyle: ToastStyle.buttonErrorTextStyle
                  });
            }
          });
        }
      })
      .catch(err => {
        this.setState({ isPaymentModalVisible: false });
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        });
      });
  };

  render() {
    let {
        navigation: {
          state: {
            params: {
              visit_plan: {
                contract_id: { contract_id },
                price_per_visit,
                no_of_visits,
                _id
              }
            }
          }
        },
        visits,
        isVisitPlanVerificationLoading
      } = this.props,
      {
        patient_paid,
        isPaymentModalVisible,
        onCardPaymentSelection
      } = this.state,
      _visits = visits || {},
      _visitsDetails = _visits.visits || [];

    return (
      <NavigatorProvider navigation={this.props.navigation}>
        {isVisitPlanVerificationLoading && <LoadingView />}
        <React.Fragment>
          <PaymentMode
            visible={isPaymentModalVisible}
            onClose={() => this.setState({ isPaymentModalVisible: false })}
            onCardPaymentSelection={onCardPaymentSelection}
          />
          <AppHeader title={contract_id} />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <View style={styles.container}>
              <MediumText style={styles.text}> Amount Payable: </MediumText>
              <MediumText style={styles.text}>
                {Util.formatToNaira(
                  parseInt(no_of_visits) * parseInt(price_per_visit)
                )}
              </MediumText>
            </View>
            <View style={styles.container}>
              <View style={styles.row}>
                <MediumText style={styles.text}> Payment status: </MediumText>
              </View>
              {patient_paid !== 1 ? (
                <View style={styles.row}>
                  <MediumText
                    style={{
                      ...styles.text,
                      color: "red",
                      marginRight: 10
                    }}
                  >
                    NOT PAID
                  </MediumText>
                  <AppButton
                    style={styles.appButton}
                    title="Make Payment"
                    onPress={() =>
                      this.setState({
                        isPaymentModalVisible: true,
                        onCardPaymentSelection: (mode, bankDirectFunc) =>
                          this.verifyVisitPlan(mode, bankDirectFunc)
                      })
                    }
                  />
                </View>
              ) : (
                <MediumText
                  style={{
                    ...styles.text,
                    color: "green"
                  }}
                >
                  PAID
                </MediumText>
              )}
            </View>
            <View style={styles.container}>
              <MediumText style={styles.text}> Duration of visits: </MediumText>
              <MediumText style={styles.text}>{no_of_visits}</MediumText>
            </View>
            <View style={{ marginTop: 20 }}>
              {_visitsDetails.map((visits, i) => (
                <View style={styles.container} key={i}>
                  <MediumText style={styles.text}>
                    {`Visit: ${i + 1}`}
                  </MediumText>
                  <MediumText style={styles.text}>
                    {moment(visits.date_of_visit).format("Do MMM, YYYY")}
                  </MediumText>
                </View>
              ))}
            </View>
          </PaddingProvider>
        </React.Fragment>
      </NavigatorProvider>
    );
  }
}

export default (_PatientVisit = props => {
  let {
    navigation: {
      state: {
        params: {
          visit_plan: { _id }
        }
      }
    }
  } = props;

  return (
    <ReduxContext.Consumer>
      {({ screenProps: { setPaystack } }) => (
        <Mutation operation="createVisitPlanPayment">
          {(mutationState, mutate) => (
            <Query
              operation="getVisitInVisitPlan"
              options={{ config: { params: { visit_plan: _id } } }}
            >
              {({ data, loading, error }) => (
                <Query
                  operation="getSingleVisitPlan"
                  options={{
                    fetchPolicy: "network-only",
                    config: { params: { plan_id: _id } }
                  }}
                >
                  {(visitPlan, fetchMore, refetchQuery) => (
                    <PatientVisit
                      loading={loading}
                      visits={data && data.entity}
                      isVisitPlanVerificationLoading={mutationState.loading}
                      verifyVisitPlan={mutate}
                      visitPlan={visitPlan.data && visitPlan.data.entity}
                      setPaystack={setPaystack}
                      {...props}
                    />
                  )}
                </Query>
              )}
            </Query>
          )}
        </Mutation>
      )}
    </ReduxContext.Consumer>
  );
});

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 25
  },
  text: {
    fontSize: Fonts.smallText,
    color: "#333"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  appButton: {
    paddingVertical: 0,
    paddingHorizontal: 10,
    height: 30,
    borderRadius: 5
  }
};
