import React from "react";
import { List } from "native-base";
import { StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Query } from "react-kunyora";
import { connect } from "react-redux";
import moment from "moment";

import AppHeader from "../../components/AppHeader";
import { FlatListItem } from "../../components/ListItem";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import AppList from "../../components/AppList";

class ViewPlansList extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    visitPlans: PropTypes.any,
    loading: PropTypes.bool,
    error: PropTypes.any,
    accountType: PropTypes.string
  };

  render() {
    let {
      navigation: { navigate },
      loading,
      visitPlans,
      accountType,
      error,
      isInitialDataSet,
      refetchQuery
    } = this.props;

    let _visitPlans = (visitPlans && visitPlans.visit_plans) || undefined;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppHeader title="Visit Plans" />
          <AppList
            loading={loading}
            error={error}
            dataArray={_visitPlans}
            fetchMore={() => null}
            refetchQuery={() => refetchQuery()}
            isFetchingMore={loading && isInitialDataSet}
            timeElapseToMountIndicator={1000}
            containerType="plain"
            renderRow={list => {
              let {
                contract_id: {
                  contract_id,
                  patient_name,
                  care_category,
                  patient_id
                },
                carer_name,
                no_of_visits,
                no_of_visits_verified,
                created
              } = list;

              let completionState =
                no_of_visits == no_of_visits_verified
                  ? "Completed"
                  : "Not Completed";

              return (
                <FlatListItem
                  type="icon"
                  iconName="ios-person"
                  title={contract_id}
                  description={
                    accountType == "carer"
                      ? patient_name
                      : `${no_of_visits} Visits`
                  }
                  tag={
                    accountType == "carer"
                      ? `${care_category} (${completionState})`
                      : completionState
                  }
                  tagStyle={
                    completionState != "Completed"
                      ? styles.awaiting
                      : styles.confirmed
                  }
                  date={moment(created).format("MMM Do, YYYY")}
                  onPress={() =>
                    navigate(
                      accountType == "carer" ? "carerVisit" : "patientVisit",
                      { visit_plan: list }
                    )
                  }
                />
              );
            }}
          />
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const styles = StyleSheet.create({
  awaiting: {
    color: "orange"
  },
  confirmed: {
    color: "green"
  }
});

const _ViewPlansList = props => {
  let {
    navigation: {
      state: {
        params: { contract_id }
      }
    }
  } = props;

  return (
    <Query
      operation="getVisitPlan"
      options={{
        fetchPolicy: "network-only",
        config: { params: { contract_id } }
      }}
    >
      {(
        { data, loading, error, isInitialDataSet },
        fetchMore,
        refetchQuery
      ) => {
        return (
          <ViewPlansList
            visitPlans={data && data.entity}
            loading={loading}
            error={error}
            refetchQuery={refetchQuery}
            isInitialDataSet={isInitialDataSet}
            {...props}
          />
        );
      }}
    </Query>
  );
};

function mapStateToProps(state) {
  return {
    accountType: state.accountType
  };
}

export default connect(mapStateToProps)(_ViewPlansList);
