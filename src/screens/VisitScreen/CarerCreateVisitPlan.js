import React from "react";
import { Item, Input, Icon, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import AppHeader from "../../components/AppHeader";
import NavigationProvider from "../../containers/NavigationProvider";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import Colors from "../../assets/Colors";
import ToastStyle from "../../assets/ToastStyle";
import LoadingView from "../../components/LoadingView";
import { Util } from "../../utils";

class CarerCreateVisitPlan extends React.PureComponent {
  state = {
    duration: "",
    price: "",
    total_price: ""
  };

  static propTypes = {
    loading: PropTypes.bool,
    createVisitPlan: PropTypes.func,
    contract_id: PropTypes.string
  };

  createVisitPlan = () => {
    let {
      createVisitPlan,
      navigation: {
        state: {
          params: { contract_id }
        },
        goBack
      }
    } = this.props;

    if (!Number(this.state.price) || Number(this.state.price) <= 0) {
      alert("The price has to be above #0");
      return;
    }

    if (!Number(this.state.duration) || Number(this.state.duration) <= 0) {
      alert("The duration cannot be 0 or less than 1");
      return;
    }

    createVisitPlan({ data: { contract_id, ...this.state } })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            type: "success",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonSuccessTextStyle
          });
          goBack();
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Toast.show({
            text: `${desc}`,
            textStyle: ToastStyle.toast,
            type: "danger",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle
          });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  render() {
    let {
      navigation: {
        state: {
          params: { patient_name }
        }
      },
      loading
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <React.Fragment>
          <AppHeader title="Create Visit Plan" />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <Item style={styles.item} rounded>
              <Icon name="ios-person" style={styles.icon} />
              <Input
                value={patient_name}
                editable={false}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Patient"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.item} rounded>
              <Icon name="md-watch" style={styles.icon} />
              <Input
                keyboardType="numeric"
                value={this.state.duration}
                onChangeText={duration => this.setState({ duration })}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="plan Duration (No. of visits)"
                style={styles.textInput}
              />
              <Icon name="md-arrow-dropdown" style={styles.icon} />
            </Item>
            <Item style={styles.item} rounded>
              <Icon name="md-cash" style={styles.icon} />
              <Input
                value={this.state.price}
                keyboardType="numeric"
                onChangeText={price => this.setState({ price })}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Price per visit(N)"
                style={styles.textInput}
              />
            </Item>
            <Item style={styles.item} rounded>
              <Icon name="md-cart" style={styles.icon} />
              <Input
                editable={false}
                value={(
                  Number(this.state.duration) * Number(this.state.price)
                ).toString()}
                placeholderTextColor={Colors.roundedInputPlaceholderColor}
                placeholder="Total Charge for visit Duration"
                style={styles.textInput}
              />
            </Item>
            <AppButton
              title="Create"
              onPress={this.createVisitPlan}
              style={styles.appButton}
            />
          </PaddingProvider>
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

export default (_CarerCreateVisitPlan = props => {
  let {
    createVisitPlan,
    navigation: {
      state: {
        params: { contract_id }
      }
    }
  } = props;

  return (
    <Mutation
      operation="createVisitPlan"
      options={{
        refetchQueries: [
          { operation: "getVisitPlan", config: { params: { contract_id } } }
        ]
      }}
    >
      {(mutationState, mutate) => (
        <CarerCreateVisitPlan
          loading={mutationState.loading}
          createVisitPlan={mutate}
          {...props}
        />
      )}
    </Mutation>
  );
});

const styles = {
  item: {
    marginBottom: 10,
    backgroundColor: "#fff"
  },
  icon: {
    color: Colors.roundedInputIconColor
  },
  appButton: {
    alignSelf: "center",
    marginTop: 20
  },
  textInput: {
    fontFamily: "Quicksand-Medium"
  }
};
