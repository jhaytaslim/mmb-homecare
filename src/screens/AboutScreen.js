import React from "react";
import { View, StyleSheet, Linking } from "react-native";

import AppHeader from "../components/AppHeader";
import PaddingProvider from "../containers/PaddingProvider";
import NavigationProvider from "../containers/NavigationProvider";
import { MediumText, BoldText } from "../components/AppText";

export default class AboutScreen extends React.PureComponent {
  openLink = type => {
    let url =
      type == "terms"
        ? "https://www.mmbhomecare.com/policy/terms"
        : "https://www.mmbhomecare.com/policy/privacy";

    Linking.openURL(url).catch(err => alert(`An error occurred: ${err}`));
  };

  render() {
    return (
      <NavigationProvider navigation={this.props.navigation}>
        <View style={styles.container}>
          <AppHeader title="About MMB Homecare" />
          <PaddingProvider type="plain" style={{ paddingTop: 10 }}>
            <BoldText style={styles.title}>
              MMB Homecare (version 0.34)
            </BoldText>
            <MediumText style={styles.subTitle}>
              MMB Homecare is a digital platform that enables patients and their
              relatives to find and book registered doctors, nurses and
              physiotherapists who are available to provide healthcare services
              in the patients' home or workplace for an agreed fee securely and
              confidentially at the click of a button.
            </MediumText>
            <MediumText style={styles.subTitle}>
              Check out our{" "}
              <MediumText
                style={styles.link}
                onPress={() => this.openLink("terms")}
              >
                Terms and conditions
              </MediumText>{" "}
              and our{" "}
              <MediumText
                style={styles.link}
                onPress={() => this.openLink("privacy")}
              >
                Privacy
              </MediumText>{" "}
              to fully understand how we make use of your data and and how
              confidential it is to us.
            </MediumText>
          </PaddingProvider>
        </View>
      </NavigationProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    color: "#333",
    fontSize: 20
  },
  subTitle: {
    color: "#333",
    fontSize: 16,
    marginTop: 15
  },
  link: {
    color: "#00b0cf",
    textDecorationLine: "underline"
  }
});
