import React from "react";
import { View, Alert, Platform } from "react-native";
import { Item, Content, Input, Radio, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class SettingsScreen extends React.PureComponent {
  static propTypes = {
    updatePassword: PropTypes.func
  };

  state = {
    current_password: "",
    new_password: "",
    cnew_password: ""
  };

  updatePassword = () => {
    let { current_password, cnew_password, new_password } = this.state;
    if (
      current_password.trim().length == 0 ||
      cnew_password.trim().length == 0 ||
      new_password.trim().length == 0
    ) {
      Alert.alert(
        "Error Message",
        `You have an empty field`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    } else if (cnew_password.trim() != new_password.trim()) {
      Alert.alert(
        "Error Message",
        `Error: New and confirmation passwords are different`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
    } else {
      let data = { current_password, new_password, cnew_password };
      this.props
        .updatePassword({ data })
        .then(result => {
          let {
            status: { code, desc }
          } = result;

          if (code == 100) {
            Toast.show({
              text: `${desc}`,
              textStyle: ToastStyle.toast,
              type: "success",
              buttonText: "okay",
              duration: 10000,
              buttonStyle: ToastStyle.buttonStyle,
              buttonTextStyle: ToastStyle.buttonSuccessTextStyle
            });
          } else if (code == 106) {
            Util.logout(this.props.navigation);
          } else {
            Platform.OS !== "ios"
              ? Alert.alert(
                  "Error Message",
                  desc,
                  [{ text: "Okay", onPress: () => null }],
                  { cancelable: false }
                )
              : Toast.show({
                  text: desc,
                  textStyle: ToastStyle.toast,
                  type: "danger",
                  buttonText: "okay",
                  duration: 30000,
                  buttonStyle: ToastStyle.buttonStyle,
                  buttonTextStyle: ToastStyle.buttonErrorTextStyle
                });
          }
        })
        .catch(err =>
          Toast.show({
            text: `${err}`,
            textStyle: ToastStyle.toast,
            type: "danger",
            buttonText: "okay",
            duration: 10000,
            buttonStyle: ToastStyle.buttonStyle,
            buttonTextStyle: ToastStyle.buttonErrorTextStyle
          })
        );
    }
  };

  render() {
    let { current_password, new_password, cnew_password } = this.state,
      { loading } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <View style={styles.container}>
          <AppHeader title="Settings" />
          <PaddingProvider style={{ backgroundColor: "#0096c0" }}>
            <MediumText style={styles.settingsHeaderText}>
              Change Password
            </MediumText>
            <Item>
              <Input
                value={current_password}
                onChangeText={current_password =>
                  this.setState({ current_password })
                }
                placeholder="Current Password"
                secureTextEntry={true}
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item>
              <Input
                value={new_password}
                secureTextEntry={true}
                onChangeText={new_password => this.setState({ new_password })}
                placeholder="New Password"
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item>
              <Input
                value={cnew_password}
                secureTextEntry={true}
                onChangeText={cnew_password => this.setState({ cnew_password })}
                placeholder="Confirm New Password"
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <AppButton
              title="Change"
              onPress={this.updatePassword}
              style={styles.button}
            />
          </PaddingProvider>
        </View>
      </NavigationProvider>
    );
  }
}

const _SettingsScreen = props => (
  <Mutation operation="updatePasswordUpdate">
    {(mutationState, mutate) => (
      <SettingsScreen
        loading={mutationState.loading}
        updatePassword={mutate}
        {...props}
      />
    )}
  </Mutation>
);

export default _SettingsScreen;

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#0096c0"
  },
  settingsHeaderText: {
    marginTop: 10,
    color: "#fff",
    alignSelf: "center",
    fontSize: 20
  },
  textInput: {
    fontFamily: "Quicksand-Medium",
    fontSize: 15,
    color: "#fff"
  },
  button: {
    alignSelf: "center",
    marginTop: 10
  },
  listItem: {
    flexDirection: "row",
    marginTop: 10
  },
  radioLabels: {
    marginLeft: 10,
    fontSize: 13,
    color: "#fff",
    alignSelf: "center"
  }
};
