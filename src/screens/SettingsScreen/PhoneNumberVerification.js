import React from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  Alert,
  Platform
} from "react-native";
import { Input, Content, Item, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import { MediumText, BoldText } from "../../components/AppText";
import Fonts from "../../assets/Fonts";
import Colors from "../../assets/Colors";
import Icon from "../../components/Icon";
import AppButton from "../../components/AppButton";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";
import TabsNavigationContext from "../../context/TabsNavigationContext";

class PhoneNumberVerification extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    confirmMobileNumber: PropTypes.func
  };

  state = {
    isVerified: false,
    accessCode: ""
  };

  verifyOtp = () => {
    if (this.state.accessCode.trim().length == 0) {
      Alert.alert(
        "Error Message",
        `Access Code field cannot be empty`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    }

    this.props
      .confirmMobileNumber({ data: { code: this.state.accessCode.trim() } })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          this.setState({ isVerified: true }, () =>
            setTimeout(() => {
              this.props.onChangeTab(0);
              this.props.navigation.navigate("tabRoutes");
            }, 2000)
          );
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `${err}`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  renderSuccessModal = () => {
    return (
      <View style={styles.container}>
        <Icon
          name="md-checkmark-circle-outline"
          type="ionic-icon"
          style={styles.icon}
        />
        <MediumText style={styles.title}>
          MOBILE NUMBER CHANGED SUCCESSFUL
        </MediumText>
      </View>
    );
  };

  renderVerificationInputContainer = () => (
    <React.Fragment>
      <View style={styles.container}>
        <BoldText style={styles.title}>MOBILE VERIFICATION</BoldText>
        <MediumText>
          Please enter the 4 digit verification code sent to your phone number
        </MediumText>
        <Item style={styles.inputBox}>
          <Input
            value={this.state.accessCode}
            keyboardType="numeric"
            onChangeText={value => this.setState({ accessCode: value })}
            placeholder="Input 4 digits code"
            style={{ fontFamily: "Quicksand-Medium" }}
          />
        </Item>
        <View style={styles.appButtonContainer}>
          <AppButton
            title="Verify"
            style={styles.verificationButton}
            onPress={this.verifyOtp}
          />
          <AppButton
            title="Back"
            style={styles.verificationButton}
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </View>
      <MediumText style={styles.bottomText} onPress={this.verifyOtp}>
        Resend Verification Number
      </MediumText>
    </React.Fragment>
  );

  render() {
    let { loading } = this.props;
    return (
      <ImageBackground
        source={require("../../assets/Images/authImage.png")}
        style={styles.mainContainer}
      >
        <React.Fragment>
          {loading && <LoadingView />}
          {this.state.isVerified
            ? this.renderSuccessModal()
            : this.renderVerificationInputContainer()}
        </React.Fragment>
      </ImageBackground>
    );
  }
}

const _PhoneNumberVerification = props => (
  <TabsNavigationContext.Consumer>
    {({ onChangeTab }) => (
      <Mutation operation="createMobileNumberConfirmation">
        {(mutationState, mutate) => (
          <PhoneNumberVerification
            {...props}
            loading={mutationState.loading}
            confirmMobileNumber={mutate}
            onChangeTab={onChangeTab}
          />
        )}
      </Mutation>
    )}
  </TabsNavigationContext.Consumer>
);

export default _PhoneNumberVerification;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    marginHorizontal: 10,
    backgroundColor: "#fff",
    borderRadius: 15,
    alignItems: "center",
    padding: 20
  },
  title: {
    fontSize: Fonts.signUp.verificationTitle,
    color: Colors.signUp.verificationTitle,
    marginVertical: 10,
    textAlign: "center"
  },
  inputBox: {
    borderBottomColor: "#006ba5"
  },
  icon: {
    fontSize: 50,
    color: "green"
  },
  bottomText: {
    color: "#fff"
  },
  appButtonContainer: {
    flexDirection: "row",
    alignSelf: "center"
  },
  verificationButton: {
    backgroundColor: "red",
    marginTop: 10,
    marginHorizontal: 10
  }
});
