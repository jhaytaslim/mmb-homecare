import React from "react";
import { Container } from "native-base";
import { StyleSheet, View } from "react-native";

import { MediumText } from "../../components/AppText";
import Colors from "../../assets/Colors";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import AppHeader from "../../components/AppHeader";
import NavigationProvider from "../../containers/NavigationProvider";

export default class SettingsScreen extends React.PureComponent {
  render() {
    let { navigation } = this.props;

    return (
      <NavigationProvider navigation={navigation}>
        <AppHeader
          title="Settings"
          onPressBackButton={() => navigation.navigate("tabRoutes")}
        />
        <Container style={styles.container}>
          <NativeTouchableFeedback
            onPress={() => navigation.navigate("passwordSettings")}
          >
            <View style={styles.section}>
              <MediumText style={styles.text}>Change Password</MediumText>
            </View>
          </NativeTouchableFeedback>
          <NativeTouchableFeedback
            onPress={() => navigation.navigate("phoneNumberSettings")}
          >
            <View style={styles.section}>
              <MediumText style={styles.text}>Change Phone Number</MediumText>
            </View>
          </NativeTouchableFeedback>
        </Container>
      </NavigationProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
    flex: 1
  },
  section: {
    padding: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#bdbdbd"
  },
  text: {
    color: "#666",
    fontSize: 16
  }
});
