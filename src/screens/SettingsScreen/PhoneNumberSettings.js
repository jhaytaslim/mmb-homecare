import React from "react";
import { View, Alert, Platform } from "react-native";
import { Item, Content, Input, Radio, Toast } from "native-base";
import { Mutation } from "react-kunyora";
import PropTypes from "prop-types";

import { MediumText } from "../../components/AppText";
import AppHeader from "../../components/AppHeader";
import PaddingProvider from "../../containers/PaddingProvider";
import AppButton from "../../components/AppButton";
import NavigationProvider from "../../containers/NavigationProvider";
import LoadingView from "../../components/LoadingView";
import ToastStyle from "../../assets/ToastStyle";
import { Util } from "../../utils";

class PhoneNumberSettings extends React.PureComponent {
  static propTypes = {
    updatePhoneNumber: PropTypes.func,
    loading: PropTypes.bool
  };

  state = {
    phoneNumber: "",
    password: ""
  };

  updatePhoneNumber = () => {
    let { password, phoneNumber } = this.state;

    if (phoneNumber.trim().length <= 7) {
      Alert.alert(
        "Error Message",
        `You have an entered a wrong phone number.Phone number must be greater than 7`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    }

    if (password.trim().length == 0) {
      Alert.alert(
        "Error Message",
        `Password field cannot be empty`,
        [{ text: "Okay", onPress: () => null }],
        { cancelable: false }
      );
      return;
    }

    this.props
      .updatePhoneNumber({
        data: {
          new_mobile: phoneNumber,
          password: password
        }
      })
      .then(result => {
        let {
          status: { code, desc }
        } = result;

        if (code == 100) {
          this.props.navigation.navigate("phoneNumberVer");
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Platform.OS !== "ios"
            ? Alert.alert(
                "Error Message",
                desc,
                [{ text: "Okay", onPress: () => null }],
                { cancelable: false }
              )
            : Toast.show({
                text: desc,
                textStyle: ToastStyle.toast,
                type: "danger",
                buttonText: "okay",
                duration: 30000,
                buttonStyle: ToastStyle.buttonStyle,
                buttonTextStyle: ToastStyle.buttonErrorTextStyle
              });
        }
      })
      .catch(err =>
        Toast.show({
          text: `Network error`,
          textStyle: ToastStyle.toast,
          type: "danger",
          buttonText: "okay",
          duration: 10000,
          buttonStyle: ToastStyle.buttonStyle,
          buttonTextStyle: ToastStyle.buttonErrorTextStyle
        })
      );
  };

  render() {
    let { phoneNumber, password } = this.state,
      { loading } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        {loading && <LoadingView />}
        <View style={styles.container}>
          <AppHeader title="Settings" />
          <PaddingProvider style={{ backgroundColor: "#0096c0" }}>
            <MediumText style={styles.settingsHeaderText}>
              Change Phone Number
            </MediumText>
            <Item>
              <Input
                value={phoneNumber}
                keyboardType="numeric"
                onChangeText={phoneNumber => this.setState({ phoneNumber })}
                placeholder="New Phone Number"
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <Item>
              <Input
                value={password}
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
                placeholder="Enter your Password"
                placeholderTextColor="#fff"
                style={styles.textInput}
              />
            </Item>
            <AppButton
              title="Change"
              onPress={this.updatePhoneNumber}
              style={styles.button}
            />
          </PaddingProvider>
        </View>
      </NavigationProvider>
    );
  }
}

const _PhoneNumberSettings = props => (
  <Mutation operation="updateMobileNumber">
    {(mutationState, mutate) => (
      <PhoneNumberSettings
        {...props}
        updatePhoneNumber={mutate}
        loading={mutationState.loading}
      />
    )}
  </Mutation>
);

export default _PhoneNumberSettings;

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#0096c0"
  },
  settingsHeaderText: {
    marginTop: 10,
    color: "#fff",
    alignSelf: "center",
    fontSize: 20
  },
  textInput: {
    fontFamily: "Quicksand-Medium",
    fontSize: 15,
    color: "#fff"
  },
  button: {
    alignSelf: "center",
    marginTop: 10
  },
  listItem: {
    flexDirection: "row",
    marginTop: 10
  },
  radioLabels: {
    marginLeft: 10,
    fontSize: 13,
    color: "#fff",
    alignSelf: "center"
  }
};
