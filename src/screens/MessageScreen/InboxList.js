import React from "react";
import { View, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Query } from "react-kunyora";
import { connect } from "react-redux";

import NavigationProvider from "../../containers/NavigationProvider";
import { RoundedListItem } from "../../components/ListItem";
import AppList from "../../components/AppList";
import { ReduxContext } from "../../context/ReduxContext";

class InboxList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.props.setMessageCountObject(this.composeConvoIdObj());
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    conversations: PropTypes.any,
    loading: PropTypes.bool,
    error: PropTypes.any,
    unreadMessages: PropTypes.arrayOf(PropTypes.object)
  };

  componentDidUpdate(prevProps) {
    let { unreadMessages: prevUnreadMessages } = prevProps,
      { unreadMessages: newUnreadMessages } = this.props;

    if (newUnreadMessages && newUnreadMessages != prevUnreadMessages) {
      this.props.setMessageCountObject(this.composeConvoIdObj());
    }
  }

  composeConvoIdObj = () => {
    let convoIdList = this.props.unreadMessages.reduce((acc, value) => {
      acc[value.convo_id] = (acc[value.convo_id] || 0) + 1;
      return acc;
    }, {});

    return convoIdList;
  };

  render() {
    let {
      navigation: { navigate },
      conversations,
      loading,
      isInitialDataSet,
      error,
      refetchQuery
    } = this.props;

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <React.Fragment>
          <AppList
            loading={loading}
            dataArray={conversations && conversations.convos}
            timeElapseToMountIndicator={1000}
            refetchQuery={() => refetchQuery()}
            error={error}
            storageKey="@conversations"
            reduxActionID="setConversations"
            reduxStateID="conversations"
            fetchMore={() => null}
            isFetchingMore={loading && isInitialDataSet}
            containerStyle={{ padding: 10 }}
            renderRow={list => {
              return (
                <View style={{ marginTop: 10, marginHorizontal: 15 }}>
                  <RoundedListItem
                    isVerified={true}
                    showUnreadMessages
                    messageId={list.convo_id}
                    photo={list.img}
                    name={list.full_name}
                    lastChat={list.last_message.message}
                    date={list.last_message.created}
                    onPress={() => navigate("chats", { convo_info: list })}
                  />
                </View>
              );
            }}
          />
        </React.Fragment>
      </NavigationProvider>
    );
  }
}

const _InboxList = props => (
  <ReduxContext.Consumer>
    {({ screenProps: { setMessageCountObject } }) => (
      <Query
        operation="getConversations"
        options={{ fetchPolicy: "network-only" }}
      >
        {(
          { data, loading, error, isInitialDataSet },
          fetchMore,
          refetchQuery
        ) => {
          return (
            <InboxList
              conversations={data && data.entity}
              loading={loading}
              error={error}
              fetchMore={fetchMore}
              setMessageCountObject={setMessageCountObject}
              refetchQuery={refetchQuery}
              isInitialDataSet={isInitialDataSet}
              {...props}
            />
          );
        }}
      </Query>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state) {
  return {
    unreadMessages: state.unreadMessages
  };
}

export default connect(mapStateToProps)(_InboxList);

const styles = StyleSheet.create({
  marginTopSm: {
    marginTop: 50
  },
  marginTopXs: {
    marginTop: 20
  },
  elevate: {
    elevation: 2
  },
  searchInputStyles: {
    paddingHorizontal: 15,
    backgroundColor: "#fff",
    elevation: 5
  },
  flRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  dashboardIconCon: {
    paddingHorizontal: 15,
    marginTop: 20
  },
  btnRed: {
    backgroundColor: "#ff4c49"
  },
  btnBlue: {
    backgroundColor: "#00d9f1"
  },
  btnLightBlue: {
    backgroundColor: "#cbebf2"
  },
  customBtnStyle: {
    paddingHorizontal: 10,
    height: 70,
    marginBottom: 5,
    borderRadius: 10
  },
  btnIconLarge: {
    fontSize: 34
  },
  text: {
    fontSize: 12
  }
});
