import React from "react";
import { View, StyleSheet, AsyncStorage, Linking } from "react-native";
import PropTypes from "prop-types";
import moment from "moment";
import { Mutation } from "react-kunyora";
import EventSource from "react-native-event-source";
import { connect } from "react-redux";
import { GiftedChat, Bubble } from "react-native-gifted-chat";
import { Toast } from "native-base";

import NavigationProvider from "../../containers/NavigationProvider";
import AppHeader from "../../components/AppHeader";
import { Util } from "../../utils";
import MessagingErrorBoundary from "../../containers/MessagingErrorBoundary";
import Icon from "../../components/Icon";
import ToastStyle from "../../assets/ToastStyle";
import { ReduxContext } from "../../context/ReduxContext";

class Chats extends React.PureComponent {
  state = {
    previousMessages: [],
    fetchMoreFromLocalStorage: true,
    numFetchedMessages: 0,
    isLoadingFromAsync: true,
    isLoadingEarlier: false
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loadingPreviousMessages: PropTypes.bool,
    sendingMessage: PropTypes.bool,
    sendMessage: PropTypes.func,
    fetchPreviousMessages: PropTypes.func,
    loadingUnread: PropTypes.bool,
    fetchUnreadMessages: PropTypes.func,
    currentUser: PropTypes.object,
    setCurrentPageData: PropTypes.func,
    messageFromNotifications: PropTypes.array,
    setMessageForCurrentViewedMessagePage: PropTypes.func
  };

  componentDidMount() {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      setCurrentPageData
    } = this.props;

    setCurrentPageData("message", convo_id);

    AsyncStorage.getItem("@chats")
      .then(chats => {
        if (chats) {
          let parsedChats = JSON.parse(chats)[convo_id],
            _thisChats = (parsedChats && parsedChats.chats) || {};

          let _storedMessages = Object.values(_thisChats).slice(
            this.state.numFetchedMessages,
            20
          );

          if (_thisChats) {
            this.setState(prevState => {
              return {
                previousMessages: GiftedChat.append(
                  prevState.previousMessages,
                  _storedMessages
                ),
                numFetchedMessages: _storedMessages.length
              };
            });
          }
        }
      })
      .catch(err => null);

    this.deleteUnreadMessagesFromNotificationStore(convo_id);
    this.fetchPrevMessages();
    this.connectedToEventStream(convo_id);
  }

  componentDidUpdate(prevProps) {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      messageFromNotifications
    } = this.props;
    if (
      messageFromNotifications &&
      messageFromNotifications[0].convo_id == convo_id &&
      messageFromNotifications != prevProps.messageFromNotifications
    ) {
      AsyncStorage.getItem("@chats").then(data => {
        if (data) {
          let _data = JSON.parse(data);
          this.updateUIWithChats(
            _data,
            messageFromNotifications,
            convo_id,
            _data[convo_id].pagination,
            true
          );
        } else {
          this.updateUIWithChats(
            {},
            messageFromNotifications,
            convo_id,
            {
              total_pages: 1,
              total_records: 1,
              current_page: 0
            },
            false
          );
        }
        this.props.setMessageForCurrentViewedMessagePage(null);
      });
    }
  }

  componentWillUnmount() {
    this.props.setCurrentPageData(null, null);
    this.eventSource.removeAllListeners();
  }

  deleteUnreadMessagesFromNotificationStore = convo_id => {
    let { unreadMessages } = this.props,
      newUnreadMessages = unreadMessages.filter(
        msg => msg.convo_id != convo_id
      );

    AsyncStorage.setItem(
      "@unreadMessages",
      JSON.stringify(newUnreadMessages)
    ).then(data => {
      this.props.setUnreadMessages(newUnreadMessages);
    });
  };

  connectedToEventStream = convo_id => {
    this.eventSource = new EventSource(
      `https://www.mmbhomecare.com/api/chat/stream?convo_id=${convo_id}`
    );

    this.eventSource.addEventListener("message", ev => {
      this.fetchUnreadMessages();
    });

    this.eventSource.addEventListener("open", ev => {
      //set state with connection stream
    });

    this.eventSource.addEventListener("error", ev => {
      if (ev.readyState == EventSource.CLOSED) {
      }
    });
  };

  updateLatestConversation = (message, created) => {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      conversations,
      setConversations
    } = this.props;

    new Promise((resolve, reject) => {
      let _newConversations = conversations.map((convo, i) => {
        if (convo.convo_id == convo_id) {
          return {
            ...convo,
            last_message: { ...convo.last_message, message, created }
          };
        }
        return convo;
      });

      AsyncStorage.setItem("@conversations", JSON.stringify(_newConversations))
        .then(data => {
          resolve(_newConversations);
        })
        .catch(err => resolve(err));
      setConversations(_newConversations);
    })
      .then(data => null)
      .catch(err => null);
  };

  buildOptmisticUserObj = text => {
    let { previousMessages } = this.state,
      {
        currentUser: { imageUrl, _id, first_name }
      } = this.props,
      message = previousMessages[0],
      data = {
        _id: Date.now(),
        text,
        createdAt: new Date(moment(new Date()).format("MMMM DD, YYYY H:mm:ss")),
        sent: false,
        id: _id,
        user: { _id, name: first_name, avatar: imageUrl }
      };
    return data;
  };

  saveUpdatedInforSync = (
    parsedMessages,
    convo_id,
    _previousMessages,
    updatingUnread
  ) => {
    let _prevChats =
        (_previousMessages[convo_id] && _previousMessages[convo_id].chats) ||
        {},
      _prevPagination =
        (_previousMessages[convo_id] &&
          _previousMessages[convo_id].pagination) ||
        (parsedMessages[convo_id] && parsedMessages[convo_id].pagination);

    let _storedChats = {
      ..._previousMessages,
      [convo_id]: {
        ..._previousMessages[convo_id],
        chats: {
          ...parsedMessages[convo_id].chats,
          ..._prevChats
        },
        pagination: {
          ..._prevPagination,
          total_records: updatingUnread
            ? parseInt(_prevPagination.total_records) + 1
            : _prevPagination.total_records
        }
      }
    };

    AsyncStorage.setItem("@chats", JSON.stringify(_storedChats)).then(
      result => {}
    );

    let _conversationBuilder = Object.values(parsedMessages[convo_id].chats)[0];
    if (_conversationBuilder) {
      let { text, createdAt } = _conversationBuilder;
      this.updateLatestConversation(text, createdAt);
    }
  };

  updateUIWithChats = (
    _previousMessages,
    newMessages,
    convo_id,
    paginationInfo,
    updatingUnread
  ) => {
    let {
      navigation: {
        state: {
          params: { convo_info }
        }
      },
      currentUser
    } = this.props;

    let parsedMessages = Util.patchStatesOfMessages(
      _previousMessages,
      newMessages,
      convo_id,
      currentUser,
      convo_info
    );

    paginationInfo && (parsedMessages[convo_id].pagination = paginationInfo);
    let _thisChats = parsedMessages[convo_id].chats;

    this.setState(prevState => {
      return {
        previousMessages: GiftedChat.append(
          prevState.previousMessages,
          Object.values(_thisChats)
        ),
        numFetchedMessages:
          prevState.numFetchedMessages + Object.values(_thisChats).length
      };
    });

    this.saveUpdatedInforSync(
      parsedMessages,
      convo_id,
      _previousMessages,
      updatingUnread
    );
  };

  saveMessage = (convo_id, newMessages, data) => {
    AsyncStorage.getItem("@chats")
      .then(chats => {
        let _previousMessages = {};
        if (chats) _previousMessages = JSON.parse(chats);

        let {
          navigation: {
            state: {
              params: { convo_info }
            }
          },
          currentUser
        } = this.props;

        this.updateUIWithChats(
          _previousMessages,
          newMessages,
          convo_id,
          data,
          false
        );
      })
      .catch(err => null);
  };

  fetchUnreadMessages = () => {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      fetchUnreadMessages
    } = this.props;

    fetchUnreadMessages({ data: { convo_id } })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (code == 100) {
          AsyncStorage.getItem("@chats").then(data => {
            if (data) {
              let _data = JSON.parse(data);
              this.updateUIWithChats(
                _data,
                entity.messages,
                convo_id,
                _data[convo_id].pagination,
                true
              );
            }
          });
        } else {
          //@Send to app center indicating a bad network
        }
      })
      .catch(err => null);
  };

  fetchPrevMessages = () => {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      fetchPreviousMessages
    } = this.props;

    fetchPreviousMessages({
      data: { convo_id },
      params: { page: 0 }
    })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (code == 100) {
          let { messages, total_records, total_pages, current_page } = entity;

          this.saveMessage(convo_id, messages, {
            total_records,
            total_pages,
            current_page
          });
        } else if (code == 106) {
          Util.logout(this.props.navigation);
        } else {
          Toast.show({
            text: `Error loading message`,
            textStyle: ToastStyle.toast,
            buttonText: "okay",
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            buttonStyle: ToastStyle.buttonStyle,
            type: "danger",
            duration: 10000
          });
        }
      })
      .catch(err => {
        Toast.show({
          text: `No network connection to load previous online messages`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          buttonText: "okay",
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          buttonStyle: ToastStyle.buttonStyle,
          type: "danger"
        });
      });
  };

  sendMessage = props => {
    props.onSend({ text: props.text.trim() }, true);
    let _obj = this.buildOptmisticUserObj(props.text.trim());
    this.setState(prevState => {
      return {
        previousMessages: GiftedChat.append(prevState.previousMessages, _obj),
        numFetchedMessages: prevState.numFetchedMessages + 1
      };
    });

    let { sendMessage } = this.props,
      {
        navigation: {
          state: {
            params: {
              convo_info: { convo_id }
            }
          }
        }
      } = this.props;

    (_obj => {
      sendMessage({ data: { convo_id, message: _obj.text } })
        .then(result => {
          let {
            status: { code, desc },
            entity
          } = result;
          if (code == 100) {
            //@see whats coming in and append done to the mesage modal
            AsyncStorage.getItem("@chats").then(data => {
              let _data = (data && JSON.parse(data)) || {},
                _prevMessages = (_data && _data[convo_id]) || {},
                _prevChats = _prevMessages.chats || {},
                _prevPagination = _prevMessages.pagination || {},
                _total_records = _prevPagination.total_records || 0;

              let _newData = {
                ..._data,
                [convo_id]: {
                  ..._prevMessages,
                  chats: {
                    [entity.date]: {
                      ..._obj,
                      createdAt: new Date(
                        moment(entity.date).format("MMMM DD, YYYY H:mm:ss")
                      ),
                      sent: true
                    },
                    ..._prevChats
                  },
                  pagination: {
                    ..._prevPagination,
                    total_records: parseInt(_total_records) + 1
                  }
                }
              };

              AsyncStorage.setItem("@chats", JSON.stringify(_newData)).then(
                _result => {
                  //@Do something with the result
                  this.setState(prevState => {
                    let _newState = prevState.previousMessages.map((msg, i) => {
                      if (msg.createdAt == _obj.createdAt) {
                        return { ...msg, sent: true };
                      }
                      return msg;
                    });
                    return {
                      previousMessages: _newState
                    };
                  });

                  this.updateLatestConversation(_obj.text, _obj.createdAt);
                }
              );
            });
          }
        })
        .catch(err => null);
    })(_obj);
  };

  savePreviousMessagesInAsync = (
    parsedMessages,
    convo_id,
    _previousMessages
  ) => {
    let _prevChats =
      (_previousMessages[convo_id] && _previousMessages[convo_id].chats) || {};

    let _storedChats = {
      ..._previousMessages,
      [convo_id]: {
        ..._previousMessages[convo_id],
        chats: {
          ..._prevChats,
          ...parsedMessages[convo_id].chats
        },
        pagination: parsedMessages[convo_id].pagination
      }
    };

    AsyncStorage.setItem("@chats", JSON.stringify(_storedChats)).then(
      result => {}
    );
  };

  fetchMoreFromServer = () => {
    let {
        navigation: {
          state: {
            params: {
              convo_info: { convo_id },
              convo_info
            }
          }
        },
        fetchPreviousMessages,
        currentUser
      } = this.props,
      { numFetchedMessages } = this.state;

    fetchPreviousMessages({
      data: { convo_id },
      params: { page: Math.floor(numFetchedMessages / 10) }
    })
      .then(result => {
        let {
          status: { code, desc },
          entity
        } = result;

        if (code == 100) {
          let { messages, total_records, total_pages, current_page } = entity;

          AsyncStorage.getItem("@chats").then(chats => {
            let _previousMessages = {};
            if (chats) _previousMessages = JSON.parse(chats);

            let parsedMessages = Util.patchStatesOfMessages(
              _previousMessages,
              messages,
              convo_id,
              currentUser,
              convo_info
            );

            parsedMessages[convo_id].pagination = {
              total_pages,
              total_records,
              current_page
            };
            let _thisChats = parsedMessages[convo_id].chats;

            this.setState(prevState => {
              return {
                previousMessages: GiftedChat.prepend(
                  prevState.previousMessages,
                  Object.values(_thisChats)
                ),
                isLoadingEarlier: false,
                isLoadingFromAsync: false,
                numFetchedMessages:
                  prevState.numFetchedMessages +
                  Object.values(_thisChats).length
              };
            });

            this.savePreviousMessagesInAsync(
              parsedMessages,
              convo_id,
              _previousMessages
            );
          });
        }
      })
      .catch(err =>
        this.setState({ isLoadingEarlier: false, isLoadingFromAsync: false })
      );
  };

  loadEarlierMessages = () => {
    let {
        navigation: {
          state: {
            params: {
              convo_info: { convo_id }
            }
          }
        }
      } = this.props,
      { numFetchedMessages, isLoadingFromAsync } = this.state;

    if (isLoadingFromAsync) {
      this.setState({ isLoadingEarlier: true });

      AsyncStorage.getItem("@chats").then(data => {
        if (data) {
          let parsedChats = JSON.parse(data)[convo_id],
            _thisChats = (parsedChats && parsedChats.chats) || {};

          let _storedMessages = Object.values(_thisChats).slice(
            numFetchedMessages,
            numFetchedMessages + 20
          );

          if (_storedMessages.length == 0) {
            this.fetchMoreFromServer();
          } else {
            this.setState(prevState => {
              return {
                previousMessages: GiftedChat.prepend(
                  prevState.previousMessages,
                  _storedMessages
                ),
                numFetchedMessages:
                  prevState.numFetchedMessages + _storedMessages.length,
                isLoadingEarlier: false
              };
            });
          }
        } else {
          this.setState({ isLoadingEarlier: false });
        }
      });
    } else {
      this.fetchMoreFromServer();
    }
  };

  openWebPage = url => {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          Toast.show({
            text: `Cannot open the web page`,
            textStyle: ToastStyle.toast,
            duration: 10000,
            buttonText: "okay",
            buttonTextStyle: ToastStyle.buttonErrorTextStyle,
            buttonStyle: ToastStyle.buttonStyle,
            type: "danger"
          });
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err =>
        Toast.show({
          text: `Error occurred in opening web page`,
          textStyle: ToastStyle.toast,
          duration: 10000,
          buttonText: "okay",
          buttonTextStyle: ToastStyle.buttonErrorTextStyle,
          buttonStyle: ToastStyle.buttonStyle,
          type: "danger"
        })
      );
  };

  parsePatterns = linkStyle => [
    { type: "url", style: styles.url, onPress: this.openWebPage }
  ];

  renderActions = () => {
    let {
      navigation: {
        state: {
          params: {
            convo_info: { convo_id }
          }
        }
      },
      accountType
    } = this.props;

    return accountType == "carer" ? (
      <Icon
        name="md-add"
        type="ionic-icon"
        contentContainerStyle={styles.chatButtonContainer}
        style={styles.chatButton}
        onPress={() =>
          this.props.navigation.navigate("createContract", { convo_id })
        }
      />
    ) : null;
  };

  renderSend = props =>
    props.text.trim().length > 1 ? (
      <Icon
        {...props}
        name="md-send"
        type="ionic-icon"
        contentContainerStyle={StyleSheet.flatten([
          styles.chatButtonContainer,
          {
            marginLeft: 0,
            marginRight: 10
          }
        ])}
        style={styles.chatButton}
        onPress={() => this.sendMessage(props)}
      />
    ) : null;

  renderBubble = props => (
    <Bubble
      {...props}
      wrapperStyle={{
        left: { backgroundColor: "#e6e9ec" },
        right: {
          backgroundColor: "#00b0cf"
        }
      }}
    />
  );

  render() {
    let {
        navigation: {
          navigate,
          state: {
            params: { convo_info }
          }
        },
        loadingPreviousMessages,
        currentUser
      } = this.props,
      { message, previousMessages, isLoadingEarlier } = this.state;

    let _currentUser = {
      _id: currentUser._id
    };

    return (
      <NavigationProvider navigation={this.props.navigation}>
        <AppHeader title={convo_info.full_name} />
        <View style={styles.giftedContainer}>
          <GiftedChat
            messages={previousMessages}
            placeholder="Type your message"
            user={_currentUser}
            isAnimated={true}
            minInputToolbarHeight={50}
            renderActions={this.renderActions}
            renderSend={this.renderSend}
            renderBubble={this.renderBubble}
            loadEarlier={true}
            onLoadEarlier={this.loadEarlierMessages}
            alwaysShowSend={false}
            isLoadingEarlier={loadingPreviousMessages || isLoadingEarlier}
            parsePatterns={this.parsePatterns}
          />
        </View>
      </NavigationProvider>
    );
  }
}

const _Chats = props => (
  <ReduxContext.Consumer>
    {({
      screenProps: {
        setConversations,
        setUnreadMessages,
        setCurrentPageData,
        setMessageForCurrentViewedMessagePage
      }
    }) => (
      <MessagingErrorBoundary>
        <Mutation operation="createPreviousMessages">
          {(previousMessages, fetchPreviousMessages) => (
            <Mutation operation="createSendMessage">
              {(mutationState, mutate) => (
                <Mutation operation="createUnreadMessages">
                  {(unreadState, fetchUnreadMessages) => (
                    <Chats
                      loadingPreviousMessages={previousMessages.loading}
                      sendingMessage={mutationState.loading}
                      fetchPreviousMessages={fetchPreviousMessages}
                      sendMessage={mutate}
                      loadingUnread={unreadState.loading}
                      fetchUnreadMessages={fetchUnreadMessages}
                      setConversations={setConversations}
                      setUnreadMessages={setUnreadMessages}
                      setCurrentPageData={setCurrentPageData}
                      setMessageForCurrentViewedMessagePage={
                        setMessageForCurrentViewedMessagePage
                      }
                      {...props}
                    />
                  )}
                </Mutation>
              )}
            </Mutation>
          )}
        </Mutation>
      </MessagingErrorBoundary>
    )}
  </ReduxContext.Consumer>
);

function mapStateToProps(state, ownProps) {
  return {
    currentUser: state.currentUser,
    accountType: state.accountType,
    unreadMessages: state.unreadMessages,
    conversations: state.conversations,
    messageFromNotifications: state.messageForCurrentViewedMessagePage
  };
}

export default connect(mapStateToProps)(_Chats);

const styles = StyleSheet.create({
  /**Gifted */
  chatButton: {
    color: "#00b0cf",
    fontSize: 30
  },
  chatButtonContainer: {
    alignSelf: "flex-end",
    marginLeft: 10,
    marginBottom: 10
  },
  giftedContainer: {
    backgroundColor: "#fff",
    flex: 1
  },
  url: {
    color: "#fff",
    textDecorationLine: "underline"
  }
});
