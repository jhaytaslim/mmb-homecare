/**
 * This file is a top level component of the application
 * This file should only contain top level integrations such as redux integrations, codePush or firebase integrations
 * This is necessary to provide this functionalities throughout the whole application itself
 */
import React from "react";
import KunyoraClient from "kunyora";
import { KunyoraProvider, Mutation } from "react-kunyora";
import PropTypes from "prop-types";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import { AsyncStorage, Platform, SafeAreaView, AppState } from "react-native";
import { Root } from "native-base";
import SplashScreen from "react-native-splash-screen";
import PushNotification from "react-native-push-notification";

import Routes from "./Routes";
import { appReducer } from "./reducers";
import { AccountTypeContext } from "./context/AccountTypeContext";
import kunyoraConfig from "./kunyora.config";
import { Util } from "./utils";

let NOTIFICATIONS = undefined;
let BACKGROUND_STATE = undefined;
let NOTIFICATIONS_COUNTER = 0;
let DEVICE_TOKEN = undefined;
let client = KunyoraClient({ ...kunyoraConfig });

const store = createStore(
  combineReducers({
    ...appReducer
  }),
  {}
);

client.middleware({
  useBeforeRequest: function(header) {
    return {
      ...header,
      common: {
        ...header.common,
        deviceType: Platform.OS == "android" ? "android" : "ios",
        Authorization: `Bearer ${store.getState().jwt}`
      }
    };
  }
});

class AccountProvider extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      account: this.props.account,
      setAccount: this.setAccount,
      appState: AppState.currentState
    };
  }

  static propTypes = {
    account: PropTypes.any,
    updateDeviceToken: PropTypes.func
  };

  componentDidMount() {
    /**
     * @This is an hack.
     * This hack involves passing custom value to the NOTIFICATION values
     * when the react native realm is started and so that we can different between
     * handling notifications when the app is totally killed and when its in the background/foreground state
     */
    NOTIFICATIONS = Date.now();
    BACKGROUND_STATE = "active";
    NOTIFICATIONS_COUNTER = 0;
    PushNotification.setApplicationIconBadgeNumber(NOTIFICATIONS_COUNTER);

    if (store.getState().jwt.length > 0) {
      this.props
        .updateDeviceToken({ data: { token: DEVICE_TOKEN } })
        .then(data => null)
        .catch(err => null);
    }

    AppState.addEventListener("change", this.handlePushNotification);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handlePushNotification);
  }

  handlePushNotification = nextAppState => {
    if (nextAppState == "background" || nextAppState == "inactive") {
      BACKGROUND_STATE = "background";
    } else {
      BACKGROUND_STATE = "active";
    }

    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState == "active"
    ) {
      NOTIFICATIONS &&
        typeof NOTIFICATIONS != "number" &&
        store.dispatch({
          type: "@pushNotificationData",
          pushNotificationData: NOTIFICATIONS
        });
    }
    this.setState({ appState: nextAppState });
  };

  setAccount = account => this.setState({ account });

  render() {
    return (
      <AccountTypeContext.Provider value={this.state}>
        {this.props.children}
      </AccountTypeContext.Provider>
    );
  }
}

export default class App extends React.PureComponent {
  state = {
    shouldComponentRender: false,
    account: undefined
  };

  componentDidMount() {
    SplashScreen.hide();

    AsyncStorage.multiGet([
      "@jwt",
      "@userType",
      "@user",
      "@carer",
      "@paymentList",
      "@contractList",
      "@notificationLogs",
      "@contractLogs",
      "@conversations",
      "@withdrawalHistory",
      "@subscription",
      "@unreadMessages",
      "@setUncheckedNewContract"
    ])
      .then(results => {
        if (results[0][1]) {
          store.dispatch({
            type: "@jwt",
            jwt: results[0][1]
          });
        }

        if (results[1][1]) {
          store.dispatch({
            type: "@userType",
            accountType: results[1][1]
          });
        }

        if (results[2][1]) {
          store.dispatch({
            type: "@user",
            user: JSON.parse(results[2][1])
          });
        }

        if (results[3][1]) {
          store.dispatch({
            type: "@carer",
            carer: JSON.parse(results[3][1])
          });
        }

        if (results[4][1]) {
          store.dispatch({
            type: "@paymentList",
            paymentList: JSON.parse(results[4][1])
          });
        }

        if (results[5][1]) {
          store.dispatch({
            type: "@contractList",
            contractList: JSON.parse(results[5][1])
          });
        }

        if (results[6][1]) {
          store.dispatch({
            type: "@notificationLogs",
            notificationLogs: JSON.parse(results[6][1])
          });
        }

        if (results[7][1]) {
          store.dispatch({
            type: "@contractLogs",
            contractLogs: JSON.parse(results[7][1])
          });
        }

        if (results[8][1]) {
          store.dispatch({
            type: "@conversations",
            conversations: JSON.parse(results[8][1])
          });
        }

        if (results[9][1]) {
          store.dispatch({
            type: "@withdrawalHistory",
            withdrawalHistory: JSON.parse(results[9][1])
          });
        }

        if (results[10][1]) {
          store.dispatch({
            type: "@subscription",
            subscription: JSON.parse(results[10][1])
          });
        }

        if (results[11][1]) {
          let newMessages = store.getState().newMessagesNotification,
            _data = JSON.parse(results[11][1]);

          if (newMessages) {
            AsyncStorage.setItem(
              "@unreadMessages",
              JSON.stringify([...newMessages, ..._data])
            )
              .then(data => {
                store.dispatch({
                  type: "@setUnreadMessages",
                  unreadMessages: [...newMessages, ..._data]
                });
                store.dispatch({
                  type: "@updatNewMessageNotificationState",
                  newMessages: null
                });
              })
              .catch(err => null);
          } else {
            store.dispatch({
              type: "@setUnreadMessages",
              unreadMessages: _data
            });
          }
        } else {
          store.dispatch({
            type: "@setUnreadMessages",
            unreadMessages: []
          });
        }

        if (results[12][1]) {
          let newContracts = store.getState().uncheckedNewContracts,
            _data = JSON.parse(results[12][1]);

          if (newContracts) {
            AsyncStorage.setItem(
              "@setUncheckedNewContracts",
              JSON.stringify([...newContracts, ..._data])
            )
              .then(data => {
                store.dispatch({
                  type: "@setUncheckedNewContracts",
                  newContracts: [...newContracts, ..._data]
                });
              })
              .catch(err => null);
          } else {
            store.dispatch({
              type: "@setUnreadMessages",
              unreadMessages: _data
            });
          }
        } else {
          store.dispatch({
            type: "@setUnreadMessages",
            unreadMessages: []
          });
        }

        this.setState({ shouldComponentRender: true, account: results[1][1] });
      })
      .catch(err => null);
  }

  render() {
    let { shouldComponentRender, account } = this.state;
    return shouldComponentRender ? (
      <Root>
        <Provider store={store}>
          <KunyoraProvider client={client} store={client.store}>
            <Mutation operation="updateDeviceToken">
              {(mutationState, mutate) => (
                <AccountProvider account={account} updateDeviceToken={mutate}>
                  {Platform.OS == "android" ? (
                    <Routes />
                  ) : (
                    <SafeAreaView style={{ flex: 1 }}>
                      <Routes />
                    </SafeAreaView>
                  )}
                </AccountProvider>
              )}
            </Mutation>
          </KunyoraProvider>
        </Provider>
      </Root>
    ) : null;
  }
}

PushNotification.configure({
  onRegister: device => {
    DEVICE_TOKEN = device.token;
    store.dispatch({
      type: "@setDeviceToken",
      deviceToken: DEVICE_TOKEN
    });
  },
  onNotification: notification => {
    let { currentPageName, currentId } = store.getState().currentPageData;
    if (
      currentPageName &&
      currentPageName == notification.type &&
      currentPageName == "message" &&
      BACKGROUND_STATE == "active" &&
      currentId == notification.message_id
    ) {
      /**
       * @There is no need to dispatch, just ignore because probably
       * the user has seen the message since its real time
       *
       *
       * However, in the future, handle cases where the firebase notification
       * gets to the device faster than the message itself, we want to
       * display a push notification in the case and clear it almost immediately after the using
       * has clicked on it without duplicating the data
       */
      store.dispatch({
        type: "@setMessageForCurrentViewedMessagePage",
        message: [Util.buildMessageObjectFromNotification(notification)]
      });
    } else if (!notification.userInteraction) {
      NOTIFICATIONS
        ? (notification = {
            ...notification,
            title: notification.title.trim(),
            message: notification.body.trim()
          })
        : (notification = {
            ...notification,
            id: "1",
            title: notification.title.trim(),
            message: notification.body.trim()
          });
      PushNotification.localNotification({
        ...notification
      });

      if (notification.type == "message") {
        let previousUnreadMessages =
          store.getState().newMessagesNotification || [];
        store.dispatch({
          type: "@updatNewMessageNotificationState",
          newMessages: [
            Util.buildMessageObjectFromNotification(notification),
            ...previousUnreadMessages
          ]
        });
      } else if (notification.type == "new_contract") {
        let previousUncheckedNewContracts =
          store.getState().uncheckedNewContracts || [];
        store.dispatch({
          type: "@setUncheckedNewContracts",
          newContracts: [
            Util.buildContractObjectFromNotification(notification),
            ...previousUncheckedNewContracts
          ]
        });
      }
    }

    if (!NOTIFICATIONS) {
      NOTIFICATIONS_COUNTER += 1;
      PushNotification.setApplicationIconBadgeNumber(NOTIFICATIONS_COUNTER);
      store.dispatch({
        type: "@pushNotificationData",
        pushNotificationData: { ...notification, timeOfArrival: Date.now() }
      });
    }

    notification.userInteraction &&
      (NOTIFICATIONS = { ...notification, timeOfArrival: Date.now() });
  },
  senderID: "424104180383",
  requestPermissions: true,
  popInitialNotification: false
});
