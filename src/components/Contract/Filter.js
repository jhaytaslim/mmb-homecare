import React from "react";
import {
  View,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback
} from "react-native";
import PropTypes from "prop-types";
import { Icon, CheckBox, ListItem, Body } from "native-base";

import { MediumText } from "../AppText";

export default class Filter extends React.PureComponent {
  state = {
    pendingState: "all"
  };

  static propTypes = {
    isVisible: PropTypes.bool,
    onClose: PropTypes.func
  };

  handleSelection = state => {
    this.setState({ pendingState: state }, () => this.props.onClose(state));
  };

  render() {
    let { pendingState } = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => this.props.onClose()}
      >
        <View style={styles.container}>
          <TouchableWithoutFeedback
            onPress={() => this.handleSelection(pendingState)}
          >
            <View style={{ flex: 1 }} />
          </TouchableWithoutFeedback>
          <View style={styles.content}>
            <View style={styles.header}>
              <MediumText> Filter By </MediumText>
              <Icon
                name="x"
                type="Feather"
                style={styles.icon}
                onPress={() => this.handleSelection(pendingState)}
              />
            </View>

            <ListItem style={styles.segment}>
              <CheckBox
                onPress={() => this.handleSelection("active")}
                checked={pendingState == "active"}
                color="#00b0cf"
              />
              <Body>
                <MediumText> Active Contracts </MediumText>
              </Body>
            </ListItem>
            <ListItem style={styles.segment}>
              <CheckBox
                onPress={() => this.handleSelection("pending")}
                checked={pendingState == "pending"}
                color="#00b0cf"
              />
              <Body>
                <MediumText> Pending Contracts </MediumText>
              </Body>
            </ListItem>
            <ListItem style={styles.segment}>
              <CheckBox
                onPress={() => this.handleSelection("all")}
                checked={pendingState == "all"}
                color="#00b0cf"
              />
              <Body>
                <MediumText> All Contracts</MediumText>
              </Body>
            </ListItem>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  content: {
    height: 300,
    padding: 10,
    backgroundColor: "#fff"
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10
  },
  icon: {
    color: "#00b0cf"
  },
  segment: {
    alignItems: "center",
    marginBottom: 8,
    borderBottomWidth: 0,
    borderBottomColor: "transparent",
    marginLeft: 0
  }
});
