import React from "react";
import { View, Picker, StyleSheet, Modal } from "react-native";
import { List, ListItem, Icon } from "native-base";
import { Query } from "react-kunyora";
import PropTypes from "prop-types";

import AppHeader from "./AppHeader";
import { MediumText } from "./AppText";
import PaddingProvider from "../containers/PaddingProvider";
import RequestAcitivityIndicator from "./RequestActivityIndicator";
import Colors from "../assets/Colors";
import Fonts from "../assets/Fonts";
import UIRenderer from "./UIRenderer";
import NativeTouchableFeedback from "./NativeTouchableFeedback";

class Filter extends React.PureComponent {
  state = {
    state: "",
    specialty: "",
    category: ""
  };

  static propTypes = {
    states: PropTypes.any,
    specialties: PropTypes.any,
    statesLoading: PropTypes.bool,
    specialtiesLoading: PropTypes.bool,
    isVisible: PropTypes.bool,
    onClose: PropTypes.func,
    refetchQuery: PropTypes.func,
    refetchSpecialty: PropTypes.func
  };

  clearFilter = () => {
    this.setState({
      state: "",
      specialty: "",
      category: ""
    });
  };

  render() {
    let {
        statesLoading,
        specialtiesLoading,
        states,
        specialties,
        refetchQuery,
        refetchSpecialty
      } = this.props,
      _specialties = specialties || {},
      _entitySpecialities = _specialties.entity || {},
      _specialtiesArr = _entitySpecialities.specialties || [],
      _states = states || {},
      _entityStates = _states.entity || {},
      _statesArr = _entityStates.states || [],
      { state, specialty, category } = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={() => this.props.onClose(this.state)}
      >
        <View style={styles.container}>
          <View style={styles.titleHeader}>
            <MediumText style={styles.title} onPress={this.clearFilter}>
              Clear
            </MediumText>
            <MediumText style={styles.title}>Filter By</MediumText>
            <MediumText
              style={styles.title}
              onPress={() => this.props.onClose(this.state)}
            >
              Search
            </MediumText>
          </View>
          <UIRenderer
            loading={statesLoading || specialtiesLoading}
            containerType="plain"
            timeElapseToMountIndicator={2000}
            onReload={() => {
              this.props.refetchQuery();
              this.props.refetchSpecialty();
            }}
            isRefreshing={statesLoading || specialtiesLoading}
            error={
              (_statesArr.length == 0 || _specialtiesArr.length == 0) &&
              this.props.error
            }
          >
            <List>
              <ListItem style={styles.listItem}>
                <Icon name="md-add-circle" style={styles.icon} />
                <MediumText style={styles.text}>
                  {" "}
                  Select health category:{" "}
                </MediumText>
                <Picker
                  selectedValue={specialty}
                  mode="dropdown"
                  style={styles.picker}
                  onValueChange={itemValue =>
                    this.setState({ specialty: itemValue })
                  }
                >
                  {["select", ..._specialtiesArr].map((specialties, i) => {
                    let label = specialties.replace(/<[^>]*>/gi, "");
                    return (
                      <Picker.Item
                        key={i}
                        label={label}
                        value={label == "select" ? "" : label}
                      />
                    );
                  })}
                </Picker>
              </ListItem>
              <ListItem style={styles.listItem}>
                <Icon name="md-pin" style={styles.icon} />
                <MediumText style={styles.text}> Select State: </MediumText>
                <Picker
                  selectedValue={state}
                  mode="dropdown"
                  style={styles.picker}
                  onValueChange={itemValue =>
                    this.setState({ state: itemValue })
                  }
                >
                  {[
                    { name: "select a state", state_id: "" },
                    ..._statesArr
                  ].map((state, i) => {
                    return (
                      <Picker.Item
                        key={i}
                        label={state.name}
                        value={state.state_id}
                      />
                    );
                  })}
                </Picker>
              </ListItem>
              <ListItem style={styles.listItem}>
                <Icon name="md-person" style={styles.icon} />
                <MediumText style={styles.text}>
                  Select Carer Category:
                </MediumText>
                <Picker
                  onValueChange={itemValue =>
                    this.setState({ category: itemValue })
                  }
                  selectedValue={category}
                  mode="dropdown"
                  style={styles.picker}
                >
                  <Picker.Item label="Select a category" value="" />
                  <Picker.Item label="Doctor" value="doctor" />
                  <Picker.Item label="Nurse" value="nurse" />
                  <Picker.Item
                    label="Physiotherapist"
                    value="physiotherapist"
                  />
                </Picker>
              </ListItem>
            </List>
          </UIRenderer>
        </View>
      </Modal>
    );
  }
}

export default (_Filter = props => (
  <Query operation="getCarerSpecialties">
    {({ data, loading, error }, fetchMore, refetchQuery) => (
      <Query operation="getStates">
        {(result, stateFetchFn, statesRefetchFn) => (
          <Filter
            states={result.data}
            specialties={data}
            statesLoading={result.loading}
            error={error}
            specialtiesLoading={loading}
            refetchQuery={statesRefetchFn}
            refetchSpecialty={refetchQuery}
            {...props}
          />
        )}
      </Query>
    )}
  </Query>
));

const styles = StyleSheet.create({
  listItem: {
    marginLeft: 0,
    paddingHorizontal: 20
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  doneContainer: {
    height: 40,
    width: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  icon: {
    color: "#b6e3ec"
  },
  text: {
    color: "#333",
    marginLeft: 10
  },
  picker: {
    flex: 1,
    color: "#0096c0"
  },
  titleHeader: {
    backgroundColor: Colors.appHeader.titleHeaderBackground,
    marginTop: 0,
    flexDirection: "row",
    height: 50,
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20
  },
  title: {
    color: Colors.appHeader.title,
    fontSize: Fonts.appHeader.text
  },
  done: {
    color: Colors.appHeader.title,
    fontSize: Fonts.appHeader.text
  }
});
