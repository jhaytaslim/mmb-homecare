import React, { Component } from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import {
  Container,
  Header,
  Content,
  ListItem,
  Form,
  Item,
  Icon,
  Input,
  Label
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import Dimensions from "Dimensions";

import AppButton from "../AppButton";
import styles from "./style";

export default class Login extends React.Component {
  static propTypes = {
    withIcon: PropTypes.bool,
    icon: PropTypes.any,
    text: PropTypes.string,
    style: PropTypes.any,
    contentContainerStyle: PropTypes.any,
    onPress: PropTypes.func
  };

  render() {
    const Height = Dimensions.get("window").height;

    return (
      <Container>
        <Content contentContainerStyle={styles.Center}>
          <Grid>
            <Col style={[styles.OuterColumn, { height: Height }]}>
              <Col style={[styles.InnerColumn, { height: Height * 0.8 }]}>
                <Form>
                  <Item>
                    <Icon active name="message" />
                    <Input placeholder="Username" />
                  </Item>
                  <Item>
                    <Icon active name="password" />
                    <Input placeholder="Password" />
                    <Icon active name="password" />
                  </Item>
                </Form>
                <View style={[styles.Center, { flex: 1 }]}>
                  <View style={{ height: 80 }}>
                    <AppButton
                      text="Log in"
                      style={{ height: 40, width: 160 }}
                    />
                  </View>
                  <View style={{ height: 80 }}>
                    <AppButton
                      text="Create an account"
                      style={{ height: 40, width: 260, backgroundColor: "red" }}
                    />
                  </View>
                </View>
              </Col>
            </Col>
            {/* <Col style={{ backgroundColor: '#00CE9F', height: 200 }}></Col> */}
          </Grid>
        </Content>
      </Container>
    );
  }
}
