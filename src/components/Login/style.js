import React from 'react';
import { StyleSheet } from "react-native";

const styles=StyleSheet.create({
    
    Container:{
        flex: 1,
        width: null,
        height: null,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        backgroundColor:"red",
        opacity:0.5,
        elevation:2,
        borderColor:"green",
        borderWidth: 10,
        padding:20,
        margin:20,
        justifyContent: "center",
        alignItems: "center"
    },
    OuterColumn:{
        backgroundColor:'rgb(101, 223, 237)' , 
        //height: Height , 
        padding:20,
        paddingRight:20,
        paddingLeft:20,
        justifyContent:"center",
    },
    InnerColumn:{
        backgroundColor:'rgb(72, 183, 206)',
        //height: Height*0.8
    },   
    Center:{
        justifyContent: "center",
        alignItems: "center"
    },
    RowCenter:{
        justifyContent: "center",
        alignItems: "center",
        height: 20,
    },
    Text:{
        textAlign:'center',
        alignItems:'center'
    },

});

  
export default styles;