import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { View, Text, StyleSheet } from "react-native";
import { Input, Item, Icon } from "native-base";

class ChatInputBox extends PureComponent {
  static propTypes = {
    onPressSend: PropTypes.func,
    onAddContract: PropTypes.func,
    value: PropTypes.string,
    onPressEmoji: PropTypes.func,
    onChangeText: PropTypes.func
  };

  render() {
    const {
      onPressSend,
      onPressEmoji,
      onAddContract,
      onChangeText,
      value
    } = this.props;

    return (
      <View style={{ justifyContent: "flex-end" }}>
        <View style={styles.flDirection}>
          <View style={[styles.width, styles.centered]}>
            <Icon
              name="add"
              style={styles.whiteColor}
              onPress={onAddContract}
            />
          </View>
          <View style={styles.maxWidth}>
            <Item rounded style={styles.roundedInput}>
              <Input
                placeholder="Type your message"
                placeholderTextColor={styles.ashcolor}
                value={value}
                onFocus={this.props.onFocus}
                onChangeText={onChangeText}
                style={styles.inputHeight}
              />
            </Item>
          </View>
          <View style={[styles.width, styles.centered]}>
            <Icon name="send" style={styles.whiteColor} onPress={onPressSend} />
          </View>
        </View>
      </View>
    );
  }
}

export default ChatInputBox;

const styles = StyleSheet.create({
  flDirection: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 70,
    backgroundColor: "#00b9d3"
  },
  ashcolor: {
    color: "#ccc"
  },
  roundedInput: {
    paddingHorizontal: 20,
    paddingVertical: 0.5,
    backgroundColor: "#fff",
    borderColor: "#fff"
  },
  inputHeight: {
    height: 40,
    fontFamily: "Quicksand-Medium"
  },
  blue: {
    color: "#00b9d3"
  },
  whiteColor: {
    color: "#fff"
  },
  width: {
    flex: 0.5
  },
  maxWidth: {
    flex: 3
  },
  centered: {
    alignItems: "center"
  }
});
