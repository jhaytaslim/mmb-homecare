import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import moment from "moment";

import { View, StyleSheet, Image } from "react-native";
import { Input, Item, Icon } from "native-base";
import { MediumText, BoldText } from "../AppText/";

class ChatListItem extends PureComponent {
  static propTypes = {
    chatText: PropTypes.string,
    time: PropTypes.string,
    photo: PropTypes.string,
    status: PropTypes.number,
    userType: PropTypes.string
  };

  renderListItem = () => {
    const { chatText, time, photo, status, userType } = this.props;

    return userType === "sender" ? (
      <View>
        <View style={styles.aiEnd}>
          <View style={[styles.jcBtw, styles.flDirection]}>
            <View
              style={[
                styles.flDirectionCol,
                styles.padRightSm,
                styles.autoWidth
              ]}
            >
              <View style={styles.sender}>
                <MediumText style={styles.ashColor}>{chatText}</MediumText>
              </View>
              <View style={styles.aiEnd}>
                <View
                  style={[styles.flDirection, styles.width, styles.padTop5]}
                >
                  <BoldText style={{ paddingRight: 10 }}>
                    {moment(time).format("HH:mm")}
                  </BoldText>
                  {status === "sent" ? (
                    <Icon
                      name="md-checkmark-circle-outline"
                      style={styles.iconXs}
                    />
                  ) : (
                    <Icon name="md-checkmark" style={styles.done} />
                  )}
                </View>
              </View>
            </View>
            <View>
              {photo !== "" ? (
                <Image
                  source={{
                    uri: `https://www.mmbhomecare.com/uploads/70-${photo}`
                  }}
                  style={styles.imageStyle}
                />
              ) : (
                <Icon
                  name="md-person"
                  style={{ fontSize: 44, color: "#00d4e2" }}
                />
              )}
            </View>
          </View>
        </View>
      </View>
    ) : (
      <View>
        <View style={styles.aiStart}>
          <View style={[styles.jcBtw, styles.flDirection]}>
            <View style={{ paddingRight: 15 }}>
              {photo !== "" ? (
                <Image
                  source={{
                    uri: `https://www.mmbhomecare.com/uploads/70-${photo}`
                  }}
                  style={styles.imageStyle}
                />
              ) : (
                <Icon
                  name="md-person"
                  style={{ fontSize: 44, color: "#00d4e2", paddingRight: 15 }}
                />
              )}
            </View>
            <View style={[styles.flDirectionCol, styles.autoWidth]}>
              <View style={styles.receiver}>
                <MediumText style={styles.whiteColor}>{chatText}</MediumText>
              </View>
              <View style={styles.aiStart}>
                <View
                  style={[styles.flDirection, styles.width, styles.padTop5]}
                >
                  <BoldText style={{ paddingRight: 10 }}>
                    {moment(time).format("HH:mm")}
                  </BoldText>
                  {status === "sent" ? (
                    <Icon
                      name="md-checkmark-circle-outline"
                      style={styles.iconXs}
                    />
                  ) : (
                    <Icon name="md-checkmark" style={styles.done} />
                  )}
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return this.renderListItem();
  }
}

export default ChatListItem;

const styles = StyleSheet.create({
  flDirection: {
    flexDirection: "row",
    alignItems: "center"
  },
  flDirectionCol: {
    flexDirection: "column"
  },
  ashcolor: {
    color: "#ccc"
  },
  blue: {
    color: "#00b9d3"
  },
  whiteColor: {
    color: "#fff"
  },
  ashColor: {
    color: "#bcbcbf"
  },
  width: {
    flex: 0.5
  },
  maxWidth: {
    flex: 3
  },
  centered: {
    alignItems: "center"
  },
  imageStyle: {
    borderRadius: 100,
    width: 30,
    height: 30
  },
  sender: {
    backgroundColor: "#fff",
    padding: 15,
    borderRadius: 50
  },
  receiver: {
    backgroundColor: "#4285f4",
    padding: 15,
    borderRadius: 50,
    flex: 1
  },
  jcEnd: {
    justifyContent: "flex-end"
  },
  jsStart: {
    justifyContent: "flex-start"
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  aiEnd: {
    alignItems: "flex-end"
  },
  aiStart: {
    alignItems: "flex-start"
  },
  iconXs: {
    fontSize: 13,
    color: "#6cbfc9"
  },
  done: {
    color: "green",
    fontSize: 13
  },
  pad15: {
    padding: 15
  },
  padRightSm: {
    paddingRight: 20
  },
  padTop10: {
    paddingTop: 10
  },
  padTop5: {
    paddingTop: 5
  },
  iconStyle: {
    backgroundColor: "#00d4e2",
    borderRadius: 100
  },
  autoWidth: {
    flex: 0,
    flexGrow: 1,
    flexShrink: 1
  }
});
