import React from "react";
import { StatusBar, Platform, View, StyleSheet } from "react-native";

export default (AppStatusBar = ({ backgroundColor, ...props }) =>
  Platform.OS == "ios" ? (
    <View
      style={[
        styles.statusBar,
        { backgroundColor: backgroundColor || "#00b0cf" }
      ]}
    >
      <StatusBar barStyle={props.barStyle || "light-content"} />
    </View>
  ) : (
    <StatusBar
      backgroundColor={backgroundColor || "#00b0cf"}
      barStyle={props.barStyle || "light-content"}
      {...props}
    />
  ));

const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
    marginTop: -20,
    zIndex: 2000
  }
});
