import React from "react";
import { Button, Icon } from "native-base";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";

import { MediumText } from "./AppText";

export default class AppButton extends React.PureComponent {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    iconName: PropTypes.string,
    title: PropTypes.string.isRequired,
    style: PropTypes.any
  };

  render() {
    let { onPress, iconName, title, style } = this.props,
      _style = style || {};
    return (
      <Button
        onPress={onPress}
        style={[styles.button, _style]}
        iconLeft={iconName ? true : false}
      >
        {iconName && <Icon name={iconName} style={styles.icon} />}
        <MediumText style={styles.title}>{title}</MediumText>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 5,
    paddingHorizontal: 40,
    alignItems: "center",
    backgroundColor: "#00e8f1",
    borderRadius: 30,
    justifyContent: "center"
  },
  icon: {
    marginRight: 10,
    marginLeft: 10,
    color: "#fff"
  },
  title: {
    color: "#fff",
    fontSize: 15
  }
});
