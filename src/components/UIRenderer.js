import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, ScrollView } from "react-native";
import { Toast } from "native-base";

import RequestActivityIndicator from "./RequestActivityIndicator";
import NativeTouchableFeedback from "./NativeTouchableFeedback";
import Icon from "./Icon";
import Fonts from "../assets/Fonts";
import Colors from "../assets/Colors";
import { MediumText } from "./AppText";
import ToastStyle from "../assets/ToastStyle";
import AppRefreshControl from "../components/AppRefreshControl";
import PaddingProvider from "../containers/PaddingProvider";

export default class UIRenderer extends React.PureComponent {
  state = {
    hasNotifiedUserOfError: false
  };

  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onReload: PropTypes.func,
    timeElapseToMountIndicator: PropTypes.number,
    isRefreshing: PropTypes.bool,
    preferredErrorMessage: PropTypes.string,
    containerStyle: PropTypes.object,
    containerType: PropTypes.string,
    contentContainerStyle: PropTypes.any
  };

  componentDidUpdate() {
    if (!this.state.hasNotifiedUserOfError && this.props.error) {
      Toast.show({
        text: "No network connection, check connection and pull to refresh",
        duration: 10000,
        textStyle: ToastStyle.toast,
        type: "danger",
        buttonText: "OKAY",
        buttonStyle: ToastStyle.buttonStyle,
        buttonTextStyle: ToastStyle.buttonErrorTextStyle
      });
      this.setState({ hasNotifiedUserOfError: true });
    }
  }

  render() {
    let {
      loading,
      error,
      onReload,
      timeElapseToMountIndicator,
      preferredErrorMessage,
      isRefreshing,
      contentContainerStyle
    } = this.props;

    if (loading) {
      return <RequestActivityIndicator delay={timeElapseToMountIndicator} />;
    } else if (error) {
      return (
        <NativeTouchableFeedback onPress={onReload}>
          <View style={styles.container}>
            <Icon name="alert-circle" type="feather-icon" style={styles.icon} />
            <MediumText style={styles.text}>
              {preferredErrorMessage ||
                `Error occurred in connecting, tap to reload`}
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      );
    }

    return (
      <PaddingProvider
        type={this.props.containerType}
        style={this.props.containerStyle || {}}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={contentContainerStyle || {}}
          refreshControl={
            <AppRefreshControl refreshing={isRefreshing} onRefresh={onReload} />
          }
        >
          {this.props.children}
        </ScrollView>
      </PaddingProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  icon: {
    fontSize: Fonts.errorIcon,
    color: Colors.errorIconColor
  },
  text: {
    color: Colors.errorIconColor
  }
});
