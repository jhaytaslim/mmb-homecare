import React from "react";
import { View, StyleSheet, Image } from "react-native";
import PropTypes from "prop-types";

import { MediumText, BoldText } from "./AppText";
import Colors from "../assets/Colors";
import Fonts from "../assets/Fonts";
import Icon from "./Icon";

/**
 * @Component displays the profile of a card for profile displays
 */
export default class ProfileDisplayCard extends React.PureComponent {
  static propTypes = {
    amountPaid: PropTypes.string,
    serviceFor: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
    src: PropTypes.string,
    renderFooter: PropTypes.any
  };

  curencyFormatter = num => {
    return <BoldText style={styles.largeText}>{num}</BoldText>;
  };

  renderAvatarDisplay = () => {
    let { src } = this.props;
    return (
      <View style={styles.avatarContainer}>
        {src ? (
          <Image
            source={{
              uri: `https://www.mmbhomecare.com/uploads/70-${src}`
            }}
            style={styles.avatar}
          />
        ) : (
          <Icon name="md-person" type="ionic-icon" style={styles.iconAvatar} />
        )}
      </View>
    );
  };

  render() {
    let { amountPaid, serviceFor, paid, name, title } = this.props;

    return (
      <View style={styles.container}>
        {this.curencyFormatter(amountPaid)}
        <MediumText style={styles.smallText}>{serviceFor}</MediumText>
        {this.renderAvatarDisplay()}
        <BoldText style={[styles.largeText, { textAlign: "center" }]}>
          {name}
        </BoldText>
        <MediumText style={[styles.smallText, { textAlign: "center" }]}>
          {title}
        </MediumText>
        <View style={styles.lineSeperator} />
        {this.props.renderFooter || null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    backgroundColor: Colors.profileDisplayCard.background,
    marginHorizontal: 15,
    alignItems: "center",
    alignSelf: "center",
    padding: 15
  },
  largeText: {
    fontSize: Fonts.profileDisplayCard.largeText,
    color: Colors.profileDisplayCard.largeText,
    paddingVertical: 2
  },
  smallText: {
    fontSize: Fonts.profileDisplayCard.smallText,
    color: Colors.profileDisplayCard.smallText
  },
  avatarContainer: {
    height: 100,
    width: 100,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 10,
    backgroundColor: Colors.profileDisplayCard.avatarContainer
  },
  avatar: {
    borderRadius: 40,
    width: 80,
    height: 80
  },
  iconAvatar: {
    fontSize: 70,
    color: "#fff"
  },
  lineSeperator: {
    width: 250,
    backgroundColor: Colors.profileDisplayCard.lineSeperator,
    height: 1,
    marginVertical: 5,
    marginHorizontal: 3
  }
});
