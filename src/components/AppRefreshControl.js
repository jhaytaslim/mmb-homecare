import React from "react";
import { RefreshControl } from "react-native";
import PropTypes from "prop-types";

const AppRefreshControl = props => (
  <RefreshControl colors={["#00b0cf"]} {...props} />
);

AppRefreshControl.propTypes = {
  ...RefreshControl.propTypes
};

export default AppRefreshControl;
