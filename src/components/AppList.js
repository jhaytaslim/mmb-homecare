import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, RefreshControl, View, AsyncStorage } from "react-native";
import { List, Toast } from "native-base";
import { connect } from "react-redux";

import RequestActivityIndicator from "./RequestActivityIndicator";
import NativeTouchableFeedback from "./NativeTouchableFeedback";
import Icon from "./Icon";
import Fonts from "../assets/Fonts";
import Colors from "../assets/Colors";
import { MediumText } from "./AppText";
import AppRefreshControl from "./AppRefreshControl";
import PaddingProvider from "../containers/PaddingProvider";
import { ReduxContext } from "../context/ReduxContext";
import ToastStyle from "../assets/ToastStyle";

class AppList extends React.PureComponent {
  state = {
    hasNotifiedUserOfError: false
  };

  static propTypes = {
    dataArray: PropTypes.array,
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    fetchMore: PropTypes.func,
    refetchQuery: PropTypes.func,
    timeElapseToMountIndicator: PropTypes.number,
    preferredEmptyItemsMessage: PropTypes.string,
    preferredErrorMessage: PropTypes.string,
    renderRow: PropTypes.func,
    isFetchingMore: PropTypes.bool,
    containerType: PropTypes.oneOf(["image", "plain"]),
    storageKey: PropTypes.string,
    reduxActionID: PropTypes.string,
    reduxStateID: PropTypes.string,
    _storageData: PropTypes.any,
    screenProps: PropTypes.object,
    containerStyle: PropTypes.object,
    dummyMode: PropTypes.bool,
    listFilterer: PropTypes.any
  };

  componentDidUpdate(prevProps) {
    let {
        dataArray,
        storageKey,
        screenProps,
        reduxActionID,
        listFilterer
      } = this.props,
      { dataArray: prevDataArray } = prevProps;

    if (dataArray && dataArray !== prevDataArray && !listFilterer) {
      if (storageKey) {
        AsyncStorage.setItem(storageKey, JSON.stringify(dataArray)).then(
          result => {
            screenProps[reduxActionID](dataArray);
          }
        );
      }
    }

    if (!this.state.hasNotifiedUserOfError && this.props.error) {
      Toast.show({
        text: "No network connection, check connection and pull to refresh",
        duration: 10000,
        textStyle: ToastStyle.toast,
        type: "danger",
        buttonText: "OKAY",
        buttonStyle: ToastStyle.buttonStyle,
        buttonTextStyle: ToastStyle.buttonErrorTextStyle
      });
      this.setState({ hasNotifiedUserOfError: true });
    }
  }

  fetchMore = event => {
    let {
        contentOffset: { y },
        contentSize: { height },
        layoutMeasurement
      } = event.nativeEvent,
      { fetchMore } = this.props;
    if (fetchMore) {
      if (height - layoutMeasurement.height == y) {
        fetchMore(event);
      }
    }
  };

  renderNormalMode = () => {
    let {
        loading,
        error,
        fetchMore,
        refetchQuery,
        timeElapseToMountIndicator,
        preferredEmptyItemsMessage,
        preferredErrorMessage,
        renderRow,
        storageKey,
        dataArray,
        containerType,
        isFetchingMore,
        _storageData,
        listFilterer
      } = this.props,
      _style = this.props.containerStyle || {},
      _listArray = storageKey
        ? !listFilterer
          ? _storageData
          : listFilterer(_storageData)
        : dataArray;

    if (!_listArray && loading) {
      return <RequestActivityIndicator delay={timeElapseToMountIndicator} />;
    } else if (!_listArray && error) {
      return (
        <NativeTouchableFeedback onPress={refetchQuery}>
          <View style={styles.container}>
            <Icon name="alert-circle" type="feather-icon" style={styles.icon} />
            <MediumText style={styles.text}>
              {preferredErrorMessage ||
                `Error occurred in connecting, tap to reload`}
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      );
    } else if (_listArray && _listArray.length == 0) {
      return (
        <NativeTouchableFeedback onPress={refetchQuery}>
          <View style={styles.container}>
            <Icon name="comment" type="community-icon" style={styles.icon} />
            <MediumText style={styles.text}>
              {preferredEmptyItemsMessage || `Oops! No result found here`}
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      );
    } else if (!_listArray) {
      return null;
    }

    return (
      <PaddingProvider
        type={containerType}
        style={{ paddingHorizontal: 0, ..._style }}
      >
        <List
          onScroll={this.fetchMore}
          dataArray={_listArray}
          refreshControl={
            <AppRefreshControl refreshing={loading} onRefresh={refetchQuery} />
          }
          renderRow={list => renderRow(list)}
        />
      </PaddingProvider>
    );
  };

  renderDummyMode = () => {
    let {
        loading,
        error,
        fetchMore,
        refetchQuery,
        timeElapseToMountIndicator,
        preferredEmptyItemsMessage,
        preferredErrorMessage,
        renderRow,
        storageKey,
        dataArray,
        containerType,
        isFetchingMore,
        _storageData
      } = this.props,
      _style = this.props.containerStyle || {},
      _listArray = storageKey ? _storageData : dataArray;

    if (loading) {
      return <RequestActivityIndicator delay={timeElapseToMountIndicator} />;
    } else if (error) {
      return (
        <NativeTouchableFeedback onPress={refetchQuery}>
          <View style={styles.container}>
            <Icon name="alert-circle" type="feather-icon" style={styles.icon} />
            <MediumText style={styles.text}>
              {preferredErrorMessage ||
                `Error occurred in connecting, tap to reload`}
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      );
    } else if (_listArray && _listArray.length == 0) {
      return (
        <NativeTouchableFeedback onPress={refetchQuery}>
          <View style={styles.container}>
            <Icon name="comment" type="community-icon" style={styles.icon} />
            <MediumText style={styles.text}>
              {preferredEmptyItemsMessage || `Oops! No existing contract`}
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      );
    } else if (!_listArray) {
      return null;
    }

    return (
      <PaddingProvider
        type={containerType}
        style={{ paddingHorizontal: 0, ..._style }}
      >
        <List
          onScroll={this.fetchMore}
          dataArray={_listArray}
          refreshControl={
            <AppRefreshControl
              refreshing={loading || isFetchingMore}
              onRefresh={refetchQuery}
            />
          }
          renderRow={renderRow}
        />
      </PaddingProvider>
    );
  };

  render() {
    if (this.props.dummyMode) {
      return this.renderDummyMode();
    } else {
      return this.renderNormalMode();
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    _storageData: state[ownProps.reduxStateID]
  };
}

const _AppList = props => (
  <ReduxContext.Consumer>
    {({ screenProps }) => <AppList {...props} screenProps={screenProps} />}
  </ReduxContext.Consumer>
);

export default connect(mapStateToProps)(_AppList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  icon: {
    fontSize: Fonts.errorIcon,
    color: Colors.errorIconColor
  },
  text: {
    color: Colors.errorIconColor
  }
});
