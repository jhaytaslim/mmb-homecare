import React from "react";
import { ListItem, Left, Icon, Right } from "native-base";
import { StyleSheet, Image, View } from "react-native";
import PropTypes from "prop-types";

import { MediumText } from "../AppText";
import NativeTouchableFeedback from "../NativeTouchableFeedback";

export default class FlatListItem extends React.PureComponent {
  static propTypes = {
    iconName: PropTypes.string,
    uri: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    descriptionStyle: PropTypes.any,
    tag: PropTypes.string,
    date: PropTypes.string,
    type: PropTypes.oneOf(["icon", "avatar"]),
    onPress: PropTypes.func.isRequired,
    tagStyle: PropTypes.any
  };

  render() {
    let {
        iconName,
        uri,
        title,
        description,
        descriptionStyle,
        tag,
        date,
        type,
        tagStyle,
        onPress
      } = this.props,
      _extraStyle = !description && !tag ? { paddingTop: 20 } : {},
      _descriptionStyle = descriptionStyle || {},
      _tagStyle = tagStyle || {};

    return (
      <ListItem style={styles.listItem} onPress={onPress}>
        <Left>
          <View style={styles.left}>
            {type == "icon" ? (
              <Icon name={iconName} style={styles.icon} />
            ) : (
              <Image source={{ uri }} style={styles.avatar} />
            )}
          </View>
          <View style={[styles.body, _extraStyle]}>
            <MediumText style={styles.title}>{title}</MediumText>
            {description && (
              <MediumText style={[styles.description, _descriptionStyle]}>
                {description}
              </MediumText>
            )}
            {tag && (
              <MediumText style={[styles.tag, _tagStyle]}>{tag}</MediumText>
            )}
          </View>
        </Left>
        {date && (
          <Right>
            <MediumText style={styles.date}>{date}</MediumText>
          </Right>
        )}
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    marginLeft: 0,
    paddingHorizontal: 15
  },
  left: {
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#b6e3ec"
  },
  body: {
    marginLeft: 20,
    justifyContent: "center"
  },
  icon: {
    color: "#fff",
    fontSize: 30
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  title: {
    color: "#333",
    fontSize: 18
  },
  description: {
    color: "#333",
    fontSize: 14
  },
  tag: {
    fontSize: 14,
    color: "#bdbdbd"
  },
  date: {
    fontSize: 12,
    color: "#0096c0",
    marginTop: -30
  }
});
