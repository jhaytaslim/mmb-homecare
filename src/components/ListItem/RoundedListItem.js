import React, { PureComponent } from "react";
import { View, StyleSheet, Image } from "react-native";
import StarRating from "react-native-star-rating";
import PropTypes from "prop-types";
import moment from "moment";
import { connect } from "react-redux";

import Icon from "../../components/Icon";
import { BoldText, MediumText } from "../../components/AppText";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";
import Colors from "../../assets/Colors";

class RoundedListItem extends PureComponent {
  static propTypes = {
    isVerified: PropTypes.bool,
    photo: PropTypes.string,
    carerCategory: PropTypes.string,
    name: PropTypes.string,
    healthCategory: PropTypes.string,
    lastChat: PropTypes.string,
    date: PropTypes.any,
    price: PropTypes.string,
    location: PropTypes.string,
    slug: PropTypes.string,
    ratingsCount: PropTypes.number,
    style: PropTypes.any,
    onPress: PropTypes.func,
    showUnreadMessages: PropTypes.bool,
    messageId: PropTypes.string,
    messageCountObject: PropTypes.object,
    isOnline: PropTypes.number
  };

  curencyFormatter = num => {
    return (
      <BoldText style={[styles.subTitle, styles.blueColor]}>
        N
        {parseInt(price)
          .toFixed(2)
          .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
      </BoldText>
    );
  };

  formatMoment = date => {
    let placeholder = undefined;
    /days/gi.test(date) && (placeholder = date.replace(/days/gi, "d"));
    /hours/gi.test(date) && (placeholder = date.replace(/hours/gi, "h"));
    /weeks/gi.test(date) && (placeholder = date.replace(/weeks/gi, "w"));
    /months/gi.test(date) && (placeholder = date.replace(/months/gi, "h"));
    return (placeholder && placeholder.replace(/\s/gi, "")) || date;
  };

  render() {
    const {
      isVerified,
      photo,
      carerCategory,
      name,
      lastChat,
      date,
      healthCategory,
      price,
      location,
      slug,
      ratingsCount,
      style,
      showUnreadMessages,
      messageCountObject,
      messageId,
      photoSize,
      isOnline
    } = this.props;

    let _style = style || {},
      _photoSize = photoSize || 60;

    return (
      <NativeTouchableFeedback onPress={this.props.onPress}>
        <View style={[styles.listBg, styles.pad15, _style]}>
          <View style={StyleSheet.flatten([styles.flRow, styles.centered])}>
            <View style={styles.flex1}>
              <View />
              <View
                style={[
                  styles.centered,
                  styles.photoConStyle,
                  {
                    width: _photoSize + 2,
                    height: _photoSize + 2,
                    borderRadius: (_photoSize + 2) / 2
                  }
                ]}
              >
                {photo ? (
                  <Image
                    source={{
                      uri: `https://www.mmbhomecare.com/uploads/70-${photo}`
                    }}
                    style={{
                      width: _photoSize,
                      height: _photoSize,
                      borderRadius: _photoSize / 2
                    }}
                  />
                ) : (
                  <Icon
                    type="ionic-icon"
                    name="ios-person"
                    style={{ fontSize: 50, color: "#fff" }}
                  />
                )}
              </View>
            </View>
            <View style={[styles.detailStyle, styles.flex3]}>
              <View style={{ flex: 1 }}>
                <View
                  style={[
                    styles.flRow,
                    styles.jcBtw,
                    styles.aiCentered,
                    { flex: 1 }
                  ]}
                >
                  <BoldText style={[styles.title, styles.blueColor]}>
                    {carerCategory}
                  </BoldText>
                  {ratingsCount != 0 &&
                    !date && (
                      <StarRating
                        disabled={true}
                        maxStars={5}
                        rating={ratingsCount}
                        fullStarColor={"#e65100"}
                        starSize={15}
                      />
                    )}
                </View>
                {lastChat ? (
                  <View style={{ marginTop: 5, flex: 1 }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        paddingRight: 10
                      }}
                    >
                      <BoldText
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={[
                          styles.subTitle,
                          styles.blueColor,
                          { marginRight: 60, fontSize: 16 }
                        ]}
                      >
                        {name}
                      </BoldText>
                      <BoldText
                        style={[
                          styles.blueColor,
                          { position: "absolute", right: 4 }
                        ]}
                      >
                        {this.formatMoment(moment(date).fromNow(true))}
                      </BoldText>
                    </View>
                    <MediumText
                      numberOfLines={2}
                      ellipsizeMode="tail"
                      style={[
                        styles.subTitle,
                        styles.ashColor,
                        { fontSize: 15, marginLeft: 3 }
                      ]}
                    >
                      {lastChat}
                    </MediumText>
                    {showUnreadMessages &&
                      messageId in messageCountObject && (
                        <View style={styles.messageCountContainer}>
                          <BoldText style={styles.messageCount}>
                            {messageCountObject[messageId]}
                          </BoldText>
                        </View>
                      )}
                  </View>
                ) : (
                  <View style={{ marginTop: 5 }}>
                    <BoldText style={[styles.subTitle, styles.blueColor]}>
                      {name}
                    </BoldText>
                    {isOnline == 1 ? (
                      <View style={styles.onlineStatusContainer}>
                        <View style={styles.onlineStatus} />
                        <MediumText style={styles.onlineStatusText}>
                          Online
                        </MediumText>
                      </View>
                    ) : isOnline == 0 ? (
                      <View style={styles.onlineStatusContainer}>
                        <View
                          style={[
                            styles.onlineStatus,
                            { backgroundColor: "#c62828" }
                          ]}
                        />
                        <MediumText style={styles.onlineStatusText}>
                          Offline
                        </MediumText>
                      </View>
                    ) : null}
                    <BoldText style={[styles.subTitle, styles.ashColor]}>
                      {healthCategory}
                    </BoldText>
                  </View>
                )}
                {price ? (
                  <View style={{ marginTop: 5 }}>
                    <BoldText style={[styles.subTitle, styles.blueColor]}>
                      N
                      {parseInt(price)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                    </BoldText>
                    {location && (
                      <BoldText style={[styles.smallTitle, styles.ashColor]}>
                        {location}
                      </BoldText>
                    )}
                  </View>
                ) : null}
                <BoldText style={[styles.extraSmallTitle, styles.blueColor]}>
                  {slug}
                </BoldText>
              </View>
            </View>
            <View style={styles.flex03}>
              <Icon
                type="simple-line-icon"
                name="arrow-right"
                style={{ fontSize: 20 }}
              />
            </View>
          </View>
        </View>
      </NativeTouchableFeedback>
    );
  }
}

function mapStateToProps(state) {
  return {
    messageCountObject: state.messageCountObject
  };
}

export default connect(mapStateToProps)(RoundedListItem);

const styles = StyleSheet.create({
  flRow: {
    flexDirection: "row"
  },
  flex05: {
    flex: 0.5
  },
  flex2: {
    flex: 2
  },
  flex3: {
    flex: 3
  },
  flex03: {
    flex: 0.3
  },
  detailStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15
  },
  top: {
    justifyContent: "flex-end",
    paddingRight: 15
  },
  centered: {
    alignItems: "center",
    justifyContent: "center"
  },
  aiCentered: {
    alignItems: "center"
  },
  onlineStatusContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  onlineStatus: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: "#4caf50"
  },
  onlineStatusText: {
    fontSize: 13,
    marginLeft: 6,
    color: Colors.brandColor
  },
  photoConStyle: {
    borderRadius: 40,
    height: 80,
    width: 80,
    backgroundColor: "#afe3eb"
  },
  pad15: {
    padding: 10
  },
  title: {
    fontSize: 26
  },
  subTitle: {
    fontSize: 18
  },
  leftPaddingXs: {
    paddingLeft: 5
  },
  smallTitle: {
    fontSize: 14
  },
  extraSmallTitle: {
    fontSize: 11
  },
  ashColor: {
    color: "#666"
  },
  darkAshColor: {
    color: "#949595"
  },
  blueColor: {
    color: "#00d0e0"
  },
  blackColor: {
    color: "#000"
  },
  listBg: {
    backgroundColor: "#ebf9fc",
    borderRadius: 25
  },
  jcBtw: {
    justifyContent: "space-between"
  },
  messageCountContainer: {
    height: 22,
    width: 22,
    borderRadius: 11,
    position: "absolute",
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: 24,
    backgroundColor: Colors.brandColor
  },
  messageCount: {
    fontSize: 10,
    color: "#fff"
  }
});
