import React from "react";
import {
  View,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback
} from "react-native";
import PropTypes from "prop-types";

import { LightText } from "../AppText";
import { NavigationContext } from "../../context/NavigtionContext";
import Icon from "../Icon";
import AppButton from "../AppButton";

export default (UnauthorizedModal = props => (
  <NavigationContext.Consumer>
    {({ navigation: { navigate } }) => (
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.isVisible}
        onRequestClose={props.onClose}
      >
        <TouchableWithoutFeedback onPress={props.onClose}>
          <View style={styles.container}>
            <View style={styles.modal}>
              <View style={styles.top}>
                <Icon
                  name="ios-information-circle-outline"
                  type="ionic-icon"
                  style={styles.icon}
                  contentContainerStyle={{ marginLeft: 20 }}
                />
                <View>
                  <LightText style={styles.text}>NOT ALLOWED!</LightText>
                  <LightText style={styles.text}>
                    You Need to Be Subscribed to View a Carer
                  </LightText>
                </View>
              </View>
              <View style={styles.bottom}>
                <AppButton title="Cancel" onPress={props.onClose} />
                <AppButton
                  title="Subscribe"
                  onPress={props.onSubscribe}
                  style={styles.subscribe}
                />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )}
  </NavigationContext.Consumer>
));

UnauthorizedModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubscribe: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(255,255,255, 0.5)",
    justifyContent: "flex-end"
  },
  modal: {
    padding: 20,
    backgroundColor: "#0096c0",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  top: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20
  },
  bottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 30
  },
  subscribe: {
    backgroundColor: "red"
  },
  icon: {
    fontSize: 100,
    color: "#fff"
  },
  text: {
    color: "#fff",
    fontSize: 15,
    marginLeft: 10
  }
});
