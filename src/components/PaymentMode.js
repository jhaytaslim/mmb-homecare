import React from "react";
import PropTypes from "prop-types";
import { Modal, View, StyleSheet, ImageBackground } from "react-native";
import { Icon } from "native-base";

import Fonts from "../assets/Fonts";
import { BoldText, MediumText } from "./AppText";
import Colors from "../assets/Colors";
import NativeTouchableFeedback from "./NativeTouchableFeedback";
import AppButton from "./AppButton";

export default class PaymentMode extends React.PureComponent {
  state = {
    segment: 0,
    ref: ""
  };

  static propTypes = {
    onCardPaymentSelection: PropTypes.func
  };

  handleBankSuccess = response => {
    let {
      entity: { ref }
    } = response;

    this.setState({ segment: 2, reference: ref });
  };

  renderPaymentModeSelectionModal = () => {
    let { onCardPaymentSelection } = this.props;
    return (
      <View>
        <BoldText style={styles.title}>SELECT PAYMENT MODE</BoldText>
        <NativeTouchableFeedback onPress={() => onCardPaymentSelection("card")}>
          <View style={styles.paymentMode}>
            <Icon name="cc-mastercard" type="FontAwesome" />
            <MediumText style={{ ...styles.text, marginTop: -5 }}>
              Payment via Card
            </MediumText>
          </View>
        </NativeTouchableFeedback>
        <NativeTouchableFeedback onPress={() => this.setState({ segment: 1 })}>
          <View style={styles.paymentMode}>
            <Icon name="transfer" type="MaterialCommunityIcons" />
            <MediumText style={{ ...styles.text, marginTop: -5 }}>
              Payment via Bank Deposit
            </MediumText>
          </View>
        </NativeTouchableFeedback>
      </View>
    );
  };

  renderBankTransferDetails = () => {
    let { onCardPaymentSelection } = this.props;
    return (
      <View>
        <BoldText style={styles.title}>GENERATE REFERENCE NUMBER</BoldText>
        <MediumText style={styles.text}>
          A Reference number would be generated for you to use in making
          payments. Please use this number as the depositor's name
        </MediumText>
        <View>
          <View style={{ flexDirection: "row" }}>
            <AppButton
              title="Generate Reference Number"
              style={{ ...styles.appButton, flex: 1 }}
              onPress={() =>
                onCardPaymentSelection("bank", this.handleBankSuccess)
              }
            />
          </View>
          <AppButton
            title="Back"
            style={{
              ...styles.appButton,
              ...styles.backBtn
            }}
            onPress={() => this.setState({ segment: 0 })}
          />
        </View>
      </View>
    );
  };

  renderPaymentSuccessful = () => {
    return (
      <View>
        <BoldText style={styles.title}>REFERENCE NUMBER GENERATED</BoldText>
        <MediumText style={styles.text}>
          Kindly proceed to make payment using the following details
        </MediumText>
        <MediumText style={styles.text}>
          Account Name: Silverwings Medical Informatics Nig. Ltd
        </MediumText>
        <MediumText style={styles.text}>Account Number: 0243133850</MediumText>
        <MediumText style={styles.text}>Bank: GTBank</MediumText>
        <MediumText style={styles.text}>
          Reference Number: {this.state.reference}{" "}
          <MediumText style={{ color: "green" }}>
            This should be used as the depositor's name
          </MediumText>
        </MediumText>
        <MediumText style={styles.text}>Send a mail to</MediumText>
        <MediumText style={styles.text}>
          Payment@mymedicalbank.com after making the transfer attaching the
          receipt of payment
        </MediumText>
        <MediumText
          style={{ ...styles.text, textAlign: "center", marginVertical: 10 }}
        >
          Or
        </MediumText>
        <MediumText style={styles.text}>
          Contact 08036288801 or 09093816893 to confirm payments
        </MediumText>
        <View style={styles.appButtonContainer}>
          <AppButton
            title="Okay"
            style={styles.appButton}
            onPress={() =>
              this.setState({ segment: 0 }, () => this.props.onClose())
            }
          />
        </View>
      </View>
    );
  };

  render() {
    let { segment } = this.state,
      { visible, onClose } = this.props;

    return (
      <Modal
        transparent={true}
        visible={visible}
        onRequestClose={() => this.setState({ segment: 0 }, () => onClose())}
        animationType="slide"
      >
        <ImageBackground
          source={require("../assets/Images/authImage.png")}
          style={styles.mainContainer}
        >
          <View style={styles.container}>
            {segment == 0
              ? this.renderPaymentModeSelectionModal()
              : segment == 1
                ? this.renderBankTransferDetails()
                : this.renderPaymentSuccessful()}
          </View>
        </ImageBackground>
      </Modal>
    );
  }
}

const styles = {
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    marginHorizontal: 10,
    backgroundColor: "#fff",
    borderRadius: 15,
    alignItems: "center",
    padding: 20
  },
  title: {
    fontSize: Fonts.signUp.verificationTitle,
    color: Colors.signUp.verificationTitle,
    marginVertical: 10,
    textAlign: "center"
  },
  paymentMode: {
    flexDirection: "row",
    alignItems: "center",
    height: 50
  },
  text: {
    fontSize: 16,
    marginLeft: 10,
    color: "#333",
    marginTop: 5
  },
  appButtonContainer: {
    flexDirection: "row",
    alignSelf: "center"
  },
  appButton: {
    marginTop: 10,
    marginHorizontal: 5
  },
  backBtn: {
    backgroundColor: "red",
    marginTop: 10,
    alignSelf: "center"
  }
};
