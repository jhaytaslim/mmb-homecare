import React from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  View,
  Image,
  NetInfo,
  ActivityIndicator,
  Platform,
  Animated
} from "react-native";

import Fonts from "../assets//Fonts";
import Icon from "./Icon";
import { MediumText } from "../components/AppText";
import Colors from "../assets/Colors";
import { NavigationContext } from "../context/NavigtionContext";
import NativeTouchableFeedback from "./NativeTouchableFeedback";
import AppStatusBar from "../components/AppStatusBar";

/**
 * @HACK
 *
 * An hack has been provided to make the back button go back to the previous screen when the user clicks on the title
 * Necessary for the Payment page which heavily only depends on this to navigatye to previos screens
 */
export default class AppHeader extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: false,
      errorView: new Animated.Value(0)
    };
  }

  static propTypes = {
    isTitleHeaderHidden: PropTypes.bool,
    title: PropTypes.string,
    titleHeaderRightComponent: PropTypes.any,
    titleHeaderBodyComponent: PropTypes.any,
    onPressBackButton: PropTypes.func
  };

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      this.monitorInternetConnectivity(isConnected);
    });

    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.monitorInternetConnectivity
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener("connectionChange");
  }

  monitorInternetConnectivity = isConnected => {
    this.setState({ isConnected }, () => {
      if (this.state.isConnected) {
        setTimeout(() => {
          Animated.timing(this.state.errorView, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true
          }).start();
        }, 3000);
      } else {
        Animated.timing(this.state.errorView, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true
        }).start();
      }
    });
  };

  render() {
    let {
        title,
        isTitleHeaderHidden,
        titleHeaderRightComponent,
        titleHeaderBodyComponent,
        onPressBackButton
      } = this.props,
      { isConnected } = this.state;

    const transform = [
      {
        translateY: this.state.errorView.interpolate({
          inputRange: [0, 1],
          outputRange: [-25, 0]
        })
      }
    ];

    return (
      <NavigationContext>
        {({ navigation: { navigate, goBack } }) => (
          <React.Fragment>
            <AppStatusBar barStyle="light-content" backgroundColor="#00b0cf" />
            <Animated.View
              style={[
                styles.networkHeader,
                {
                  transform,
                  backgroundColor: isConnected ? "#009cc3" : "#d50000"
                }
              ]}
            >
              {isConnected ? (
                <React.Fragment>
                  <ActivityIndicator
                    animating={true}
                    size={Platform.OS == "android" ? 20 : 0}
                    color="#fff"
                  />
                  <MediumText style={styles.networkText}>
                    Connecting...
                  </MediumText>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <Icon
                    name="reload"
                    type="simple-line-icon"
                    style={styles.networkIcon}
                  />
                  <MediumText style={styles.networkText}>Offline</MediumText>
                </React.Fragment>
              )}
            </Animated.View>
            <View style={styles.symbolHeader}>
              <Image
                source={require("../assets/Images/logo.png")}
                style={styles.healthIcon}
              />
            </View>
            <View style={styles.header}>
              <Icon
                name="md-menu"
                type="ionic-icon"
                onPress={() => navigate("DrawerOpen")}
                style={[styles.icon]}
              />
              <Icon
                name="md-settings"
                type="ionic-icon"
                onPress={() => navigate("settings")}
                style={[styles.icon]}
              />
            </View>
            {!isTitleHeaderHidden && (
              <View style={[styles.header, styles.titleHeader]}>
                <NativeTouchableFeedback
                  onPress={
                    onPressBackButton ? onPressBackButton : () => goBack()
                  }
                >
                  <View style={styles.backIconContainer}>
                    <Icon
                      name="ios-arrow-dropleft"
                      type="ionic-icon"
                      style={[styles.icon, styles.titleHeaderIcons]}
                    />
                  </View>
                </NativeTouchableFeedback>
                {titleHeaderBodyComponent || (
                  <MediumText onPress={() => goBack()} style={styles.title}>
                    {title}
                  </MediumText>
                )}
                {titleHeaderRightComponent || <View />}
              </View>
            )}
          </React.Fragment>
        )}
      </NavigationContext>
    );
  }
}

const styles = StyleSheet.create({
  symbolHeader: {
    height: 130,
    width: 130,
    zIndex: 1,
    marginTop: -95,
    alignSelf: "center",
    backgroundColor: Colors.appHeader.symbolContainerBackground,
    borderRadius: 65,
    alignItems: "center",
    paddingTop: 65
  },
  networkHeader: {
    height: 25,
    flexDirection: "row",
    zIndex: 2,
    elevation: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  networkText: {
    color: "#fff",
    marginLeft: 5
  },
  networkIcon: {
    color: "#fff"
  },
  titleHeader: {
    backgroundColor: Colors.appHeader.titleHeaderBackground,
    marginTop: 0
  },
  titleHeaderBody: {
    flexDirection: "row"
  },
  title: {
    color: Colors.appHeader.title,
    fontSize: Fonts.appHeader.text,
    marginLeft: -40,
    zIndex: 100
  },
  healthIcon: {
    paddingTop: 10,
    height: 50,
    width: 50
  },
  header: {
    backgroundColor: Colors.appHeader.mainHeaderBackground,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    marginTop: -65,
    paddingHorizontal: 20
  },
  icon: {
    fontSize: Fonts.appHeader.icons,
    color: Colors.appHeader.defaultIcon
  },
  titleHeaderIcons: {
    color: Colors.appHeader.titleIcon
  },
  backIconContainer: {
    height: 40,
    width: 40,
    zIndex: 1000,
    justifyContent: "center"
  }
});
