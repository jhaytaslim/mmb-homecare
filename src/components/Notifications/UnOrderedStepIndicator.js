import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, ScrollView, Dimensions, View } from "react-native";
import { TabView, SceneMap } from "react-native-tab-view";

import NativeTouchableFeedback from "../NativeTouchableFeedback";
import { MediumText } from "../AppText";
import Colors from "../../assets/Colors";

const ActivityItems = ({ activityItems, renderItem }) => (
  <View style={styles.pageStyle}>
    <ScrollView showsVerticalScrollIndicator={false}>
      {activityItems && activityItems.length > 0 ? (
        activityItems.map((item, i) => renderItem(item, i))
      ) : !activityItems ? null : (
        <MediumText style={styles.noData}>No Data Available Yet</MediumText>
      )}
    </ScrollView>
  </View>
);

const BookingItems = ({ bookingItems, renderItem }) => (
  <View style={styles.pageStyle}>
    <ScrollView showsVerticalScrollIndicator={false}>
      {bookingItems && bookingItems.length > 0 ? (
        bookingItems.map((item, i) => renderItem(item, i))
      ) : !bookingItems ? null : (
        <MediumText style={styles.noData}>No Data Available Yet</MediumText>
      )}
    </ScrollView>
  </View>
);

const TabBar = ({ index, switchTab }) => (
  <View style={styles.tabBar}>
    <NativeTouchableFeedback onPress={() => switchTab(0)}>
      <View style={[styles.tab, index == 0 ? styles.activeTab : {}]}>
        <MediumText
          style={[styles.tabLabel, index == 0 ? styles.activeTabLabel : {}]}
        >
          My Activity Log
        </MediumText>
      </View>
    </NativeTouchableFeedback>
    <NativeTouchableFeedback onPress={() => switchTab(1)}>
      <View style={[styles.tab, index == 1 ? styles.activeTab : {}]}>
        <MediumText
          style={[styles.tabLabel, index == 1 ? styles.activeTabLabel : {}]}
        >
          My Booking Notifications
        </MediumText>
      </View>
    </NativeTouchableFeedback>
  </View>
);

export default class UnOrderedStepIndicator extends React.PureComponent {
  static propTypes = {
    activityItems: PropTypes.array,
    bookingItems: PropTypes.array,
    renderItem: PropTypes.func
  };

  state = {
    index: 0,
    routes: [
      { key: "activity", title: "My Activity Log" },
      { key: "booking", title: "My Booking Notifications" }
    ]
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          activity: props => (
            <ActivityItems
              activityItems={this.props.activityItems}
              renderItem={this.props.renderItem}
            />
          ),
          booking: props => (
            <BookingItems
              bookingItems={this.props.bookingItems}
              renderItem={this.props.renderItem}
            />
          )
        })}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height
        }}
        renderTabBar={props => (
          <TabBar
            index={this.state.index}
            switchTab={index => this.setState({ index })}
          />
        )}
      />
    );
  }
}

export const IndicatorRow = props => {
  return (
    <View style={styles.tabContainer}>
      <View>
        <View style={styles.lineSeperator} />
        <View style={styles.circleIndicator} />
      </View>

      <View style={styles.activityContainer}>
        <MediumText style={styles.activity}>{props.activity}</MediumText>
        <MediumText style={styles.activity}>{props.time}</MediumText>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10
  },
  pageStyle: {
    flex: 1
  },
  tabContainer: {
    flexDirection: "row",
    marginTop: 0
  },
  lineSeperator: {
    height: 60,
    width: 2,
    marginLeft: 3,
    backgroundColor: Colors.unorderedStepIndicator.indicator
  },
  circleIndicator: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: Colors.unorderedStepIndicator.indicator
  },
  activityContainer: {
    marginLeft: 20,
    marginTop: 45,
    position: "absolute"
  },
  activity: {
    color: Colors.unorderedStepIndicator.text
  },
  noData: {
    marginTop: 40,
    textAlign: "center",
    color: "#333"
  },
  tabBar: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  tab: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20
  },
  activeTab: {
    backgroundColor: "#00b0cf"
  },
  activeTabLabel: {
    color: "#fff"
  },
  tabLabel: {
    color: "#333"
  }
});
