import React from "react";
import {
  Platform,
  TouchableNativeFeedback,
  TouchableHighlight
} from "react-native";

export default (NativeTouchableFeedback = props =>
  Platform.OS == "android" ? (
    <TouchableNativeFeedback {...props}>
      {props.children}
    </TouchableNativeFeedback>
  ) : (
    <TouchableHighlight {...props}>{props.children}</TouchableHighlight>
  ));

NativeTouchableFeedback.propTypes = {
  ...TouchableNativeFeedback.propTypes
};
