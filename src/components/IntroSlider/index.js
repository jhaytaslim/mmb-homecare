import React from "react";
import { StyleSheet, View, Image } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

import { BoldText } from "../AppText";
import Icon from "../Icon";
import NotificationHandler from "../../containers/NotificationHandler";
import AppStatusBar from "../AppStatusBar";

const slides = [
  {
    key: "somethun",
    title: "Instant Booking",
    text:
      "Book registered healthcare professionals instantly from your mobile devices",
    icon: "ios-images-outline",
    bg: "#0089b7",
    titleColor: {
      color: "#fff"
    },
    textColor: {
      color: "#fff"
    },
    image: require("../../assets/Images/stethoscope.png")
  },
  {
    key: "somethun1",
    title: "Choice",
    text:
      "Find a healthcare professional of your choice to look after you and your loved ones.",
    icon: "ios-options-outline",
    bg: "#fff",
    titleColor: {
      color: "#ff2a39"
    },
    textColor: {
      color: "#4f4a4b"
    },
    image: require("../../assets/Images/checklist.png")
  },
  {
    key: "eliminate",
    title: "Eliminate Waiting Rooms",
    text:
      "Eliminate the need to spend time in hospital waiting rooms unless necessary",
    icon: "ios-beer-outline",
    bg: "#2a2841",
    titleColor: {
      color: "#fff"
    },
    textColor: {
      color: "#fff"
    },
    image: require("../../assets/Images/cancel.png")
  },
  {
    key: "privacy",
    title: "Privacy",
    text:
      "Receive medical consultation in the privacy of your home or workplace",
    icon: "ios-beer-outline",
    bg: "#0089b7",
    titleColor: {
      color: "#fff"
    },
    textColor: {
      color: "#fff"
    },
    image: require("../../assets/Images/binoculars.png")
  },
  {
    key: "healthcare",
    title: "Personalized Healthcare",
    text: "Receive personalised healthcare, tailored to your own needs.",
    icon: "ios-beer-outline",
    bg: "#fff",
    titleColor: {
      color: "#ff2a39"
    },
    textColor: {
      color: "#4f4a4b"
    },
    image: require("../../assets/Images/first-aid-kit.png")
  },
  {
    key: "Comfort",
    title: "Comfort",
    text:
      "Receive medical consultation in the comfort of your home or workplace",
    icon: "ios-beer-outline",
    bg: "#2a2841",
    titleColor: {
      color: "#fff"
    },
    textColor: {
      color: "#fff"
    },
    image: require("../../assets/Images/bed.png")
  }
];

class IntroSlider extends React.Component {
  _renderItem = props => (
    <View
      style={[
        styles.mainContent,
        {
          paddingTop: props.topSpacer,
          paddingBottom: props.bottomSpacer,
          width: props.width,
          height: props.height,
          backgroundColor: props.bg
        }
      ]}
    >
      <View />
      <Image source={props.image} />
      <View>
        <BoldText style={[styles.title, props.titleColor]}>
          {props.title}
        </BoldText>
        <BoldText style={[styles.text, props.textColor]}>{props.text}</BoldText>
      </View>
    </View>
  );

  _renderNextButton = props => {
    return (
      <View>
        <Icon
          name="arrow-right"
          type="feather-icon"
          style={{
            backgroundColor: "transparent",
            color: "#000",
            fontSize: 50
          }}
        />
      </View>
    );
  };

  _renderPrevButton = props => {
    return (
      <View>
        <Icon
          name="arrow-left"
          type="feather-icon"
          style={{
            backgroundColor: "transparent",
            color: "#000",
            fontSize: 50
          }}
        />
      </View>
    );
  };

  _renderDoneButton = () => {
    let {
      navigation: { navigate }
    } = this.props;
    return (
      <View style={styles.buttonCircle}>
        <Icon
          type="material-icon"
          onPress={() => navigate("login")}
          name="check"
          style={{
            backgroundColor: "transparent",
            fontSize: 50,
            color: "green"
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <React.Fragment>
        <AppStatusBar />
        <AppIntroSlider
          slides={slides}
          showPrevButton
          renderItem={this._renderItem}
          renderDoneButton={this._renderDoneButton}
          renderNextButton={this._renderNextButton}
          renderPrevButton={this._renderPrevtButton}
          activeDotColor="#f44336"
        />
      </React.Fragment>
    );
  }
}

export default (_IntroSlider = props => (
  <NotificationHandler navigation={props.navigation}>
    <IntroSlider {...props} />
  </NotificationHandler>
));

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  text: {
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 20,
    paddingHorizontal: 22,
    lineHeight: 20,
    letterSpacing: 0.8
  },
  title: {
    fontSize: 40,
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16,
    lineHeight: 60
  }
});
