import React from "react";
import { View, Dimensions } from "react-native";
import PropTypes from "prop-types";
import moment from "moment";

import { LightText } from "../AppText";

export default class SubscriptionsList extends React.PureComponent {
  static propTypes = {
    dataArray: PropTypes.array
  };

  renderSection = text => (
    <View style={styles.container}>
      <LightText style={styles.text}>{text}</LightText>
    </View>
  );

  render() {
    let { dataArray } = this.props;
    return (
      <React.Fragment>
        <LightText style={styles.mediumText}>
          Previous Subscription Payments
        </LightText>
        <View style={{ ...styles.section, ...styles.sectionActive }}>
          {this.renderSection("Package Title")}
          {this.renderSection("Package Date")}
          {this.renderSection("Amount")}
          {this.renderSection("Status")}
          {this.renderSection("Expiry Date")}
        </View>
        {dataArray.map((subscription, i) => (
          <View style={styles.section} key={i}>
            {this.renderSection(
              `${subscription.plan} Invoice ID ${subscription.ref}`
            )}
            {this.renderSection(`${subscription.created_}`)}
            {this.renderSection(`${subscription.amount}`)}
            {this.renderSection(
              moment(subscription.expiring).isBefore(new Date())
                ? "Not Active"
                : "Active"
            )}
            {this.renderSection(`${subscription.expiring_}`)}
          </View>
        ))}
      </React.Fragment>
    );
  }
}

const styles = {
  mediumText: {
    color: "#fff",
    alignSelf: "center",
    fontSize: 21,
    marginTop: 20,
    marginBottom: 15
  },
  container: {
    width: (Dimensions.get("window").width - 40) / 6,
    flexWrap: "wrap"
  },
  section: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#fff"
  },
  sectionActive: {
    backgroundColor: "#e4f6fa"
  },
  text: {
    color: "#0096c0",
    fontSize: 11
  }
};
