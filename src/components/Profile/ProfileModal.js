import React from "react";
import {
  View,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet
} from "react-native";
import PropTypes from "prop-types";

import { LightText } from "../../components/AppText";
import { NavigationContext } from "../../context/NavigtionContext";
import NativeTouchableFeedback from "../../components/NativeTouchableFeedback";

export default (ProfileModal = props => (
  <NavigationContext.Consumer>
    {({ navigation: { navigate } }) => (
      <Modal
        animationType="fade"
        transparent={true}
        visible={props.isVisible}
        onRequestClose={props.onClose}
      >
        <TouchableWithoutFeedback onPress={props.onClose}>
          <View style={styles.container}>
            <NativeTouchableFeedback
              onPress={() => props.onClose(() => navigate("profile"))}
            >
              <View style={styles.section}>
                <LightText style={[styles.text, styles.coloredText]}>
                  Personal Details
                </LightText>
              </View>
            </NativeTouchableFeedback>
            <NativeTouchableFeedback
              onPress={() =>
                props.onClose(() => navigate("professionalDetails"))
              }
            >
              <View style={[styles.section, styles.coloredSection]}>
                <LightText style={styles.text}>Professional Details</LightText>
              </View>
            </NativeTouchableFeedback>
            <NativeTouchableFeedback
              onPress={() => props.onClose(() => navigate("carerPaymentInfo"))}
            >
              <View style={styles.section}>
                <LightText style={[styles.text, styles.coloredText]}>
                  Payment Information
                </LightText>
              </View>
            </NativeTouchableFeedback>
            <NativeTouchableFeedback
              onPress={() => props.onClose(() => navigate("carerIntro"))}
            >
              <View style={[styles.section, styles.coloredSection]}>
                <LightText style={styles.text}>Carer Introduction</LightText>
              </View>
            </NativeTouchableFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )}
  </NavigationContext.Consumer>
));

ProfileModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    marginTop: 50
  },
  section: {
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  },
  coloredSection: {
    backgroundColor: "#0096c0"
  },
  text: {
    color: "#fff",
    fontSize: 15
  },
  coloredText: {
    color: "#0096c0"
  }
});
